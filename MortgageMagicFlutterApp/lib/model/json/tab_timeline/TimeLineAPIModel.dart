import 'TimeLinePostModel.dart';

class TimeLineAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  TimeLineAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory TimeLineAPIModel.fromJson(Map<String, dynamic> j) {
    return TimeLineAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  List<dynamic> timelinePosts;
  _ResponseData({this.timelinePosts});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_Messages = [];
    try {
      list_Messages = (j['TimelinePosts'] != null)
          ? j['TimelinePosts']
              .map((i) => TimeLinePostModel.fromJson(i))
              .toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(timelinePosts: list_Messages ?? []);
  }
  Map<String, dynamic> toMap() => {
        'TimelinePosts': timelinePosts,
      };
}
