import 'dart:developer';

import 'package:aitl/model/json/tab_timeline/TimelineUserModel.dart';

class TimeLineAdvisorAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  TimeLineAdvisorAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  TimeLineAdvisorAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<TimelineUserModel> users;
  ResponseData({this.users});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['Users'] != null) {
      users = [];
      json['Users'].forEach((v) {
        try {
          users.add(new TimelineUserModel.fromJson(v));
        } catch (e) {
          log(e.toString());
        }
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['Users'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
