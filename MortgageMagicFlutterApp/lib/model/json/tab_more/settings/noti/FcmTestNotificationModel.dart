class FcmTestNotificationModel {
  int userId;
  Null user;
  bool isRead;
  String publishDateTime;
  String readDateTime;
  int entityId;
  String entityName;
  int notificationEventId;
  Null notificationEvent;
  int initiatorId;
  String initiatorName;
  String message;
  String webUrl;
  String description;
  int companyId;
  int id;

  FcmTestNotificationModel(
      {this.userId,
      this.user,
      this.isRead,
      this.publishDateTime,
      this.readDateTime,
      this.entityId,
      this.entityName,
      this.notificationEventId,
      this.notificationEvent,
      this.initiatorId,
      this.initiatorName,
      this.message,
      this.webUrl,
      this.description,
      this.companyId,
      this.id});

  FcmTestNotificationModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    isRead = json['IsRead'];
    publishDateTime = json['PublishDateTime'];
    readDateTime = json['ReadDateTime'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    notificationEventId = json['NotificationEventId'];
    notificationEvent = json['NotificationEvent'];
    initiatorId = json['InitiatorId'];
    initiatorName = json['InitiatorName'];
    message = json['Message'];
    webUrl = json['WebUrl'];
    description = json['Description'];
    companyId = json['CompanyId'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['IsRead'] = this.isRead;
    data['PublishDateTime'] = this.publishDateTime;
    data['ReadDateTime'] = this.readDateTime;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['NotificationEventId'] = this.notificationEventId;
    data['NotificationEvent'] = this.notificationEvent;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['Message'] = this.message;
    data['WebUrl'] = this.webUrl;
    data['Description'] = this.description;
    data['CompanyId'] = this.companyId;
    data['Id'] = this.id;
    return data;
  }
}
