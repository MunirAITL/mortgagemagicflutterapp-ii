import 'package:aitl/model/json/tab_more/badge/UserBadgeModel.dart';

class BadgePhotoIDAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  BadgePhotoIDAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  BadgePhotoIDAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : {};
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> userBadgePost;
  Messages({this.userBadgePost});
  Messages.fromJson(Map<String, dynamic> json) {
    try {
      userBadgePost = json['user_badge_post'].cast<String>() ?? [];
    } catch (e) {
      userBadgePost = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_badge_post'] = this.userBadgePost;
    return data;
  }
}

class ResponseData {
  UserBadgeModel userBadge;
  ResponseData({this.userBadge});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userBadge = json['UserBadge'] != null
        ? new UserBadgeModel.fromJson(json['UserBadge'])
        : {};
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userBadge != null) {
      data['UserBadge'] = this.userBadge.toJson();
    }
    return data;
  }
}
