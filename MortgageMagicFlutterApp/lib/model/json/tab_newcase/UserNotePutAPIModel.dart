import 'UserNotesModel.dart';

class UserNotePutAPIModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  UserNotePutAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory UserNotePutAPIModel.fromJson(Map<String, dynamic> j) {
    return UserNotePutAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  UserNotesModel userNote;
  _ResponseData({this.userNote});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    /*var list_UserNote = [];
    try {
      list_UserNote = (j['UserNote'] != null)
          ? j['UserNote'].map((i) => UserNotesModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }*/
    return _ResponseData(
        userNote: (j['UserNote'] != null)
            ? UserNotesModel.fromJson(j['UserNote'])
            : {});
  }
  Map<String, dynamic> toMap() => {
        'UserNote': userNote,
      };
}
