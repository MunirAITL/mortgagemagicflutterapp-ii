import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';

class UserData {
  static final UserData _appData = new UserData._internal();
  UserModel userModel;

  factory UserData() {
    return _appData;
  }
  UserData._internal();

  setUserModel() async {
    userModel = await DBMgr.shared.getUserProfile();
  }
}

final userData = UserData();
