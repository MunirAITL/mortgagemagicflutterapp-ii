import 'package:aitl/model/json/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/BadgePhotoIDDelAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/settings/ChangePwdAPIModel.dart';
import 'package:aitl/model/json/auth/ForgotAPIModel.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:aitl/model/json/auth/RegAPIModel.dart';
import 'package:aitl/model/json/auth/email/VerifyEmailAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:aitl/model/json/comp_acc/CompAccAPIModel.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/model/json/tab_more/support/ResolutionAPIModel.dart';
import 'package:aitl/model/json/tab_newcase/EditCaseAPIModel.dart';
import 'package:aitl/model/json/tab_newcase/PostCaseAPIModel.dart';
import 'package:aitl/model/json/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:aitl/model/json/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:aitl/model/json/tab_newcase/UserNotePutAPIModel.dart';
import 'package:aitl/model/json/tab_noti/NotiAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLinePostAPIModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      //  misc
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, VerifyEmailAPIModel)) {
      return VerifyEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangePwdAPIModel)) {
      return ChangePwdAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, EditCaseAPIModel)) {
      return EditCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, CompAccAPIModel)) {
      return CompAccAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeAPIModel)) {
      return BadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeEmailAPIModel)) {
      return BadgeEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDAPIModel)) {
      return BadgePhotoIDAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDDelAPIModel)) {
      // return BadgePhotoIDDelAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
