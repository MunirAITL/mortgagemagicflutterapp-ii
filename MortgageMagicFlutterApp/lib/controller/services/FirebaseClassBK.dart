import 'package:aitl/model/data/PrefMgr.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/material.dart';
//  https://console.firebase.google.com/project/mortgagemagicflutterapp/notification

class PushNotificationsManagerBK {
  PushNotificationsManagerBK._();

  static const String fcmTokenKey = "fcmTokenKey";

  factory PushNotificationsManagerBK() => _instance;

  static final PushNotificationsManagerBK _instance =
      PushNotificationsManagerBK._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  init() async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });

      //_firebaseMessaging.configure();

      // For testing purposes print the Firebase Messaging token
      // = await _firebaseMessaging.getToken();
      //print("FirebaseMessaging token: $appData.token");

      _initialized = true;

      _firebaseMessaging.getToken().then((String token) async {
        print("fcm token=" + token);
        await PrefMgr.shared.setPrefStr(fcmTokenKey, token);
      });

      /*Future.delayed(Duration.zero, () {
        _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
            print("onMessage: $message");
          },
          onBackgroundMessage:
              Platform.isIOS ? null : myBackgroundMessageHandler,
          onLaunch: (Map<String, dynamic> message) async {
            print("onLaunch: $message");
          },
          onResume: (Map<String, dynamic> message) async {
            print("onResume: $message");
          },
        );
      });*/
    }
  }

  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    print(message);
    return Future<void>.value();
  }

  notification() async {
    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher.png');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    /*await Navigator.push(
      context,
      MaterialPageRoute<void>(builder: (context) => CaseAlertScreen()),
    );*/
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    /*showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CaseAlertScreen(),
                ),
              );
            },
          )
        ],
      ),
    );*/
  }
}
