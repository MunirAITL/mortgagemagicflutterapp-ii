import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Common {
  static Future<String> getUDID(BuildContext context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  static String unEscapeString(String str) {
    //return json.decode(json.decode(str));
    return str;
  }

  static String getLastMondayDate() {
    var dayOfWeek = 1;
    DateTime date = DateTime.now();
    return date
        .subtract(Duration(days: date.weekday - dayOfWeek))
        .toIso8601String();
  }
}
