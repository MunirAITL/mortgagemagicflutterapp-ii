import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 6;

  isEmpty(TextEditingController tf, arg) {
    if (tf.text.isEmpty) {
      showToast(msg: arg);
      return false;
    }
    return true;
  }

  isFullName(String name) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(msg: "Invalid full name");
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid first name");
      return false;
    }
    return true;
  }

  isLNameOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid last name");
      return false;
    }
    return true;
  }

  isEmailOK(TextEditingController tf, String msg) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showToast(msg: msg);
      return false;
    }
    return true;
  }

  isPhoneOK(TextEditingController tf) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(msg: "Invalid Mobile Number");
      return false;
    }
    return true;
  }

  isPwdOK(TextEditingController tf) {
    if (tf.text.length < 4) {
      showToast(msg: "Password should be greater by 4 characters");
      return false;
    }
    return true;
  }

  isComNameOK(TextEditingController tf) {
    if (tf.text.length < 6) {
      showToast(msg: "Invalid Company Name");
      return false;
    }
    return true;
  }

  isDOBOK(str) {
    if (str == '') {
      showToast(msg: "Invalid Date of Birth");
      return false;
    }
    return true;
  }
}
