import 'package:aitl/model/data/UserData.dart';
import 'package:flutter/material.dart';

class PostNewCaseHelper {
  getParam({
    bool isOtherApplicantSwitch,
    bool isSPVSwitch,
    List<TextEditingController> listOApplicantInputFieldsCtr,
    String compName,
    String regAddr,
    String regDate,
    String regNo,
    String title,
    String note,
  }) {
    try {
      final paramMortgageCaseInfoEntityModelList = [];
      String customerEmail1 = "";
      String customerEmail2 = "";
      String customerEmail3 = "";
      try {
        customerEmail1 = listOApplicantInputFieldsCtr[0].text;
      } catch (e) {}
      try {
        customerEmail2 = listOApplicantInputFieldsCtr[1].text;
      } catch (e) {}
      try {
        customerEmail3 = listOApplicantInputFieldsCtr[2].text;
      } catch (e) {}

      final paramMortgageCaseInfoEntity = {
        "UserId": userData.userModel.id.toString(),
        "CompanyId": userData.userModel.userCompanyID,
        "TaskId": 0,
        "Status": 0,
        "CreationDate": "17-Oct-2020",
        "UpdatedDate": "17-Oct-2020",
        "VersionNumber": 0,
        "CaseType": "",
        "CustomerType": "",
        "IsSmoker": "No",
        "Remarks": "",
        "IsAnyOthers": (isOtherApplicantSwitch) ? "Yes" : "No",
        "CustomerEmail1": customerEmail1,
        "CustomerEmail2": customerEmail2,
        "CustomerEmail3": customerEmail3,
        "IsCurrentProperty": "No",
        "CustomerName": userData.userModel.name,
        "CustomerEmail": userData.userModel.email,
        "CustomerMobileNumber": userData.userModel.mobileNumber,
        "CustomerAddress": (userData.userModel.address +
                ' ' +
                userData.userModel.addressLine1 +
                ' ' +
                userData.userModel.addressLine2 +
                ' ' +
                userData.userModel.addressLine3)
            .trim(),
        "CoapplicantUserId": 0,
        "AreYouBuyingThePropertyInNameOfASPV": (isSPVSwitch) ? "Yes" : "No",
        "CompanyName": compName.trim(),
        "RegisteredAddress": regAddr.trim(),
        "DateRegistered": regDate,
        "CompanyRegistrationNumber": regNo.trim(),
        "DateRegistered1": "",
        "DateRegistered2": "",
        "DateRegistered3": "",
        "AdminFee": 0,
        "AdminFeeWhenPayable": "",
        "AdviceFee": 0,
        "AdviceFeeWhenPayable": "",
        "IsFeesRefundable": "",
        "FeesRefundable": ""
      };

      return {
        "Status": 903,
        "Title": title,
        "Description": note,
        "IsInPersonOrOnline": false,
        "DutDateType": 0,
        "DeliveryDate": "17-Oct-2020",
        "DeliveryTime": "",
        "WorkerNumber": 1,
        "Skill": "",
        "IsFixedPrice": true,
        "HourlyRate": 0,
        "FixedBudgetAmount": 0,
        "NetTotalAmount": 0,
        "JobCategory": "Regular",
        "PreferedLocation": "",
        "Requirements": "",
        "TotalHours": 0,
        "Latitude": 0,
        "Longitude": 0,
        "TaskReferenceNumber": "",
        "ReferenceTaskerId": 0,
        "MortgageCaseInfoEntityModelList": paramMortgageCaseInfoEntityModelList,
        "UserId": userData.userModel.id.toString(),
        "EntityId": userData.userModel.userCompanyInfo.entityID,
        "EntityName": userData.userModel.userCompanyInfo.entityName,
        "CompanyId": userData.userModel.userCompanyID,
        "MortgageCaseInfoEntity": paramMortgageCaseInfoEntity,
      };
    } catch (e) {}
  }
}
