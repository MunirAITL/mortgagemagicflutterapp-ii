import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_newcase/LocationsModel.dart';
import 'package:flutter/material.dart';

class EditCaseHelper {
  getParam({
    LocationsModel caseModel,
    bool isOtherApplicantSwitch,
    bool isSPVSwitch,
    List<TextEditingController> listOApplicantInputFieldsCtr,
    String compName,
    String regAddr,
    String regDate,
    String regNo,
    String title,
    String note,
  }) {
    try {
      final paramMortgageCaseInfoEntityModelList = [];
      String customerEmail1 = "";
      String customerEmail2 = "";
      String customerEmail3 = "";
      try {
        customerEmail1 = listOApplicantInputFieldsCtr[0].text;
      } catch (e) {}
      try {
        customerEmail2 = listOApplicantInputFieldsCtr[1].text;
      } catch (e) {}
      try {
        customerEmail3 = listOApplicantInputFieldsCtr[2].text;
      } catch (e) {}

      final paramMortgageCaseInfoEntity = {
        "UserId": userData.userModel.id.toString(),
        "CompanyId": userData.userModel.userCompanyID,
        "TaskId": caseModel.entityID,
        "Status": 0,
        "CreationDate": caseModel.creationDate,
        "UpdatedDate": caseModel.updatedDate,
        "VersionNumber": 0,
        "CaseType": "",
        "CustomerType": "",
        "IsSmoker": "No",
        "Remarks": "",
        "IsAnyOthers": (isOtherApplicantSwitch) ? "Yes" : "No",
        "CustomerEmail1": customerEmail1,
        "CustomerEmail2": customerEmail2,
        "CustomerEmail3": customerEmail3,
        "IsCurrentProperty": "No",
        "CustomerName": userData.userModel.name,
        "CustomerEmail": userData.userModel.email,
        "CustomerMobileNumber": userData.userModel.mobileNumber,
        "CustomerAddress": (userData.userModel.address +
                ' ' +
                userData.userModel.addressLine1 +
                ' ' +
                userData.userModel.addressLine2 +
                ' ' +
                userData.userModel.addressLine3)
            .trim(),
        "CoapplicantUserId": 0,
        "AreYouBuyingThePropertyInNameOfASPV": (isSPVSwitch) ? "Yes" : "No",
        "CompanyName": compName.trim(),
        "RegisteredAddress": regAddr.trim(),
        "DateRegistered": regDate,
        "CompanyRegistrationNumber": regNo.trim(),
        "DateRegistered1": "",
        "DateRegistered2": "",
        "DateRegistered3": "",
        "AdminFee": 0,
        "AdminFeeWhenPayable": "",
        "AdviceFee": 0,
        "AdviceFeeWhenPayable": "",
        "IsFeesRefundable": "",
        "FeesRefundable": ""
      };

      return {
        "Id": caseModel.id,
        "Status": caseModel.status,
        "Title": title,
        "Description": note,
        "IsInPersonOrOnline": caseModel.isInPersonOrOnline,
        "DutDateType": caseModel.dutDateType,
        "DeliveryDate": caseModel.deliveryDate,
        "DeliveryTime": caseModel.deliveryTime,
        "WorkerNumber": caseModel.workerNumber,
        "Skill": caseModel.skill,
        "IsFixedPrice": caseModel.isFixedPrice,
        "HourlyRate": caseModel.hourlyRate,
        "FixedBudgetAmount": caseModel.fixedBudgetAmount,
        "NetTotalAmount": caseModel.netTotalAmount,
        "JobCategory": caseModel.jobCategory,
        "PreferedLocation": caseModel.preferedLocation,
        "Requirements": caseModel.requirements,
        "TotalHours": caseModel.totalHours,
        "Latitude": caseModel.latitude,
        "Longitude": caseModel.longitude,
        "TaskReferenceNumber": caseModel.taskReferenceNumber,
        "ReferenceTaskerId": caseModel.taskReferenceNumber,
        "MortgageCaseInfoEntityModelList": paramMortgageCaseInfoEntityModelList,
        "UserId": userData.userModel.id.toString(),
        "EntityId": userData.userModel.userCompanyInfo.entityID,
        "EntityName": userData.userModel.userCompanyInfo.entityName,
        "CompanyId": userData.userModel.userCompanyID,
        "MortgageCaseInfoEntity": paramMortgageCaseInfoEntity,
      };
    } catch (e) {}
  }
}
