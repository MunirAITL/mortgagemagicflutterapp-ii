import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/UserNotesCfg.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_newcase/UserNotesModel.dart';

class UserNotesHelper {
  getUrl({pageStart = 0, pageCount = 50, status}) {
    var url = Server.USERNOTEBYENTITY_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", status);
    url = url.replaceAll("#entityId#", "0");
    url = url.replaceAll("#entityName#",
        "Case"); //userModel.userCompanyInfo.entityName); //"Case");
    return url;
  }

  getParam({
    // UserModel userModel,
    UserNotesModel userNoteModel,
  }) {
    return {
      "UserId": userNoteModel.userId,
      "User": null,
      "Status": UserNotesCfg.DONE,
      "CreationDate": DateTime.now().toString(),
      "Title": "Appointment for Mortgage Opportunity",
      "Comments": "We have a meeting via ZOOM",
      "ServiceDate": DateTime.now().toString(),
      "IsTag": false,
      "InitiatorId": userNoteModel.initiatorId,
      "InitiatorName": userNoteModel.initiatorName,
      "OwnerName": userNoteModel.ownerName,
      "OwnerEmail": userNoteModel.ownerEmail,
      "ProfileImageUrl": userNoteModel.profileImageUrl,
      "Type": "",
      "LeadId": 0,
      "Priority": "High",
      "Category": "Task",
      "ComplainMode": "",
      "NoteCategory": null,
      "ServiceEndDate": DateTime.now().toString(),
      "AdviserId": userNoteModel.adviserId,
      "AdviserName": userNoteModel.adviserName,
      "DirectionType": null,
      "ResolutionsUrl": null,
      "EntityId": userNoteModel.entityId,
      "EntityName": userNoteModel.entityName, //"Case",
      "Id": userNoteModel.id
    };
  }
}
