import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:flutter/material.dart';

class NewCaseHelper {
  getUrl({pageStart, pageCount, status}) {
    var url = Server.NEWCASE_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", status);
    url = url.replaceAll(
        "#userCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }

  //All = 901
  //In-Progress = 902 ==> CaseStatus Color= Yellow
  //Submitted = 903 ==> CaseStatus Color= Grey
  //FMA Submitted = 904 ==> CaseStatus Color= ______
  //Completed = 905
  Color getCaseStatusColor(int code) {
    switch (code) {
      case NewCaseCfg.IN_PROGRESS:
        return Colors.yellow;
        break;
      case NewCaseCfg.SUBMITTED:
        return Colors.grey;
        break;
      case NewCaseCfg.FMA_SUBMITTED:
        return Colors.blueAccent;
        break;
      case NewCaseCfg.COMPLETED:
        return Colors.green;
        break;
      default:
        return Colors.yellow;
    }
  }

  getCreateCaseIconByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map["url"];
        }
      }
    } catch (e) {}
    return null;
  }

  getCaseByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map;
        }
      }
    } catch (e) {}
    return null;
  }
}
