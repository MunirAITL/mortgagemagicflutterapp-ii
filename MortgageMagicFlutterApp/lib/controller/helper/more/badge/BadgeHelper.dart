import 'package:aitl/Mixin.dart';
import 'package:aitl/model/json/tab_more/badge/UserBadgeModel.dart';

enum BadgeType {
  Email,
  NationalIDCard,
  NationalIDCard2,
  CustomerPrivacy,
  Mobile
}

extension ParseToString on BadgeType {
  String toShortString() {
    return this.toString().split('.').last;
  }
}

class BadgeHelper with Mixin {
  UserBadgeModel getBadgeByType(
      List<UserBadgeModel> listUserBadgeModel, BadgeType badgeType) {
    try {
      for (UserBadgeModel model in listUserBadgeModel) {
        try {
          if (model.type == badgeType.toShortString()) {
            return model;
          }
        } catch (e) {}
      }
    } catch (e) {}
    return null;
  }

  List<UserBadgeModel> updateListBadgeAPIModel(
      List<UserBadgeModel> listUserBadgeModel,
      UserBadgeModel userBadgeModel,
      BadgeType badgeType) {
    List<UserBadgeModel> listUserBadgeModel2 = [];
    try {
      listUserBadgeModel2.add(userBadgeModel);
      for (UserBadgeModel model in listUserBadgeModel) {
        try {
          if (model.type != badgeType.toShortString()) {
            listUserBadgeModel2.add(model);
          }
        } catch (e) {}
      }
    } catch (e) {}

    return listUserBadgeModel2;
  }
}
