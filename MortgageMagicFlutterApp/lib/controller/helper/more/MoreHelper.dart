import 'package:aitl/Mixin.dart';
import 'package:aitl/view/dashboard/more/badges/BadgeScreen.dart';
import 'package:aitl/view/dashboard/more/help/HelpScreen.dart';
import 'package:aitl/view/dashboard/more/profile/ProfileScreen.dart';
import 'package:aitl/view/dashboard/more/reviews/ReviewsScreen.dart';
import 'package:aitl/view/dashboard/more/settings/SettingsScreen.dart';
import 'package:aitl/view/dashboard/noti/NotiTab.dart';
import 'package:flutter/material.dart';

class MoreHelper with Mixin {
  static const List<Map<String, dynamic>> listMore = [
    {"title": "Profile", "route": ProfileScreen},
    //{"title": "Reviews", "route": ReviewsScreen},
    //{"title": "Badges", "route": BadgeScreen},
    {"title": "Notifications", "route": NotiTab},
    {"title": "Settings", "route": SettingsScreen},
    {"title": "Help", "route": HelpScreen},
    {"title": "Logout", "route": null},
  ];

  setRoute({
    Type route,
    BuildContext context,
    Function callback,
  }) async {
    try {
      if (identical(route, ProfileScreen)) {
        navTo(context: context, page: () => ProfileScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, BadgeScreen)) {
        navTo(context: context, page: () => BadgeScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, ReviewsScreen)) {
        navTo(context: context, page: () => ReviewsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, SettingsScreen)) {
        navTo(context: context, page: () => SettingsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, HelpScreen)) {
        navTo(context: context, page: () => HelpScreen()).then((value) {
          callback(route);
        });
      }
    } catch (e) {}
  }
}
