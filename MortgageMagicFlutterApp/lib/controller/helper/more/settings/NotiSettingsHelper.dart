import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/more/settings/NotiSettingsCfg.dart';
import 'package:aitl/model/data/UserData.dart';

class NotiSettingsHelper {
  getUrl() {
    var url = Server.NOTI_SETTINGS_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#userCompanyId#", userData.userModel.userCompanyID.toString());
    //print(url);
    return url;
  }

  getParam({
    NotiSettingsCfg notiSettingsCfg,
    String category = "User",
  }) {
    final caseUpdateMap = notiSettingsCfg.listOpt[0];
    final caseRecomendationMap = notiSettingsCfg.listOpt[1];
    final caseCompletedMap = notiSettingsCfg.listOpt[2];
    final caseReminderMap = notiSettingsCfg.listOpt[3];
    final userUpdateMap = notiSettingsCfg.listOpt[4];
    final helpfulInfoMap = notiSettingsCfg.listOpt[5];
    final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

    /*final transactionalMap = notiSettingsCfg.listOpt[7];
    final leadUpdateMap = notiSettingsCfg.listOpt[8];
    final helpFullMap = notiSettingsCfg.listOpt[9];*/

    return {
      "UserId": userData.userModel.id,
      "UserCompanyId": userData.userModel.userCompanyInfo.id,
      "Category": category,
      //
      //"IsTransactionalEmail": transactionalMap['isEmail'] as bool,
      //"IsTransactionalSMS": transactionalMap['isSms'] as bool,
      //"IsTransactionalNotification": transactionalMap['isPush'] as bool,
      //
      "IsCaseCompletedEmail": caseCompletedMap['isEmail'] as bool,
      "IsCaseCompletedSMS": caseCompletedMap['isSms'] as bool,
      "IsCaseCompletedNotification": caseCompletedMap['isPush'] as bool,
      //
      "IsCaseRecomendationEmail": caseRecomendationMap['isEmail'] as bool,
      "IsCaseRecomendationSMS": caseRecomendationMap['isSms'] as bool,
      "IsCaseRecomendationNotification": caseRecomendationMap['isPush'] as bool,
      //
      "IsCaseUpdateEmail": caseUpdateMap['isEmail'] as bool,
      "IsCaseUpdateSMS": caseUpdateMap['isSms'] as bool,
      "IsCaseUpdateNotification": caseUpdateMap['isPush'] as bool,
      //
      "IsCaseReminderEmail": caseReminderMap['isEmail'] as bool,
      "IsCaseReminderSMS": caseReminderMap['isSms'] as bool,
      "IsCaseReminderNotification": caseReminderMap['isPush'] as bool,
      //
      "IsUserUpdateEmail": userUpdateMap['isEmail'] as bool,
      "IsUserUpdateSMS": userUpdateMap['isSms'] as bool,
      "IsUserUpdateNotification": userUpdateMap['isPush'] as bool,
      //
      "IsHelpfulInformationEmail": helpfulInfoMap['isEmail'] as bool,
      "IsHelpfulInformationSMS": helpfulInfoMap['isSms'] as bool,
      "IsHelpfulInformationNotification": helpfulInfoMap['isPush'] as bool,
      //
      "IsUpdateAndNewsLetterEmail": updateNewsLetterMap['isEmail'] as bool,
      "IsUpdateAndNewsLetterSMS": updateNewsLetterMap['isSms'] as bool,
      "IsUpdateAndNewsLetterNotification":
          updateNewsLetterMap['isPush'] as bool,
      //
      //"IsHelpFullEmail": helpFullMap['isEmail'] as bool,
      //"IsHelpFullSMS": helpFullMap['isSms'] as bool,
      //"IsHelpFullNotification": helpFullMap['isPush'] as bool,
      //
      //"IsLeadUpdateEmail": leadUpdateMap['isEmail'] as bool,
      //"IsLeadUpdateSMS": leadUpdateMap['isSms'] as bool,
      //"IsLeadUpdateNotification": leadUpdateMap['isPush'] as bool,
      //
      "Id": userData.userModel.id,
    };
  }

  /*setSettings(List<dynamic> list) async {
    try {
      List<String> list2 = list.map((e) => json.encode(e)).toList();
      SharedPreferences pref = await SharedPreferences.getInstance();
      await pref.setStringList("NotiSettingsCfg", list2);
    } catch (e) {}
  }

  Future<dynamic> getSettings() async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      var result = pref.getStringList("NotiSettingsCfg");
      if (result != null) {
        return result.map((e) => json.decode(e)).toList();
      }
    } catch (e) {}
    return null;
  }*/
}
