class HelpTutHelper {
  List<String> listTips = [
    "To request a Mortgage Case, tap Create Case Icon",
    "To see all your Cases, tap on the My cases icon",
    "To view your Messages, tap on the Messages icon",
    "To view your Notifications, tap on the Notifications icon",
    "Tap on More icon to manage your profile and other settings"
  ];

  List<String> listTab = [
    "Create Case",
    "My Cases",
    "Messages",
    "Notifications",
    "More"
  ];
}
