import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class TimeLineMessagesHelper {
  getUrl({
    pageStart,
    pageCount,
    taskId,
    isPrivate,
    customerId,
    receiverId,
    timeLineId,
  }) {
    var url = Server.TIMELINE_URL;
    url = url.replaceAll("#isPrivate#", isPrivate.toString());
    url = url.replaceAll("#receiverId#", receiverId.toString());
    url = url.replaceAll("#senderId#", userData.userModel.id.toString());
    url = url.replaceAll("#taskId#", taskId.toString());
    url = url.replaceAll("#Count#", pageCount.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#customerId#", customerId.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#timeLineId#", timeLineId.toString());
    return url;
  }
}
