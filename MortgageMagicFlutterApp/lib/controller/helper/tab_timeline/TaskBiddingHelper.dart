import 'package:aitl/config/Server.dart';

class TaskBiddingHelper {
  getUrl({pageStart, pageCount, taskId}) {
    var url = Server.TASKBIDDING_URL;
    url = url.replaceAll("#taskId#", taskId.toString());
    return url;
  }
}
