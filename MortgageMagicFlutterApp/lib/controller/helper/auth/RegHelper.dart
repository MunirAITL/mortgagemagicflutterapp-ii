class RegHelper {
  getParam({
    String email,
    String pwd,
    String fname = '',
    String lname = '',
    String fullName = '',
    String phone,
    String dob = '',
    String dobDD = '',
    String dobMM = '',
    String dobYY = '',
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "FirstName": fname,
      "LastName": lname,
      "MobileNumber": phone,
      "CommunityId": "1",
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": dobDD,
      "BirthMonth": dobMM,
      "BirthYear": dobYY,
      "UserCompanyId": 1003,
      "dialCode": "880",
      "ConfirmPassword": pwd,
      "confirmPassword": pwd,
      "DateofBirth": dob,
    };
  }
}
