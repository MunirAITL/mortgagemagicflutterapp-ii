import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/helper/tab_timeline/TimeLineAdvisorHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:flutter/cupertino.dart';

class TimelineAPIMgr with Mixin {
  static final TimelineAPIMgr _shared = TimelineAPIMgr._internal();

  factory TimelineAPIMgr() {
    return _shared;
  }

  TimelineAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    int status,
    Function(TimeLineAdvisorAPIModel) callback,
  }) async {
    try {
      final url = TimeLineAdvisorHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
      );
      log(url);
      await NetworkMgr()
          .req<TimeLineAdvisorAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
