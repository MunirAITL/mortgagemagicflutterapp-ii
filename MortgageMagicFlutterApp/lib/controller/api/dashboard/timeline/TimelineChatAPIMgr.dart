import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/tab_timeline/TimeLineMessagesHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:flutter/cupertino.dart';

class TimelineChatAPIMgr with Mixin {
  static final TimelineChatAPIMgr _shared = TimelineChatAPIMgr._internal();

  factory TimelineChatAPIMgr() {
    return _shared;
  }

  TimelineChatAPIMgr._internal();

  wsPostTimelineAPI({
    BuildContext context,
    dynamic param,
    Function(TimeLinePostAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<TimeLinePostAPIModel, Null>(
        context: context,
        url: Server.TIMELINE_POST_URL,
        isLoading: false,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsOnPageLoad({
    BuildContext context,
    int startPage,
    int pageCount,
    int taskId,
    int taskBiddingUserId,
    Function(TimeLineAPIModel) callback,
  }) async {
    try {
      var url = TimeLineMessagesHelper().getUrl(
        pageStart: startPage,
        pageCount: pageCount,
        taskId: taskId,
        isPrivate: true,
        customerId: 0,
        receiverId: taskBiddingUserId.toString(),
        timeLineId: 0,
      );
      log(url);
      await NetworkMgr()
          .req<TimeLineAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
