import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/tab_noti/NotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_noti/NotiAPIModel.dart';
import 'package:flutter/cupertino.dart';

class NotiAPIMgr with Mixin {
  static final NotiAPIMgr _shared = NotiAPIMgr._internal();

  factory NotiAPIMgr() {
    return _shared;
  }

  NotiAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    Function(NotiAPIModel) callback,
  }) async {
    try {
      final url =
          NotiHelper().getUrl(pageStart: pageStart, pageCount: pageCount);
      log(url);
      await NetworkMgr()
          .req<NotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
