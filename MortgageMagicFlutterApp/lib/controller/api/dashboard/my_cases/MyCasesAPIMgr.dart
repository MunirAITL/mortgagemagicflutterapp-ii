import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:flutter/cupertino.dart';

class MyCasesAPIMgr with Mixin {
  static final MyCasesAPIMgr _shared = MyCasesAPIMgr._internal();

  factory MyCasesAPIMgr() {
    return _shared;
  }

  MyCasesAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    int caseStatus,
    Function(TaskInfoSearchAPIModel) callback,
  }) async {
    try {
      final url = NewCaseHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus.toString(),
      );
      log(url);
      await NetworkMgr()
          .req<TaskInfoSearchAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
