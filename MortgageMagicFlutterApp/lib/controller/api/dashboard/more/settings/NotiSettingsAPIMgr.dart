import 'dart:convert';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/more/settings/NotiSettingsCfg.dart';
import 'package:aitl/controller/helper/more/settings/NotiSettingsHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:flutter/cupertino.dart';

class NotiSettingsAPIMgr with Mixin {
  static final NotiSettingsAPIMgr _shared = NotiSettingsAPIMgr._internal();

  factory NotiSettingsAPIMgr() {
    return _shared;
  }

  NotiSettingsAPIMgr._internal();

  wsPostNotificationSettingsAPI({
    BuildContext context,
    NotiSettingsCfg notiSettingsCfg,
    Function(NotiSettingsPostAPIModel) callback,
  }) async {
    try {
      final param = NotiSettingsHelper().getParam(
        notiSettingsCfg: notiSettingsCfg,
      );
      log(json.encode(param));
      await NetworkMgr()
          .req<NotiSettingsPostAPIModel, Null>(
        context: context,
        url: Server.NOTI_SETTINGS_POST_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  wsGetNotificationSettingsAPI({
    BuildContext context,
    Function(NotiSettingsAPIModel) callback,
  }) async {
    try {
      final url = NotiSettingsHelper().getUrl();
      log(url);
      await NetworkMgr()
          .req<NotiSettingsAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
