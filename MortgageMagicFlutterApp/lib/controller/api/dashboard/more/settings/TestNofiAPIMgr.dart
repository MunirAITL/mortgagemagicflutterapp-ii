import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/more/settings/FcmTestNotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:flutter/cupertino.dart';

class TestNotiAPIMgr with Mixin {
  static final TestNotiAPIMgr _shared = TestNotiAPIMgr._internal();

  factory TestNotiAPIMgr() {
    return _shared;
  }

  TestNotiAPIMgr._internal();

  wsTestNotiAPI({
    BuildContext context,
    Function(FcmTestNotiAPIModel) callback,
  }) async {
    try {
      final url = FcmTestNotiHelper().getUrl(
        userId: userData.userModel.id,
      );
      log(url);
      await NetworkMgr()
          .req<FcmTestNotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: true,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
