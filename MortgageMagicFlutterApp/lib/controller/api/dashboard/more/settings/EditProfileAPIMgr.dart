import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:flutter/cupertino.dart';

class EditProfileAPIMgr with Mixin {
  static final EditProfileAPIMgr _shared = EditProfileAPIMgr._internal();

  factory EditProfileAPIMgr() {
    return _shared;
  }

  EditProfileAPIMgr._internal();

  wsUpdateProfileAPI({
    BuildContext context,
    dynamic param,
    Function(UserProfileAPIModel) callback,
  }) async {
    await NetworkMgr()
        .req<UserProfileAPIModel, Null>(
      context: context,
      url: Server.EDIT_PROFILE_URL,
      reqType: ReqType.Put,
      param: param,
    )
        .then((model) async {
      callback(model);
    });
  }

  wsDeactivateProfileAPI({
    BuildContext context,
    String reason,
    Function(DeactivateProfileAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<DeactivateProfileAPIModel, Null>(
        context: context,
        url: Server.DEACTIVATE_PROFILE_URL,
        param: {'id': userData.userModel.id, 'CancelReason': reason},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
