import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_more/settings/settings/ChangePwdAPIModel.dart';
import 'package:flutter/cupertino.dart';

class ChangePwdAPIMgr with Mixin {
  static final ChangePwdAPIMgr _shared = ChangePwdAPIMgr._internal();

  factory ChangePwdAPIMgr() {
    return _shared;
  }

  ChangePwdAPIMgr._internal();

  wsChangePwdAPI({
    BuildContext context,
    String curPwd,
    String newPwd,
    String newPwd2,
    Function(ChangePwdAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<ChangePwdAPIModel, Null>(
        context: context,
        url: Server.CHANGE_PWD_URL,
        reqType: ReqType.Put,
        param: {
          'CurrentPassword': curPwd,
          'password': newPwd,
          'confirmPassword': newPwd2,
        },
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
