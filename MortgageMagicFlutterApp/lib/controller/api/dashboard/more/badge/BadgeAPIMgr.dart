import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/more/badge/BadgeEmailHelper.dart';
import 'package:aitl/controller/helper/more/badge/BadgePhotoIDHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:flutter/material.dart';

class BadgeAPIMgr with Mixin {
  static final BadgeAPIMgr _shared = BadgeAPIMgr._internal();

  factory BadgeAPIMgr() {
    return _shared;
  }

  BadgeAPIMgr._internal();

  wsGetUserBadge(
      {BuildContext context,
      int userId,
      Function(BadgeAPIModel) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgeAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url:
            Server.BADGE_USER_GET_URL.replaceAll("#userId#", userId.toString()),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsPostEmailBadge(
      {BuildContext context,
      Function(BadgeEmailAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgeEmailAPIModel, Null>(
        context: context,
        url: Server.BADGE_EMAIL_URL,
        param: BadgeEmailHelper().getParam(),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsPostPhotoIDBadge(
      {BuildContext context,
      String refrenceUrl,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url: Server.BADGE_PHOTOID_URL,
        param: BadgePhotoIDHelper().getParam(refrenceUrl: refrenceUrl),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsDelPhotoIDBadge(
      {BuildContext context,
      int id,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr().req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url:
            Server.BADGE_PHOTOID_DEL_URL.replaceAll("#badgeId#", id.toString()),
        reqType: ReqType.Delete,
        param: {},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
