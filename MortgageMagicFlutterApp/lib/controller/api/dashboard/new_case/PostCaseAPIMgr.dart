import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_newcase/PostCaseAPIModel.dart';
import 'package:flutter/cupertino.dart';

class PostCaseAPIMgr with Mixin {
  static final PostCaseAPIMgr _shared = PostCaseAPIMgr._internal();

  factory PostCaseAPIMgr() {
    return _shared;
  }

  PostCaseAPIMgr._internal();

  wsOnPostCase({
    BuildContext context,
    dynamic param,
    Function(PostCaseAPIModel) callback,
  }) async {
    try {
      log(param);
      await NetworkMgr()
          .req<PostCaseAPIModel, Null>(
        context: context,
        url: Server.POSTCASE_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
