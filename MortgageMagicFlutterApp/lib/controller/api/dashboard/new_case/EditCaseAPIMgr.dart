import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_newcase/EditCaseAPIModel.dart';
import 'package:flutter/cupertino.dart';

class EditCaseAPIMgr with Mixin {
  static final EditCaseAPIMgr _shared = EditCaseAPIMgr._internal();

  factory EditCaseAPIMgr() {
    return _shared;
  }

  EditCaseAPIMgr._internal();

  wsOnPutCase({
    BuildContext context,
    dynamic param,
    Function(EditCaseAPIModel) callback,
  }) async {
    try {
      log(param);
      await NetworkMgr()
          .req<EditCaseAPIModel, Null>(
        context: context,
        url: Server.EDITCASE_URL,
        reqType: ReqType.Put,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
