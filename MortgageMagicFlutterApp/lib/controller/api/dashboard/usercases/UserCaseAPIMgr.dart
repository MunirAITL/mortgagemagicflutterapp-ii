import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/tab_newcase/UserNotesHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:flutter/cupertino.dart';

class UserCaseAPIMgr with Mixin {
  static final UserCaseAPIMgr _shared = UserCaseAPIMgr._internal();

  factory UserCaseAPIMgr() {
    return _shared;
  }

  UserCaseAPIMgr._internal();

  wsUserNotesAPI({
    BuildContext context,
    int status,
    Function(UserNoteByEntityAPIModel) callback,
  }) async {
    try {
      final url = UserNotesHelper().getUrl(
        status: status.toString(),
      );
      log(url);
      await NetworkMgr()
          .req<UserNoteByEntityAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
