import 'dart:async';
import 'dart:math';
import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:jiffy/jiffy.dart';
import 'controller/classes/GpsMgr.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:aitl/view/mywidgets/com/AlrtDialog.dart';

mixin Mixin {
  getW(context) {
    return ResponsiveFlutter.of(context).wp(100);
  }

  getH(context) {
    return ResponsiveFlutter.of(context).hp(100);
  }

  getWP(context, p) {
    return ResponsiveFlutter.of(context).wp(p);
  }

  getHP(context, p) {
    return ResponsiveFlutter.of(context).hp(p);
  }

  getTxtSize({BuildContext context, double txtSize = 0}) {
    if (txtSize == 0) txtSize = MyTheme.txtSize;
    return ResponsiveFlutter.of(context).fontSize(txtSize);
  }

  navTo({
    BuildContext context,
    Widget Function() page,
    isRep = false,
    isFullscreenDialog = false,
  }) {
    if (!isRep) {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    } else {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    }
  }

  //  get user current location
  Future<Position> getLocation() async {
    Position pos;
    final isGPSPermission = await GpsMgr().requestLocationPermission();
    if (isGPSPermission) {
      pos = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    }
    return pos;
  }

  openUrl(context, url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Alert(context: context, title: "Alert", desc: 'Could not launch $url')
          .show();
    }
  }

  openMap(context, double latitude, double longitude) async {
    openUrl(context,
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude');
  }

  Iterable<E> mapIndexed<E, T>(
      Iterable<T> items, E Function(int index, T item) f) sync* {
    var index = 0;
    for (final item in items) {
      yield f(index, item);
      index = index + 1;
    }
  }

  getTimeAgoTxt(dt) {
    try {
      final t = Jiffy(dt).fromNow();
      print(t);
      return t;
    } catch (e) {
      print(e.toString());
    }
    return dt;
  }

  void startLoading() {
    //EasyLoading.instance
    //..loadingStyle = EasyLoadingStyle.light
    //..backgroundColor = Colors.white
    //..radius = 20.0;
    EasyLoading.show(status: "Loading...");
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  void showToast({msg, isToast = false, which = 1}) {
    //EasyLoading.instance..loadingStyle = EasyLoadingStyle.dark;
    //  0 = error,
    //  1= success,
    //  2= info
    //  3= default toast

    /*switch (which) {
      case 0:
        EasyLoading.showError(msg);
        break;
      case 1:
        EasyLoading.showSuccess(msg);
        break;
      case 2:
        EasyLoading.showInfo(msg);
        break;
      default:
        EasyLoading.showInfo(msg);
    }*/

    if (isToast) {
      EasyLoading.showInfo(msg);
    } else {
      Get.defaultDialog(
          onConfirm: () {
            Get.back();
          },
          middleText: msg);

      Get.dialog(AlrtDialog(
        which: which,
        msg: msg,
      ));
    }
  }

  void showSnake(String title, String msg) {
    Get.snackbar(title, msg);
  }

  showAlert({msg, which = 2}) async {
    //  0 = error = AlertType.error
    //  1= success,
    //  2= info
    /*var t = AlertType.info;
    switch (which) {
      case 0:
        t = AlertType.error;
        break;
      case 1:
        t = AlertType.success;
        break;
      case 2:
        t = AlertType.info;
        break;
      default:
        t = AlertType.info;
        break;
    }
    Alert(
      context: context,
      type: t,
      title: "",
      desc: msg,
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();*/

    Get.defaultDialog(
        onConfirm: () {
          Get.back();
        },
        middleText: msg);
  }

  showAlertErr({context, title, msg, isServerErr = false}) async {
    if (msg != null) {
      //var geocode = await getGeoCode(true);
      sendMail(context, runtimeType.toString() + '-' + title, msg);
    }
  }

  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      print(str);
      //final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      //pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }

  getRand(list) {
    final _random = new Random();
    return list[_random.nextInt(list.length)];
  }

  sendMail(context, subject, msg) async {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {});
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

extension CapExtension on String {
  String get inCaps =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.toLowerCase().split(' ').map((word) {
        String leftText =
            (word.length > 1) ? word.substring(1, word.length) : '';
        return word[0].toUpperCase() + leftText;
      }).join(' ');
}
