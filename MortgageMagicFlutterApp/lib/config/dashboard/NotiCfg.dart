import 'package:aitl/view/dashboard/timeline/TimeLineTab.dart';
import 'package:aitl/view/dashboard/timeline/chat/TaskBiddingScreen.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';

class NotiCfg {
  static const List<Map<String, dynamic>> EVENT_LIST = [
    {
      "communityId": 1,
      "entityName": "Task",
      "txt": "#EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "UserRegistration",
      "txt": "#EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoCaseNormalSubmission",
      "txt": "#EventName#  has been submitted",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoRecomendation",
      "txt": "#EventName#  has been Recommended",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoAIPObtained",
      "txt": "#EventName#  has been AIP Obtained",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoSubmitted",
      "txt": "#EventName#  has been FMA submitted",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoInProgress",
      "txt": "#EventName#  has been updated by In-progress",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoOffered",
      "txt": "#EventName# has been offered",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoValuationSatisfied",
      "txt": "#EventName#  has been valuation satisfied",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoCompleted",
      "txt": "#EventName#  has been completed",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoDeclined",
      "txt": "#EventName#  has been declined",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoCancelled",
      "txt": "#EventName#  has been cancelled",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CasePaymentInfoValuationInstructed",
      "txt": "#EventName#  has been valuation instructed",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CaseAdviserAssigned",
      "txt": "#EventName#  Adviser has been assigned",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "CaseIntroducerAssigned",
      "txt": "#EventName# Introducer has been assigned",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "TaskBidding",
      "txt":
          "#InitiatorDisplayName# has assigned an Adviser to your case on #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": TaskBiddingScreen,
    },
    {
      "communityId": 1,
      "entityName": "TaskBiddingAccepted",
      "txt": "#EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": TaskBiddingScreen,
    },
    {
      "communityId": 1,
      "entityName": "UserRating",
      "txt":
          "#InitiatorDisplayName# gave you rating & review for case. #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": WebScreen,
    },
    {
      "communityId": 1,
      "entityName": "TaskBiddingComment",
      "txt": "#InitiatorDisplayName# sent message on case. #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 1011,
      "route": TimeLineTab,
    },
    {
      "communityId": 1,
      "entityName": "TaskBiddingAdminComment",
      "txt": "#InitiatorDisplayName# sent message on case. #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 1012,
      "route": TaskBiddingScreen,
    },
    {
      "communityId": 2,
      "entityName": "TaskBiddingComment",
      "txt": "#InitiatorDisplayName# sent message on case. #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": TaskBiddingScreen,
    },
    {
      "communityId": 2,
      "entityName": "TaskBiddingAdminComment",
      "txt": "#InitiatorDisplayName# sent message on case. #EventName#",
      "desc": "Financial Adviser",
      "notificationEventId": 0,
      "route": TaskBiddingScreen,
    },
  ];
}
