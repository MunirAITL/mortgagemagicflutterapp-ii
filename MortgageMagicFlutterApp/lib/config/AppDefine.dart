class AppDefine {
  //  DEFINE STUFF START HERE...
  static const APP_NAME = "MortgageMagic";
  static const SUPPORT_EMAIL = "info@mortgage-magic.co.uk";
  static const SUPPORT_CALL = "0333 888 0245";
  static const COUNTRY_CODE = "+44";
}
