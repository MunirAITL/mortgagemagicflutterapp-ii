class Server {
  static const bool isTest = true;

  //  BaseUrl
  static const String BASE_IP = "158.176.138.61";
  static const String BASE_URL = "https://app.mortgage-magic.co.uk";

  //  Device Info & APNS Stuff
  static const String FCM_DEVICE_INFO_URL =
      BASE_URL + "/api/fcmdeviceinfo/post";

  //  Company Acount
  static const String COMP_ACC_GET_URL =
      BASE_URL + "/api/users/get/getcompanyaccountsbyuserid?UserId=#userId#";

  //  Login
  static const String LOGIN_URL = BASE_URL + "/api/authentication/login";

  //  Login by Mobile OTP
  static const String LOGIN_MOBILE_OTP_POST_URL =
      BASE_URL + "/api/userotp/post";
  static const String SEND_OTP_NOTI_URL =
      BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";
  static const String LOGIN_MOBILE_OTP_PUT_URL = BASE_URL + "/api/userotp/put";
  static const String LOGIN_REG_OTP_FB_URL =
      BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Login by FB Native

  //  Login by Gmail Native

  //  Login by Apple Native

  //  Forgot
  static const String FORGOT_URL =
      BASE_URL + "/api/authentication/forgotpassword";

  // Change password
  static const String CHANGE_PWD_URL =
      BASE_URL + "/api/users/put/change-password";

  //  EmailVerify
  static const String EMAILVERIFY_URL =
      BASE_URL + "/api/authentication/verifyemail";

  //  Register
  static const String REG_URL = BASE_URL + "/api/authentication/register";

  //  Deactivate Profile
  static const String DEACTIVATE_PROFILE_URL =
      BASE_URL + "/api/users/deactivebyuserownerbyreason";

  //  Dashboard Action Alert and Cases
  static const String USERNOTEBYENTITY_URL = BASE_URL +
      "/api/usernote/getusernotebyentityidandentitynameanduseridandstatus?EntityId=#entityId#&EntityName=#entityName#&Status=#status#&UserId=#userId#";

  //  New Case
  static const String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearch" +
      "/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#userCompanyId#";

  //  Post Case
  static const String POSTCASE_URL = BASE_URL + "/api/task/post";

  //  Edit Case
  static const String EDITCASE_URL = BASE_URL + "/api/task/put";

  //  Notification
  static const String NOTI_URL =
      BASE_URL + "/api/notifications/get?userId=#userId#";

  //  Timeline
  //static const String TIMELINE_URL = BASE_URL +
  // "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_ADVISOR_URL = BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforprivatemessage?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#companyId#";
  static const String TASKBIDDING_URL =
      BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static const String TIMELINE_URL = BASE_URL +
      "/api/timeline/gettimelinebyapp?Count=#count#&count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_POST_URL = BASE_URL + "/api/timeline/post";

  //  More::Help->Support->Send Email  + Attachments
  static const String MEDIA_UPLOADFILES_URL =
      BASE_URL + "/api/media/uploadpictures";
  static const String RESOLUTION_URL = BASE_URL + "/api/resolution/post";

  //  More::Settings->Edit Profile
  static const String EDIT_PROFILE_URL = BASE_URL + "/api/users/put";

  //  More::Settings->User Notification Settings
  //static const String NOTI_SETTINGS_URL =
  //BASE_URL + "/api/usernotificationsetting/get?userId=#userId#";
  static const String NOTI_SETTINGS_URL = BASE_URL +
      "/api/usernotificationsetting/getnotificationsettingbyusercompanyidanduserid?UserCompanyId=#userCompanyId#&UserId=#userId#";
  static const String NOTI_SETTINGS_POST_URL =
      BASE_URL + "/api/usernotificationsetting/post";
  static const String FCM_TEST_NOTI_URL =
      BASE_URL + "/api/notifications/sendtestpushnotificationtouser/#userId#";

  //  More::Badge
  static const String BADGE_USER_GET_URL =
      BASE_URL + "/api/userBadge/get?UserId=#userId#";
  static const String BADGE_EMAIL_URL =
      BASE_URL + "/api/userBadge/postemailbadge";
  static const String BADGE_PHOTOID_URL = BASE_URL + "/api/userBadge/post";
  static const String BADGE_PHOTOID_DEL_URL =
      BASE_URL + "/api/userBadge/delete/#badgeId#";

  //  WEBVIEW::
  //  Case Details WebView
  static const String CASEDETAILS_WEBVIEW_URL =
      BASE_URL + "/apps/about-me/#title#-#taskId#";
  static const String BASE_URL_NOTI_WEB = BASE_URL + "/apps/about-me";

  //  Misc
  static const String DOMAIN = "https://mortgage-magic.co.uk";
  static const String ABOUTUS_URL =
      "https://app.mortgage-magic.co.uk/apps/about-me/";
  static const String HELP_INFO_URL =
      "https://www.mortgagemagic.com/privacy-policy/";
  static const String TC_URL = "https://www.mortgagemagic.com/privacy-policy/";
  static const String PRIVACY_URL =
      "https://www.mortgagemagic.com/privacy-policy/";
}
