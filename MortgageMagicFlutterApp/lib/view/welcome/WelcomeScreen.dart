import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/services/PushNotificationService.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/dashboard/more/settings/CaseAlertScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/splash/SplashScreen.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();

    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      //SystemChrome.setSystemUIOverlayStyle(
      //SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      final pushNotificationService =
          PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(
          callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          navTo(context: context, isRep: true, page: () => DashBoardScreen())
              .then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        } else {
          setState(() {
            isDoneCookieCheck = true;
          });
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        FlutterRingtonePlayer.play(
          android: AndroidSounds.notification,
          ios: IosSounds.glass,
          looping: true, // Android only - API >= 28
          volume: 0.1, // Android only - API >= 28
          asAlarm: false, // Android only - all APIs
        );

        navTo(
            context: context,
            page: () => CaseAlertScreen(
                  message: message,
                )).then((value) async {
          await userData.setUserModel();
          navTo(context: context, isRep: true, page: () => DashBoardScreen())
              .then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.themeData.accentColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? SplashScreen()
                : Container(
                    //decoration: MyTheme.boxDeco,
                    height: getH(context),

                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          drawTopLogo(),
                          drawCenterImage(),
                          drawMMText(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 40, right: 40, top: 20),
                            child: MMBtn(
                                txt: "Let's Go",
                                height: getHP(context, 7),
                                width: getW(context),
                                callback: () {
                                  navTo(
                                      context: context,
                                      page: () => RegScreen());
                                }),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),
          )),
    );
  }

  drawTopLogo() {
    return Container(
      width: getW(context),
      //height: getHP(context, 35),
      child: Column(
        children: [
          SizedBox(height: getHP(context, 10)),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20),
            child: Txt(
              txt: "Welcome to",
              txtColor: MyTheme.welcomeTitleColor,
              txtSize: MyTheme.txtSize + 3,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
            child: RichText(
              text: new TextSpan(
                // Note: Styles for TextSpans must be explicitly defined.
                // Child text spans will inherit styles from parent

                children: <TextSpan>[
                  new TextSpan(
                      text: 'Mortgage ',
                      style: new TextStyle(
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + 1.2),
                          color: MyTheme.welcomeTitleColor,
                          fontWeight: FontWeight.bold)),
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'Magic',
                        style: new TextStyle(
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + 1.2),
                          color: MyTheme.redColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      WidgetSpan(
                        child: Transform.translate(
                          offset: Offset(-5, -22),
                          child: Text(
                            'TM',
                            //superscript is usually smaller in size
                            textScaleFactor: 0.5,
                            style: TextStyle(
                                color: MyTheme.redColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: getHP(context, 5)),
        ],
      ),
    );
  }

  drawCenterImage() {
    return Container(
      width: getWP(context, 60),
      height: getHP(context, 25),
      child: Image.asset(
        'assets/images/screens/welcome/welcome_theme.png',
        fit: BoxFit.cover,
      ),
    );
  }

  drawMMText() {
    return Container(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.only(
              left: getWP(context, 15),
              right: getWP(context, 15),
              top: 60,
              bottom: 20),
          child: Txt(
            txt:
                "We'll give you the best unbiased advice and sort your mortgage from start to finish to get you the best deal for FREE.",
            txtColor: MyTheme.timelineTitleColor,
            txtSize: MyTheme.txtSize + .35,
            txtAlign: TextAlign.center,
            isBold: false,
            //txtLineSpace: 1.5,
          ),
        ));
  }
}
