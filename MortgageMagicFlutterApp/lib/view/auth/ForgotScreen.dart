import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/ForgotAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../Mixin.dart';

class ForgotScreen extends StatefulWidget {
  @override
  State createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> with Mixin {
  final TextEditingController _email = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  FocusNode inputNode = FocusNode();
// to open keyboard call this function;
  void openKeyboard() {
    FocusScope.of(context).requestFocus(inputNode);
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text = "anisur5001@yopmail.com";
        //"anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
      }
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isEmpty(_email, 'Invalid Email')) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          titleSpacing: 0,
          title: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              width: 100,
              height: 60,
              child: Image.asset(
                "assets/images/icons/back_btn_icon.png",
              ),
            ),
          ),
          actions: [
            TextButton(
              //textColor: Colors.white,
              onPressed: () {
                navTo(context: context, isRep: true, page: () => RegScreen());
              },
              child: Txt(
                  txt: "Sign up",
                  txtColor: MyTheme.redColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              //shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                  child: Txt(
                    txt: "Forgot Password?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1.3,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Txt(
                    txt: "Please enter your email address",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Txt(
                    txt: "associated with your account.",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: drawInputBox(
                    title: "Email",
                    input: _email,
                    kbType: TextInputType.emailAddress,
                    len: 50,
                    autofocus: true,
                  ),
                ),
                /*Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: GestureDetector(
                      onTap: () {
                        navTo(
                            context: context,
                            isRep: true,
                            page: () => ChangePwdScreen());
                      },
                      child: Txt(
                          txt: "Try another way",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .1,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ),
                ),*/
                Padding(
                  padding: const EdgeInsets.only(
                      top: 20, left: 20, right: 20, bottom: 20),
                  child: MMBtn(
                      txt: "Send",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        if (validate()) {
                          ForgotAPIMgr().wsForgotAPI(
                            context: context,
                            email: _email.text.trim(),
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    try {
                                      final msg = model
                                          .messages.forgotpassword[0]
                                          .toString();
                                      showToast(msg: msg, which: 1);
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  } else {
                                    try {
                                      if (mounted) {
                                        final err = model
                                            .errorMessages.forgotpassword[0]
                                            .toString();
                                        showToast(msg: err);
                                      }
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  }
                                } catch (e) {
                                  log(e.toString());
                                }
                              }
                            },
                          );
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
