import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../Mixin.dart';
import 'ForgotScreen.dart';
import 'otp/Sms1Screen.dart';
import 'otp/Sms2Screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  State createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Mixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      try {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: MyTheme.themeData.accentColor));
      } catch (e) {}
      if (Server.isTest) {
        _email.text = "anisur5001@yopmail.com";
        //"anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _pwd.text = "123456";
      }
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isEmpty(_email, 'Invalid Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          titleSpacing: 0,
          title: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              width: 100,
              height: 60,
              child: Image.asset(
                "assets/images/icons/back_btn_icon.png",
              ),
            ),
          ),
          actions: [
            TextButton(
              //textColor: Colors.white,
              onPressed: () {
                navTo(context: context, isRep: true, page: () => RegScreen());
              },
              child: Txt(
                  txt: "Sign up",
                  txtColor: MyTheme.redColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              //shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                  child: Txt(
                    txt: "Let's sign you in",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1.3,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Txt(
                    txt: "Welcome back",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Txt(
                    txt: "You have been missed!",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: drawInputBox(
                    title: "Email",
                    input: _email,
                    kbType: TextInputType.emailAddress,
                    len: 50,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: drawInputBox(
                    title: "Password",
                    input: _pwd,
                    kbType: TextInputType.text,
                    len: 20,
                    isPwd: true,
                  ),
                ),
                Center(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 10, right: 10),
                    child: GestureDetector(
                      onTap: () {
                        navTo(context: context, page: () => ForgotScreen());
                      },
                      child: Txt(
                          txt: "Forgot password?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .1,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 40, bottom: 20),
                  child: MMBtn(
                      txt: "Sign in",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        //wsLoginAPI();
                        if (validate()) {
                          setState(() {
                            isLoading = true;
                          });
                          LoginAPIMgr().wsLoginAPI(
                              context: context,
                              email: _email.text.trim(),
                              pwd: _pwd.text.trim(),
                              callback: (model) async {
                                if (model != null && mounted) {
                                  try {
                                    setState(() {
                                      isLoading = false;
                                    });
                                    if (model.success) {
                                      try {
                                        await DBMgr.shared.setUserProfile(
                                            user: model.responseData.user);
                                        await userData.setUserModel();
                                        try {
                                          ProfileHelper().downloadProfileImages(
                                            context: context,
                                          );
                                        } catch (e) {
                                          log(e.toString());
                                        }
                                        if (Server.isTest) {
                                          /*navTo(
                        context: context,
                        isRep: true,
                        page: () => CompAccScreen(
                              isLoggedIn: false,
                              userModel: model.responseData.user,
                            ));*/

                                          navTo(
                                                  context: context,
                                                  isRep: true,
                                                  page: () => DashBoardScreen())
                                              .then((value) {
                                            //callback(route);
                                          });
                                        } else {
                                          if (model.responseData.user
                                              .isMobileNumberVerified) {
                                            /*navTo(
                          context: context,
                          isRep: true,
                          page: () => CompAccScreen(
                                isLoggedIn: false,
                                userModel: model.responseData.user,
                              ));*/
                                            navTo(
                                                    context: context,
                                                    isRep: true,
                                                    page: () =>
                                                        DashBoardScreen())
                                                .then((value) {
                                              //callback(route);
                                            });
                                          } else {
                                            navTo(
                                                context: context,
                                                page: () => Sms2Screen(
                                                      mobile: '',
                                                    )).then((value) {
                                              //callback(route);
                                            });
                                          }
                                        }
                                      } catch (e) {
                                        log(e.toString());
                                      }
                                    } else {
                                      try {
                                        if (mounted) {
                                          final err = model
                                              .errorMessages.login[0]
                                              .toString();
                                          showToast(msg: err);
                                        }
                                      } catch (e) {
                                        log(e.toString());
                                      }
                                    }
                                  } catch (e) {
                                    log(e.toString());
                                  }
                                }
                              });
                        }
                      }),
                ),
                //drawOtpBtn(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawOtpBtn() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: MMBtn(
                txt: "Log in with Mobile",
                height: getHP(context, 7),
                width: getW(context),
                callback: () {
                  navTo(
                    context: context,
                    isRep: true,
                    page: () => Sms1Screen(),
                  ).then((value) {
                    //callback(route);
                  });
                }),
          ),
        ],
      ),
    );
  }
}
