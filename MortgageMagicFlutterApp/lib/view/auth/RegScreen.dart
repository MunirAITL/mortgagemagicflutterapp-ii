import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/RegAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/LoginScreen.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/mywidgets/DatePickerView.dart';
import 'package:aitl/view/mywidgets/InputBox.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/SwitchView.dart';
import 'package:aitl/view/mywidgets/TCView.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../Mixin.dart';
import 'otp/Sms2Screen.dart';

class RegScreen extends StatefulWidget {
  @override
  State createState() => _RegScreenState();
}

enum genderEnum { male, female }

class _RegScreenState extends State<RegScreen> with Mixin {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  final _compName = TextEditingController();

  bool _isSwitch = false;

  String dob = "";

  String dobDD = "";

  String dobMM = "";

  String dobYY = "";

  genderEnum _gender = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _compName.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {
      log(e.toString());
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    } else if (!UserProfileVal().isEmailOK(_email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    }
    return true;
  }

  validate2() {
    if (_isSwitch && !UserProfileVal().isComNameOK(_compName)) {
      return false;
    } else if (!UserProfileVal().isDOBOK(dob)) {
      return false;
    }
    return true;
  }

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          titleSpacing: 0,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                (isStep2)
                    ? Container(
                        //color: Colors.black,
                        width: 100,
                        height: 60,
                        child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isStep2 = false;
                              });
                            },
                            child: Image.asset(
                                "assets/images/icons/back_btn_icon.png")),
                      )
                    : SizedBox(),
                GestureDetector(
                  onTap: () {
                    navTo(
                        context: context,
                        isRep: true,
                        page: () => LoginScreen());
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Txt(
                        txt: "Sign in",
                        txtColor: MyTheme.redColor,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (!isStep2) ? drawRegView1() : drawRegView2(),
        ),
      ),
    );
  }

  drawRegView1() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, bottom: 10),
                      child: Txt(
                        txt: "Let's Get Started",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + 1.3,
                        txtAlign: TextAlign.start,
                        isBold: true,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 10),
                      child: Txt(
                        txt: "Create an account to see how",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, bottom: 10),
                      child: Txt(
                        txt: "this wonderfull app works.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "First Name",
                        input: _fname,
                        kbType: TextInputType.name,
                        len: 20,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Last Name",
                        input: _lname,
                        kbType: TextInputType.name,
                        len: 20,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Email",
                        input: _email,
                        kbType: TextInputType.emailAddress,
                        len: 50,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Phone Number",
                        input: _mobile,
                        kbType: TextInputType.phone,
                        len: 20,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Password",
                        input: _pwd,
                        kbType: TextInputType.text,
                        len: 20,
                        isPwd: true,
                      ),
                    ),
                  ],
                ),
              ),
              /*SizedBox(height: 20),
              Txt(
                txt: "or",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: ListTile(
                  tileColor: Colors.blue.shade800,
                  leading: Image(
                    image: AssetImage(
                      "assets/images/icons/fb_icon.png",
                    ),
                    //color: null,
                    width: 20,
                    height: 20,
                  ),
                  minLeadingWidth: 0,
                  title: MaterialButton(
                    child: Container(
                      width: double.infinity,
                      child: Align(
                        alignment: Alignment(-1, 0),
                        child: Text(
                          "Continue with Facebook",
                        ),
                      ),
                    ),
                    onPressed: () async {
                      //
                    },
                  ),
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: TCView(screenName: 'Continue'),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 30, bottom: 20),
                child: MMBtn(
                    txt: "Continue",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      if (validate1()) {
                        isStep2 = true;
                        setState(() {});
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 20),
      child: Container(
        width: getW(context),
        //height: getH(context),
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                drawCompSwitch(),
                Padding(
                  padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                  child: Txt(
                      txt: "Select Date of Birth",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: DatePickerView(
                    cap: null,
                    dt: (dob == '') ? 'Select Date' : dob,
                    initialDate: dateDOBlast,
                    firstDate: dateDOBfirst,
                    lastDate: dateDOBlast,
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat('dd-MMM-yyyy')
                                .format(value)
                                .toString();
                            final dobArr = dob.toString().split('-');
                            this.dobDD = dobArr[0];
                            this.dobMM = dobArr[1];
                            this.dobYY = dobArr[2];
                          } catch (e) {
                            log(e.toString());
                          }
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 20),
                drawGender(),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: MMBtn(
                      txt: "Get Started",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        if (validate2()) {
                          setState(() {
                            isLoading = true;
                          });
                          RegAPIMgr().wsRegAPI(
                              context: context,
                              email: _email.text.trim(),
                              pwd: _pwd.text.trim(),
                              fname: _fname.text.trim(),
                              lname: _lname.text.trim(),
                              mobile: _mobile.text.trim(),
                              dob: dob,
                              dobDD: dobDD,
                              dobMM: dobMM,
                              dobYY: dobYY,
                              callback: (model) async {
                                if (model != null && mounted) {
                                  try {
                                    if (model.success) {
                                      //  recall login to get cookie
                                      LoginAPIMgr().wsLoginAPI(
                                          context: context,
                                          email: _email.text.trim(),
                                          pwd: _pwd.text.trim(),
                                          callback: (model2) async {
                                            if (model2 != null && mounted) {
                                              try {
                                                setState(() {
                                                  isLoading = false;
                                                });
                                                if (model2.success) {
                                                  //log(model.responseData.user.address);
                                                  try {
                                                    //DBMgr.shared.getUserProfile();
                                                    await DBMgr.shared
                                                        .setUserProfile(
                                                      user: model2
                                                          .responseData.user,
                                                      otpID: model
                                                          .responseData.otpId
                                                          .toString(),
                                                      otpMobileNumber: model
                                                          .responseData
                                                          .otpMobileNumber,
                                                    );
                                                    await userData
                                                        .setUserModel();
                                                    try {
                                                      ProfileHelper()
                                                          .downloadProfileImages(
                                                        context: context,
                                                      );
                                                    } catch (e) {
                                                      log(e.toString());
                                                    }

                                                    if (model.responseData.user
                                                        .isMobileNumberVerified) {
                                                      navTo(
                                                        context: context,
                                                        isRep: true,
                                                        page: () =>
                                                            DashBoardScreen(),
                                                      ).then((value) {
                                                        //callback(route);
                                                      });
                                                      /*navTo(
                                  context: context,
                                  isRep: true,
                                  page: () => CompAccScreen(
                                        isLoggedIn: false,
                                        userModel: model.responseData.user,
                                        otpId: model.responseData.otpId,
                                        otpMobileNumber:
                                            model.responseData.otpMobileNumber,
                                      ));*/
                                                    } else {
                                                      navTo(
                                                        context: context,
                                                        page: () => Sms2Screen(
                                                          mobile: _mobile.text,
                                                        ),
                                                      ).then((value) {
                                                        //callback(route);
                                                      });
                                                    }
                                                  } catch (e) {
                                                    log(e.toString());
                                                  }
                                                } else {
                                                  try {
                                                    if (mounted) {
                                                      final err = model2
                                                          .errorMessages
                                                          .login[0]
                                                          .toString();
                                                      showToast(msg: err);
                                                    }
                                                  } catch (e) {
                                                    log(e.toString());
                                                  }
                                                }
                                              } catch (e) {
                                                log(e.toString());
                                              }
                                            }
                                          });
                                    } else {
                                      try {
                                        setState(() {
                                          isLoading = false;
                                        });
                                        if (mounted) {
                                          final err = model
                                              .errorMessages.register[0]
                                              .toString();
                                          showToast(msg: err);
                                        }
                                      } catch (e) {
                                        log(e.toString());
                                      }
                                    }
                                  } catch (e) {
                                    log(e.toString());
                                  }
                                }
                              });
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Padding(
      padding: const EdgeInsets.only(top: 30, left: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Gender",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
            SizedBox(height: 10),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.male;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.male,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Male",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.female;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.female,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Female",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCompSwitch() {
    return Container(
      //color: MyTheme.redColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 50),
                      child: Txt(
                        txt:
                            "Do you have a mortgage company? Otherwise we will recommend to you.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true,
                        //txtLineSpace: 1.5,
                      ),
                    ),
                  ),
                  //SizedBox(width: 10),
                  SwitchView(
                    onTxt: "Yes",
                    offTxt: "No",
                    value: _isSwitch,
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 30),
                        Txt(
                            txt: "Company Name",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 20),
                        InputBox(
                          ctrl: _compName,
                          lableTxt: "Company Name",
                          kbType: TextInputType.text,
                          len: 100,
                          isPwd: false,
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
