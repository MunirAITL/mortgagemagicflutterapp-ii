import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/mywidgets/Btn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';

class Sms2Screen extends StatefulWidget {
  final String mobile;
  Sms2Screen({Key key, this.mobile = ''}) : super(key: key);
  @override
  State createState() => _Sms2ScreenState();
}

class _Sms2ScreenState extends State<Sms2Screen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _phoneController = TextEditingController();

  Color btnBgColor;
  Color btnTxtColor;
  bool isOtpCalled = false;
  bool isLoading = false;
  final int timeOut = 60;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _phoneController.dispose();
    try {} catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));
    } catch (e) {}
    try {
      if (widget.mobile.length == 0) {
        btnBgColor = Colors.grey[300];
        btnTxtColor = Colors.black;
      } else {
        btnBgColor = MyTheme.redColor;
        btnTxtColor = Colors.white;
      }

      _phoneController.text = widget.mobile;
      _phoneController.addListener(() {
        if (mounted) {
          if (_phoneController.text.trim().length <
              UserProfileVal.PHONE_LIMIT) {
            btnBgColor = Colors.grey[300];
            btnTxtColor = Colors.black;
          } else {
            btnBgColor = MyTheme.redColor;
            btnTxtColor = Colors.white;
          }
          setState(() {});
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Form(
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 5)),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Enter your mobile number",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              //txtLineSpace: 1.0,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              controller: _phoneController,
              keyboardType: TextInputType.phone,
              maxLength: 15,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize:
                    getTxtSize(context: context, txtSize: MyTheme.txtSize + .3),
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                hintStyle: new TextStyle(
                  color: Colors.grey,
                  fontSize: getTxtSize(
                      context: context, txtSize: MyTheme.txtSize + .8),
                ),
                hintText: AppDefine.COUNTRY_CODE + " xxxxxx",
                //prefixText: AppDefine.COUNTRY_CODE,
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                border: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          Btn(
            txt: "Next",
            txtColor: btnTxtColor,
            bgColor: btnBgColor,
            width: getWP(context, 90),
            height: getHP(context, 8),
            radius: 10,
            callback: () async {
              if (_phoneController.text.trim().length >=
                  UserProfileVal.PHONE_LIMIT) {
                Sms2APIMgr().wsLoginMobileOtpPostAPI(
                  context: context,
                  mobile: _phoneController.text.trim(),
                  callback: (model) {
                    if (model != null && mounted) {
                      try {
                        if (model.success) {
                          try {
                            //final msg = model.messages.postUserotp[0].toString();
                            //showToast(msg: msg, which: 1);
                            if (mounted) {
                              Sms2APIMgr().wsSendOtpNotiAPI(
                                  context: context,
                                  otpId: model.responseData.userOTP.id,
                                  callback: (model) {
                                    if (model != null && mounted) {
                                      try {
                                        if (model.success) {
                                          try {
                                            //final msg = model.messages.postUserotp[0].toString();
                                            //showToast(msg: msg, which: 1);
                                            navTo(
                                              context: context,
                                              page: () => Sms3Screen(
                                                mobileUserOTPModel:
                                                    model.responseData.userOTP,
                                              ),
                                            ).then((value) {
                                              //callback(route);
                                            });
                                          } catch (e) {
                                            log(e.toString());
                                          }
                                        } else {
                                          try {
                                            if (mounted) {
                                              final err = model
                                                  .messages.postUserotp[0]
                                                  .toString();
                                              showToast(msg: err);
                                            }
                                          } catch (e) {
                                            log(e.toString());
                                          }
                                        }
                                      } catch (e) {
                                        log(e.toString());
                                      }
                                    }
                                  });
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                        } else {
                          try {
                            if (mounted) {
                              final err =
                                  model.messages.postUserotp[0].toString();
                              showToast(msg: err);
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                        }
                      } catch (e) {
                        log(e.toString());
                      }
                    }
                  },
                );
              } else {
                showToast(msg: 'Please enter your valid phone number');
              }
            },
          ),
        ],
      ),
    );
  }
}
