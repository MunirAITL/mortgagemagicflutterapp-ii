import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms3APIMgr.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOTPModel.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Sms3Screen extends StatefulWidget {
  final MobileUserOTPModel mobileUserOTPModel;
  const Sms3Screen({
    Key key,
    @required this.mobileUserOTPModel,
  }) : super(key: key);
  @override
  State createState() => new _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Screen>
    with SingleTickerProviderStateMixin, Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {}

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, widget.mobileUserOTPModel.mobileNumber);
      },
      child: Center(
        child: Txt(
          txt: "Resend code",
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize - .2,
          txtAlign: TextAlign.center,
          isBold: false,
        ),
      ),
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 80,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: 80.0,
                  ),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(
                          context, widget.mobileUserOTPModel.mobileNumber);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      //height: getH(context),
//        padding: new EdgeInsets.only(bottom: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Txt(
              txt: "Verify your code",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + 1.3,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
          ),
          _getInputField,
          _getResendButton,
          _getOtpKeyboard
        ],
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      child: Txt(
          txt: digit != null ? digit.toString() : "",
          txtColor: Colors.white,
          txtSize: MyTheme.txtSize + 2.8,
          txtAlign: TextAlign.start,
          isBold: true),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        color: MyTheme.redColor,
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + 1.8,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;

          final otpCode = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode.length == SMS_AUTH_CODE_LEN) {
            //signInWithPhoneNumber(otpCode);
            Sms3APIMgr().wsLoginMobileOtpPutAPI(
              context: context,
              otpCode: otpCode,
              mobile: widget.mobileUserOTPModel.mobileNumber,
              callback: (model) {
                if (model != null && mounted) {
                  try {
                    if (model.success) {
                      try {
                        //final msg = model.messages.postUserotp[0].toString();
                        //showToast(msg: msg, which: 1);
                        if (mounted) {
                          Sms3APIMgr().wsLoginMobileFBPostAPI(
                            context: context,
                            mobileUserOTPModel: model.responseData.userOTP,
                            otpCode: otpCode,
                            callback: (model) async {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    try {
                                      await DBMgr.shared.setUserProfile(
                                          user: model.responseData.user);
                                      await userData.setUserModel();
                                      try {
                                        ProfileHelper().downloadProfileImages(
                                          context: context,
                                        );
                                      } catch (e) {
                                        log(e.toString());
                                      }
                                      Get.offAll(
                                        () => DashBoardScreen(),
                                      ).then((value) {
                                        //callback(route);
                                      });

                                      /*navTo(
                    context: context,
                    isRep: true,
                    page: () => CompAccScreen(
                          isLoggedIn: false,
                          userModel: model.responseData.user,
                        ));*/
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  } else {
                                    try {
                                      if (mounted) {
                                        final err = model.errorMessages.login[0]
                                            .toString();
                                        showToast(msg: err);
                                      }
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  }
                                } catch (e) {
                                  log(e.toString());
                                }
                              }
                            },
                          );
                        }
                      } catch (e) {
                        log(e.toString());
                      }
                    } else {
                      try {
                        if (mounted) {
                          final err = model.errorMessages.postUserotp[0];
                          showToast(msg: err, which: 0);
                        }
                      } catch (e) {
                        log(e.toString());
                      }
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                }
              },
            );
          }
        }
      });
    }
  }
}
