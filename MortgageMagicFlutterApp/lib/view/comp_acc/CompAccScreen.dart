import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/comp_acc/CompAccAPIMgr.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/model/json/comp_acc/UserCompanyInfoModel.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class CompAccScreen extends StatefulWidget {
  final UserModel userModel;
  final int otpId;
  final String otpMobileNumber;
  final bool isLoggedIn; //  bcz, for back button via more
  const CompAccScreen(
      {Key key,
      @required this.userModel,
      this.otpId,
      this.otpMobileNumber,
      @required this.isLoggedIn})
      : super(key: key);
  @override
  State createState() => _CompAccScreenState();
}

class _CompAccScreenState extends State<CompAccScreen> with Mixin {
  List<UserCompanyInfoModel> listUserCompanyInfoModel = [];

  int _groupValue = 0;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      CompAccAPIMgr().wsGetCompAccAPI(
          context: context,
          userId: widget.userModel.id.toString(),
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    listUserCompanyInfoModel =
                        model.responseData.userCompanyInfos;
                    setState(() {});
                  } catch (e) {
                    log(e.toString());
                  }
                } else {
                  try {
                    if (mounted) {
                      final err = model.messages.toString();
                      showToast(msg: err);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: "Company Account",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: true,
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            SizedBox(height: 10),
            Txt(
                txt: "Use these accounts instead of creating a new one.",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                //txtLineSpace: 1.2,
                isBold: true),
            SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listUserCompanyInfoModel.length,
              itemBuilder: (context, index) {
                final model = listUserCompanyInfoModel[index];
                //MMBtn btn = item['btn'] as MMBtn;

                return Card(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: ListTile(
                      title: Theme(
                        data: MyTheme.radioThemeData,
                        child: RadioListTile(
                          value: index,
                          groupValue: _groupValue,
                          onChanged: (value) {
                            if (mounted) {
                              setState(() {
                                _groupValue = value;
                              });
                            }
                          },
                          title: Txt(
                              txt: model.companyName,
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: "Domain: " + model.companyWebsite,
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                //txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: "Phone: " + model.officePhoneNumber,
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                //txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20),
                            child: MMBtn(
                                  txt: model.userType,
                                  bgColor: Colors.grey,
                                  height: null,
                                  width: null,
                                  callback: () {
                                    switch (model.id) {
                                      case 1003:
                                        break;
                                      default:
                                    }
                                  },
                                ) ??
                                SizedBox(),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: 20),
            (listUserCompanyInfoModel.length > 0)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: MMBtn(
                      txt: "Continue with this account",
                      height: getHP(context, 7),
                      width: null,
                      callback: () async {
                        //
                        try {
                          if (widget.otpId != null &&
                              widget.otpMobileNumber != null) {
                            await DBMgr.shared.setUserProfile(
                              user: widget.userModel,
                              otpID: widget.otpId.toString(),
                              otpMobileNumber: widget.otpMobileNumber,
                            );
                          } else {
                            await DBMgr.shared
                                .setUserProfile(user: widget.userModel);
                          }
                          await userData.setUserModel();
                        } catch (e) {}
                        try {
                          ProfileHelper()
                              .downloadProfileImages(context: context);
                        } catch (e) {
                          log(e.toString());
                        }
                        navTo(
                            context: context,
                            isRep: true,
                            page: () => DashBoardScreen()).then((value) {
                          //callback(route);
                        });
                      },
                    ),
                  )
                : SizedBox(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
