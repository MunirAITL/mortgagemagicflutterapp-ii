import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'NewCase2Screen.dart';

class NewCase1Screen extends StatefulWidget {
  @override
  State createState() => _NewCase1ScreenState();
}

class _NewCase1ScreenState extends State<NewCase1Screen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Create New",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getHP(context, 10)),
            drawHeader(),
            SizedBox(height: getHP(context, 7)),
            drawCenterTxt(),
            Padding(
              padding: const EdgeInsets.only(top: 40, left: 40, right: 40),
              child: MMBtn(
                  txt: "Create Now!",
                  height: getHP(context, 7),
                  width: getW(context),
                  callback: () {
                    navTo(
                        context: context,
                        isRep: true,
                        page: () => NewCase2Screen());
                  }),
            ),
          ],
        ),
      ),
    );
  }

  drawHeader() {
    return Container(
      width: getWP(context, 80),
      height: getWP(context, 40),
      child: Image.asset(
          "assets/images/screens/home/dashboard/create_new_header.png",
          fit: BoxFit.fill),
    );
  }

  drawCenterTxt() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Txt(
              txt: "Create New Case",
              txtColor: MyTheme.welcomeTitleColor,
              txtSize: MyTheme.txtSize + 1,
              txtAlign: TextAlign.center,
              isBold: true),
          Padding(
            padding: const EdgeInsets.only(top: 30, left: 40, right: 40),
            child: Center(
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  // Note: Styles for TextSpans must be explicitly defined.
                  // Child text spans will inherit styles from parent

                  children: <TextSpan>[
                    new TextSpan(
                        text:
                            'Select the type of mortgage you are looking for from the next page and let ',
                        style: new TextStyle(
                          height: 1.2,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + .4),
                          color: MyTheme.welcomeTitleColor,
                        )),
                    new TextSpan(
                        text: 'MortgageLad',
                        style: new TextStyle(
                          height: 1.2,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + .4),
                          color: MyTheme.redColor,
                        )),
                    new TextSpan(
                        text: ' do its magic',
                        style: new TextStyle(
                          height: 1.2,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + .4),
                          color: MyTheme.welcomeTitleColor,
                        )),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
