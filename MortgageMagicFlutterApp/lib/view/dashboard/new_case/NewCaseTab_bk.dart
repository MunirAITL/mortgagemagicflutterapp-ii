import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/HomeCfg.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/dashboard/new_case/post_case/NewCase1Screen.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/paint/CirclePainter.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewCaseTab2 extends StatefulWidget {
  @override
  State createState() => NewCaseTab2State();
}

class NewCaseTab2State extends State<NewCaseTab2> with Mixin, StateListener {
  int completedVal = 25;
  int fmaVal = 22;
  int offeredVal = 5;
  int leadVal = 115;

  final List<dynamic> listGrid = [
    {
      'title': 'Total Cases',
      'value': 566,
      'icon': "assets/images/screens/home/dashboard/grid/total_cases.png",
    },
    {
      'title': 'Completed Cases',
      'value': 25,
      'icon': "assets/images/screens/home/dashboard/grid/complete_cases.png",
    },
    {
      'title': 'FMA Submitted Cases',
      'value': 22,
      'icon':
          "assets/images/screens/home/dashboard/grid/fma_submitted_cases.png",
    },
    {
      'title': 'Total Offered Cases',
      'value': 05,
      'icon':
          "assets/images/screens/home/dashboard/grid/total_offered_cases.png",
    },
    {
      'title': 'Total Customers',
      'value': 474,
      'icon': "assets/images/screens/home/dashboard/grid/total_customers.png",
    },
    {
      'title': 'Total Lead',
      'value': 303,
      'icon': "assets/images/screens/home/dashboard/grid/total_leads.png",
    },
    {
      'title': 'Total Advisors',
      'value': 200,
      'icon': "assets/images/screens/home/dashboard/grid/total_advisor.png",
    },
    {
      'title': 'Total Introducers',
      'value': 100,
      'icon': "assets/images/screens/home/dashboard/grid/total_introducers.png",
    },
  ];

  bool isShowSlider = true;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit_dashboard;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar1_reload_case_api) {
      //_getRefreshData();
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      Future.delayed(Duration(seconds: 3 * NewCaseCfg.listSliderImages.length),
          () {
        if (mounted) {
          setState(() {
            isShowSlider = false;
          });
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          leading: IconButton(
              icon: Icon(Icons.add_business_outlined,
                  color: MyTheme.redColor, size: 30),
              onPressed: () {
                navTo(context: context, page: () => NewCase1Screen());
              }),
          title: Txt(
              txt: "Dashboard",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //drawCircleLayout(),
            drawGridLayout(),
          ],
        ),
      ),
    );
  }

  drawCircleLayout() {
    return Container(
      child: Card(
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 40, bottom: 40),
            child: Container(
              height: getWP(context, 40),
              width: getWP(context, 40),
              child: CustomPaint(
                foregroundPainter: new CirclePainter(
                  totalCases: 566,
                  completePA: 40,
                  fmaPA: 10,
                  offeredPA: 20,
                  width: getWP(context, 100),
                  height: getHP(context, 100),
                  fontSize:
                      getTxtSize(context: context, txtSize: MyTheme.txtSize),
                ),
              ),
            ),
          ),
          SizedBox(width: 50),
          Expanded(
            child: Container(
              //color: Colors.blue,
              //height: 100,
              //width: 200,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.completedColor, completedVal, "Completed"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.fmaColor, fmaVal, "FMA Submitted"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.offeredColor, offeredVal, "Offered"),
                  SizedBox(height: 10),
                  drawCircleRightIcons(
                      HomeCfg.leadColor, leadVal, "Total Lead"),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  drawCircleRightIcons(Color colr, int val, String status) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(Icons.circle, color: colr, size: 10),
        SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: val.toString(),
                  txtColor: MyTheme.timelineTitleColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 5),
              Txt(
                  txt: status,
                  txtColor: MyTheme.timelineTitleColor,
                  txtSize: MyTheme.txtSize - .7,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ],
    );
  }

  drawGridLayout() {
    final double itemHeight = (getH(context) - kToolbarHeight - 24) / 2;
    final double itemWidth = getW(context) / 2;
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      crossAxisCount: 2,
      padding: const EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      childAspectRatio: (itemHeight / itemWidth),
      children: List.generate(
        listGrid.length,
        (index) {
          return Card(
            margin: new EdgeInsets.all(10),
            //color: Colors.black,
            child: new Row(
              //mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //footer: new Text(listGrid[index]['title']),
                Expanded(
                  //flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Txt(
                          txt: listGrid[index]['title'],
                          txtColor: MyTheme.timelineTitleColor,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt: listGrid[index]['value'].toString(),
                          txtColor: MyTheme.timelineTitleColor,
                          txtSize: MyTheme.txtSize + 1,
                          txtAlign: TextAlign.center,
                          isBold: true),
                    ],
                  ),
                ),
                SizedBox(width: 10),
                Flexible(
                  child: Container(
                    width: getWP(context, 14),
                    height: getWP(context, 14),
                    //color: Colors.black,
                    child: Image.asset(
                      listGrid[index]['icon'],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),

                //just for testing, will fill with image later
              ],
            ),
          );
        },
      ),
    );
  }
}
