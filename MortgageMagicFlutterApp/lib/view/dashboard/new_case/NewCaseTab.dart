import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/config/dashboard/UserNotesCfg.dart';
import 'package:aitl/controller/api/dashboard/usercases/UserCaseAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/tab_newcase/UserNotesModel.dart';
import 'package:aitl/view/dashboard/new_case/PostNewCaseScreen.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewCaseTab extends StatefulWidget {
  @override
  State createState() => NewCaseTabState();
}

class NewCaseTabState extends State<NewCaseTab> with Mixin, StateListener {
  List<dynamic> listUserNotesModel = [];

  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit_dashboard;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar1_reload_case_api) {
      userNotesAPI();
    }
  }

  userNotesAPI() async {
    try {
      setState(() {
        isLoading = true;
      });
      UserCaseAPIMgr().wsUserNotesAPI(
          context: context,
          status: UserNotesCfg.PENDING,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    listUserNotesModel = model.responseData.userNotes;
                    if (listUserNotesModel.length > 0) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    } else {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } catch (e) {
                    setState(() {
                      isLoading = false;
                    });
                    log(e.toString());
                  }
                } else {
                  setState(() {
                    isLoading = false;
                  });
                }
              } catch (e) {
                setState(() {
                  isLoading = false;
                });
                log(e.toString());
              }
            }
          });
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    listUserNotesModel = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      userNotesAPI();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          /*leading: IconButton(
              icon: Icon(Icons.add_business_outlined,
                  color: MyTheme.redColor, size: 30),
              onPressed: () {
                navTo(context: context, page: () => NewCase1Screen());
              }),*/
          title: Txt(
              txt: "Dashboard",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        primary: true,
        shrinkWrap: true,
        children: [
          (listUserNotesModel.length > 0) ? drawActionReqView() : SizedBox(),
          drawCreateNewCase(),
        ],
      ),
    );
  }

  drawActionReqView() {
    //print('absolute coordinates on screen: ${containerKey.globalPaintBounds}');
    try {
      //RenderBox _box = posKey.currentContext.findRenderObject();
      //Offset position = _box.localToGlobal(Offset.zero);

      return Container(
        //color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: "Action required",
                  txtColor: MyTheme.timelinePostCallerNameColor,
                  txtSize: MyTheme.txtSize + .5,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  color: Colors.transparent,
                  height: getHP(context, 20),
                  child: ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: listUserNotesModel.length,
                    itemBuilder: (context, index) {
                      final UserNotesModel userNotesModel =
                          listUserNotesModel[index];
                      return Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/icons/info2_icon.png",
                              width: 35,
                              height: 35,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Txt(
                                  txt: userNotesModel.title,
                                  txtColor: MyTheme.timelineTitleColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      );
    } catch (e) {
      log(e.toString());
      return Container();
    }
  }

  drawCreateNewCase() {
    var _crossAxisSpacing = 8;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = 2;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = getHP(context, 15);
    var _aspectRatio = _width / cellHeight;
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Create a new case",
              txtColor: MyTheme.timelinePostCallerNameColor,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          GridView.builder(
            shrinkWrap: true,
            primary: false,
            //padding: const EdgeInsets.only(top: 40, bottom: 40),
            scrollDirection: Axis.vertical,
            itemCount: NewCaseCfg.listCreateNewCase.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: _aspectRatio),
            itemBuilder: (context, index) {
              final map = NewCaseCfg.listCreateNewCase[index];
              final icon = map["url"];
              final title = map["title"];
              return GestureDetector(
                onTap: () async {
                  navTo(
                    context: context,
                    page: () => PostNewCaseScreen(
                      indexCase: index,
                    ),
                  ).then((value) {
                    //callback(route);
                  });
                },
                child: Card(
                  margin: new EdgeInsets.all(10),
                  //color: Colors.black,
                  child: new Row(
                    //mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //footer: new Text(listGrid[index]['title']),
                      Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Txt(
                                txt: title,
                                txtColor: MyTheme.timelineTitleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                maxLines: 4,
                                //txtLineSpace: 1.2,
                                isBold: false),
                            /*SizedBox(height: 10),
                            Txt(
                                txt: "266",
                                txtColor: MyTheme.timelineTitleColor,
                                txtSize: MyTheme.txtSize + 1,
                                txtAlign: TextAlign.center,
                                isBold: true),*/
                          ],
                        ),
                      ),
                      SizedBox(width: 5),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Container(
                            width: getWP(context, 15),
                            height: getWP(context, 15),
                            //color: Colors.black,
                            child: Image.asset(
                              icon,
                              //fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),

                      //just for testing, will fill with image later
                    ],
                  ),
                ),
              );
            },
          ),
        ]);
  }
}
