import 'dart:convert';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/controller/api/dashboard/new_case/PostCaseAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/helper/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/tab_newcase/PostNewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/tab_newcase/LocationsModel.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/TxtBox.dart';
import 'package:aitl/view/mywidgets/com/OtherApplicantSwitchView.dart';
import 'package:aitl/view/mywidgets/com/SPVSwitchView.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostNewCaseScreen extends StatefulWidget {
  final int indexCase;
  const PostNewCaseScreen({
    Key key,
    @required this.indexCase,
  }) : super(key: key);
  @override
  State createState() => _PostNewCaseScreenState();
}

class _PostNewCaseScreenState extends State<PostNewCaseScreen> with Mixin {
  final TextEditingController _note = TextEditingController();

  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  String _regAddr = "";
  final _regNo = TextEditingController();
  String regDate = "";

  var title = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  int otherApplicantRadioIndex = 1;

  StateProvider _stateProvider;

  validate() {
    if (_note.text.trim().length == 0) {
      showToast(msg: "Please enter case note");
      return false;
    }
    if (isOtherApplicantSwitch) {
      int i = 0;
      while (i < otherApplicantRadioIndex) {
        if (!UserProfileVal().isEmailOK(
            listOApplicantInputFieldsCtr[i],
            "Invalid " +
                MyDefine.ORDINAL_NOS[i + 1] +
                " applicant email address")) {
          return false;
        }
        i++;
      }
    }
    if (isSPVSwitch) {
      if (!UserProfileVal().isEmpty(_compName, "Missing company name")) {
        return false;
      } else if (_regAddr == "") {
        showToast(msg: "Missing company registered address");
        return false;
      } else if (!UserProfileVal()
          .isEmpty(_regNo, "Missing company registeration number")) {
        return false;
      } else if (regDate == "") {
        showToast(msg: "Please select registered date");
        return false;
      }
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    _note.dispose();
    _compName.dispose();
    _regNo.dispose();
    listOApplicantInputFieldsCtr = null;
    _regAddr = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    _stateProvider = new StateProvider();
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
    } catch (e) {}
    try {
      final map = NewCaseCfg.listCreateNewCase[widget.indexCase];
      //final icon = map["url"];
      title = map["title"];
      if (map["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (map["isSPV"]) {
        isSPVSwitchShow = true;
      }
    } catch (e) {}
    try {
      _compName.addListener(() {
        log(_compName.text);
      });
      _regNo.addListener(() {
        log(_regNo.text);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      //Get.off
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Create New",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: getH(context),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              drawCaseType(),
              SizedBox(height: 30),
              drawCaseNote(),
              SizedBox(height: 30),
              (isOtherApplicantSwitchShow)
                  ? OtherApplicantSwitchView(
                      listOApplicantInputFieldsCtr:
                          listOApplicantInputFieldsCtr,
                      isSwitch: isOtherApplicantSwitch,
                      callback: (value) {
                        otherApplicantRadioIndex = value;
                        log(value);
                      },
                      callbackSwitch: (isSwitch_1) {
                        isOtherApplicantSwitch = isSwitch_1;
                      },
                    )
                  : SizedBox(),
              (isSPVSwitchShow)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: SPVSwitchView(
                        compName: _compName,
                        regAddr: _regAddr,
                        regNo: _regNo,
                        regDate: regDate,
                        isSwitch: isSPVSwitch,
                        callback: (value) {
                          regDate = value;
                        },
                        callbackSwitch: (isSwitch_2) {
                          isSPVSwitch = isSwitch_2;
                        },
                        callbackAddress: (address) {
                          _regAddr = address;
                        },
                      ),
                    )
                  : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(
                    top: 40, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                    txt: "Post Case",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      if (validate()) {
                        final param = PostNewCaseHelper().getParam(
                          isOtherApplicantSwitch: isOtherApplicantSwitch,
                          isSPVSwitch: isSPVSwitch,
                          listOApplicantInputFieldsCtr:
                              listOApplicantInputFieldsCtr,
                          compName: _compName.text.trim(),
                          regAddr: _regAddr,
                          regDate: regDate.trim(),
                          regNo: _regNo.text.trim(),
                          title: title.trim(),
                          note: _note.text.trim(),
                        );
                        log(json.encode(param));
                        PostCaseAPIMgr().wsOnPostCase(
                          context: context,
                          param: param,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                final LocationsModel caseModel =
                                    model.responseData.task;

                                Get.to(
                                  () => WebScreen(
                                    title: title,
                                    url: CaseDetailsWebHelper().getLink(
                                      title: title,
                                      taskId: caseModel.id,
                                    ),
                                  ),
                                ).then((value) {
                                  try {
                                    Navigator.pop(context);
                                    _stateProvider.notify(ObserverState
                                        .STATE_CHANGED_tabbar1_reload_case_api);
                                    _stateProvider.notify(ObserverState
                                        .STATE_CHANGED_tabbar2_reload_case_api);
                                    _stateProvider.notify(ObserverState
                                        .STATE_CHANGED_tabbar4_reload_case_api);
                                    _stateProvider.notify(
                                        ObserverState.STATE_CHANGED_tabbar2);
                                  } catch (e) {
                                    log(e.toString());
                                  }
                                });
                              } catch (e) {
                                log(e.toString());
                              }
                            }
                          },
                        );
                      }
                    }),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            TxtBox(txt: title, height: 5),
          ],
        ),
      ),
    );
  }

  drawCaseNote() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Note",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextFormField(
                  controller: _note,
                  minLines: 5,
                  maxLines: 10,
                  autocorrect: false,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: getTxtSize(context: context),
                  ),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: " Case Note",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: getTxtSize(context: context),
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
