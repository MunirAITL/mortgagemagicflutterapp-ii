import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/api/misc/DeviceInfoAPIMgr.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/dashboard/more/MoreTab.dart';
import 'package:aitl/view/dashboard/my_cases/MyCaseTab.dart';
import 'package:aitl/view/dashboard/new_case/NewCaseTab.dart';
import 'package:aitl/view/dashboard/noti/NotiTab.dart';
import 'package:aitl/view/dashboard/timeline/TimeLineTab.dart';
import 'package:aitl/view/mywidgets/com/HelpTutDialog.dart';
import 'package:aitl/view/mywidgets/tabs_nav/bottomNavigation.dart';
import 'package:aitl/view/mywidgets/tabs_nav/tabItem.dart';
import 'package:aitl/view/welcome/WelcomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashBoardScreen extends StatefulWidget {
  @override
  State createState() => DashBoardScreenState();
}

class DashBoardScreenState extends State<DashBoardScreen>
    with Mixin, StateListener, TickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider;
  bool isDialogHelpOpenned = false;

  static int currentTab = 0;

  @override
  onStateChanged(ObserverState state) async {
    int tabIndex = 0;
    if (state == ObserverState.STATE_CHANGED_logout) {
      CookieMgr().delCookiee();
      DBMgr.shared.delTable("User");
      Get.offAll(
        () => WelcomeScreen(),
      ).then((value) {
        //callback(route);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar1) {
      tabIndex = 0;
      if (!isDialogHelpOpenned) {
        isDialogHelpOpenned = true;
        Get.dialog(HelpTutDialog()).then((value) {
          setState(() {
            isDialogHelpOpenned = false;
            _selectTab(0);
          });
        });
      }
      setState(() {
        _selectTab(0);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar2) {
      tabIndex = 1;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar3) {
      tabIndex = 2;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar4) {
      tabIndex = 3;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar5) {
      tabIndex = 4;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    }
  }

  //  Tabbar stuff start here...
  final List<TabItem> tabs = [
    TabItem(
      tabName: "New Case",
      icon: AssetImage("assets/images/tabbar/new_case_icon.png"),
      //page: NewCaseTab(),
      page: NewCaseTab(),
    ),
    TabItem(
      tabName: "My Cases",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      page: MyCaseTab(),
    ),
    TabItem(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/msg_icon.png"),
      page: TimeLineTab(),
    ),
    TabItem(
      tabName: "Notifications",
      icon: AssetImage("assets/images/tabbar/noti_icon.png"),
      page: NotiTab(),
    ),
    TabItem(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/more_icon.png"),
      page: MoreTab(),
    )
  ];

  DashBoardScreenState() {
    // indexing is necessary for proper funcationality
    // of determining which tab is active

    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  // sets current tab index
  // and update state
  void _selectTab(int index) {
    if (index == currentTab) {
      // pop to first route
      // if the user taps on the active tab
      tabs[index].key.currentState.popUntil((route) => route.isFirst);
      //setState(() {});
    } else {
      // update the state
      // in order to repaint

      if (mounted) {
        setState(() => currentTab = index);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      //SystemChrome.setSystemUIOverlayStyle(
      //SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);

      DeviceInfoAPIMgr().wsFcmDeviceInfo(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {}
            } catch (e) {
              log(e.toString());
            }
          } else {
            log("not in");
          }
        },
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab =
                !await tabs[currentTab].key.currentState.maybePop();
            if (isFirstRouteInCurrentTab) {
              // if not on the 'main' tab
              if (currentTab != 0) {
                // select 'main' tab
                _selectTab(0);
                // back button handled by app
                return false;
              }
            }
            // let system handle back button if we're on the first route
            return isFirstRouteInCurrentTab;
          },
          // this is the base scaffold
          // don't put appbar in here otherwise you might end up
          // with multiple appbars on one screen
          // eventually breaking the app
          child: Scaffold(
            // indexed stack shows only one child
            body: IndexedStack(
              index: currentTab,
              children: tabs.map((e) => e.page).toList(),
            ),
            // Bottom navigation
            bottomNavigationBar: BottomNavigation(
              context: context,
              onSelectTab: _selectTab,
              tabs: tabs,
              isHelpTut: isDialogHelpOpenned,
              totalMsg: 0,
              totalNoti: 0,
            ),
          ),
        ),
      ),
    );
  }
}
