import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/controller/api/dashboard/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/helper/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/tab_newcase/LocationsModel.dart';
import 'package:aitl/view/dashboard/new_case/post_case/NewCase2Screen.dart';
import 'package:aitl/view/dashboard/new_case/EditCaseScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'MyCasesAppbar.dart';

class MyCaseTab extends StatefulWidget {
  MyCaseTab({
    Key key,
  }) : super(key: key);

  @override
  State createState() => MyCaseTabState();
}

class MyCaseTabState extends State<MyCaseTab>
    with Mixin, SingleTickerProviderStateMixin, StateListener {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TabController _tabController;
  List<LocationsModel> listTaskInfoSearchModel = [];

  StateProvider _stateProvider;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  //  tab stuff start here
  int caseStatus = NewCaseCfg.ALL;
  int totalTabs = 5;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar2_reload_case_api) {
      setState(() {
        isLoading = true;
      });
      _getRefreshData();
    }
  }

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      MyCasesAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        caseStatus: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> locations = model.responseData.locations;
                  if (locations != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (locations.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (LocationsModel location in locations) {
                        listTaskInfoSearchModel.add(location);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listTaskInfoSearchModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  }
                } catch (e) {
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    isLoading = false;
                    showToast(msg: "Cases not found");
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            log("not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    setState(() {
      pageStart = 0;
      isPageDone = false;
      isLoading = true;
      listTaskInfoSearchModel.clear();
    });
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _tabController.dispose();
    _tabController = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    listTaskInfoSearchModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    isLoading = false;
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _tabController = new TabController(vsync: this, length: totalTabs);
      _tabController.addListener(() {
        if (!isLoading) {
          controlTabbarIndex(_tabController.index);
          log('my tab index is: ' + _tabController.index.toString());
        }
      });
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  controlTabbarIndex(int index) {
    try {
      switch (index) {
        case 0:
          caseStatus = NewCaseCfg.ALL;
          break;
        case 1:
          caseStatus = NewCaseCfg.IN_PROGRESS;
          break;
        case 2:
          caseStatus = NewCaseCfg.SUBMITTED;
          break;
        case 3:
          caseStatus = NewCaseCfg.FMA_SUBMITTED;
          break;
        case 4:
          caseStatus = NewCaseCfg.COMPLETED;
          break;
        default:
      }

      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: totalTabs,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          key: _scaffoldKey,
          backgroundColor: MyTheme.themeData.accentColor,
          appBar: MyCasesAppbar(
              context: context,
              tabController: _tabController,
              h: getH(context),
              isLoading: isLoading,
              callback: (index2) {
                setState(() {
                  _tabController.index = _tabController.previousIndex;
                });
              },
              callbackWebView: () {
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              }),
          body: TabBarView(
            physics: (isLoading)
                ? NeverScrollableScrollPhysics()
                : AlwaysScrollableScrollPhysics(),
            controller: _tabController,
            children: <Widget>[
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
            ],
          ),
        ),
      ),
    );
  }

  drawRecentCases() {
    try {
      return Container(
        child: (listTaskInfoSearchModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: Theme(
                  data: MyTheme.refreshIndicatorTheme,
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ListView.builder(
                        addAutomaticKeepAlives: true,
                        cacheExtent: AppConfig.page_limit.toDouble(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        //primary: false,
                        itemCount: listTaskInfoSearchModel.length,
                        itemBuilder: (BuildContext context, int index) {
                          return drawRecentCaseItem(index);
                        },
                      ),
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/home/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt:
                                  "Looks like you haven't created any case yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize + .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Create Now!",
                            bgColor: MyTheme.mycasesNFBtnColor,
                            width: getWP(context, 50),
                            height: getHP(context, 8),
                            callback: () {
                              navTo(
                                  context: context,
                                  page: () => NewCase2Screen());
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
      );
    } catch (e) {}
  }

  drawRecentCaseItem(index) {
    try {
      LocationsModel caseModel = listTaskInfoSearchModel[index];
      if (caseModel == null) return SizedBox();
      final icon = NewCaseHelper().getCreateCaseIconByTitle(caseModel.title);
      if (icon == null) return SizedBox();
      final isShowEditButton = (caseModel.status == "DRAFT");
      //final topBadgePos = getHP(context, 2);
      //final endBadgePos = getHP(context, .05);
      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Card(
          color: Colors.white,
          elevation: 2,
          //height: getHP(context, 20),
          //color: Colors.blue,
          /*decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                //spreadRadius: 0,
                //blurRadius: 0,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),*/
          child: Column(
            children: [
              //SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: getWP(context, 12),
                      height: getWP(context, 12),
                      child: Image.asset(
                        icon,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Txt(
                            txt: caseModel.title,
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize + .2,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      ),
                    ),
                    //SizedBox(width: 20),
                    Flexible(
                      child: Container(
                        //color: Colors.black,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            (isShowEditButton)
                                ? IconButton(
                                    onPressed: () {
                                      navTo(
                                          context: context,
                                          page: () => EditCaseScreen(
                                                caseModel: caseModel,
                                              )).then((value) {
                                        //callback(route);
                                      });
                                    },
                                    icon: Image.asset(
                                        "assets/images/icons/edit_icon.png"),
                                    //color: Colors.grey.shade400,
                                    iconSize: 25,
                                  )
                                : SizedBox(),
                            SizedBox(height: (isShowEditButton) ? 5 : 0),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: (isShowEditButton) ? 0 : 25,
                                  bottom: (isShowEditButton) ? 0 : 25),
                              child: IconButton(
                                onPressed: () {
                                  Get.to(
                                    () => WebScreen(
                                      title: caseModel.title,
                                      url: CaseDetailsWebHelper().getLink(
                                          title: caseModel.title,
                                          taskId: caseModel.id),
                                    ),
                                  ).then((value) {
                                    //callback(route);
                                  });
                                },
                                icon: Image.asset(
                                    "assets/images/icons/visible_icon.png"),
                                //color: Colors.grey.shade400,
                                iconSize: 25,
                              ),
                            ),
                            /*Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: MMBtn(
                                      txt: "View",
                                      width: getWP(context, 15),
                                      height: getHP(context, 7),
                                      callback: () {
                                        final LocationsModel caseModel =
                                            listTaskInfoSearchModel[index];
                                         navTo(
        context: context,
        page: () => TaskBiddingScreen(
                                              title: caseModel.title,
                                              taskId: caseModel.id),
                                        ).then((value) {
                                          //callback(route);
                                        });
                                      })),*/
                          ],
                        ),
                      ),
                    ),
                    //SizedBox(width: 5),
                  ],
                ),
              ),
              //SizedBox(height: 5),
            ],
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
