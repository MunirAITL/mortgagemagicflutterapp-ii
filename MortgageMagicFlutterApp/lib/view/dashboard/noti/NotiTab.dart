import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/dashboard/noti/NotiAPIMgr.dart';
import 'package:aitl/controller/helper/tab_noti/NotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/tab_noti/NotiModel.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/BoldTxt.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/MyNetworkImage.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotiTab extends StatefulWidget {
  @override
  State createState() => _NotiTabState();
}

class _NotiTabState extends State<NotiTab> with Mixin, StateListener {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider;

  List<NotiModel> listNotiModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  @override
  onStateChanged(ObserverState state) async {
    isLoading = false;
    if (state == ObserverState.STATE_CHANGED_tabbar4_reload_case_api) {
      _getRefreshData();
    }
  }

  onPageLoad() async {
    setState(() {
      isLoading = true;
    });
    try {
      NotiAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> notifications =
                      model.responseData.notifications;

                  if (notifications != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (notifications.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (NotiModel noti in notifications) {
                        listNotiModel.add(noti);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listNotiModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(msg: "Notifications not found");
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listNotiModel = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: "Notifications",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    try {
      return Container(
        child: (listNotiModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: Theme(
                  data: MyTheme.refreshIndicatorTheme,
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ListView.builder(
                        addAutomaticKeepAlives: true,
                        cacheExtent: AppConfig.page_limit.toDouble(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        //primary: false,
                        itemCount: listNotiModel.length,
                        itemBuilder: (BuildContext context, int index) {
                          return drawNotiItem(index);
                        },
                      ),
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: ListView(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        shrinkWrap: true,
                        children: [
                          Container(
                            width: getWP(context, 100),
                            height: getHP(context, 48),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          //SizedBox(height: 40),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              width: getWP(context, 70),
                              child: Txt(
                                txt:
                                    "Looks like you haven't got any notification yet",
                                txtColor: MyTheme.mycasesNFBtnColor,
                                txtSize: MyTheme.txtSize + .2,
                                txtAlign: TextAlign.center,
                                isBold: false,
                                //txtLineSpace: 1.5,
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: MMBtn(
                              txt: "Refresh",
                              bgColor: MyTheme.mycasesNFBtnColor,
                              width: getWP(context, 50),
                              height: getHP(context, 8),
                              callback: () {
                                onPageLoad();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];
      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap = NotiHelper().getNotiMap(model: notiModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      String initiatorDisplayName = '';
      if ((txt.startsWith(notiModel.initiatorDisplayName))) {
        txt = txt.replaceAll(notiModel.initiatorDisplayName.trim(), '');
        initiatorDisplayName = notiModel.initiatorDisplayName;
      }

      return GestureDetector(
        onTap: () async {
          if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () {
                  _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar3);
                });
          }
        },
        child: Container(
          //height: getHP(context, 25),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            elevation: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20),
                  child: Container(
                    decoration: MyTheme.picEmboseCircleDeco,
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.transparent,
                      backgroundImage: new CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl((notiModel != null)
                            ? notiModel.initiatorImageUrl
                            : MyDefine.MISSING_IMG),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 40, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        BoldTxt(
                          text: txt ?? '',
                          eventName: eventName,
                          initiatorDisplayName: initiatorDisplayName,
                        ),
                        SizedBox(height: 20),
                        Center(
                            child: Txt(
                          txt: notiMap['publishDateTime'],
                          txtColor: MyTheme.mycasesNFBtnColor,
                          txtSize: MyTheme.txtSize - .6,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
