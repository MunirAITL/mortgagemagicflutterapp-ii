import 'dart:convert';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/NewCaseCfg.dart';
import 'package:aitl/controller/api/dashboard/timeline/TimelineAPIMgr.dart';
import 'package:aitl/controller/helper/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/model/json/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/dashboard/timeline/chat/TaskBiddingScreen.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/MyNetworkImage.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<TimeLineTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<TimelineUserModel> listTimelineUserModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int caseStatus = NewCaseCfg.ALL;

  onPageLoad() async {
    try {
      final j =
          '{"Success":true,"ErrorMessages":{},"Messages":{},"ResponseData":{"Users":[{"FirstName":"Ben","LastName":"Stokes","Name":"Ben Stokes","UserName":"benstoke5001","ProfileImageUrl":"https://mortgage-magic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2019-12-19T10:48:00.86","DateCreatedLocal":"2019-12-19T10:48:00.86Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"ben-stokes","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":null,"ReferenceType":null,"Remarks":null,"Community":null,"Locations":null,"Cohort":null,"CommunityId":"9","IsFirstLogin":false,"MobileNumber":"824287682424","DateofBirth":"01-Dec-2019","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"33 Minutes ago","UserProfileUrl":"Ben-Stokes-115773","UserAccesType":0,"Address":null,"Email":"benstoke5001@gmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":null,"MiddleName":null,"AreYouASmoker":null,"AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":115773},{"FirstName":"Bilkis","LastName":"khanom","Name":"Bilkis khanom","UserName":"mk.adv001","ProfileImageUrl":"http://localhost:59082/api//media/uploadpictures/c13632f7dcf04e6c84f1ca3041a56b86_128_128.jpg","CoverImageUrl":"//http://localhost:59082/api","ProfileUrl":null,"DateCreatedUtc":"2020-11-07T08:39:34.067","DateCreatedLocal":"2020-11-07T08:39:34.067Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"bilkis-khanom","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"115765","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"8801717225888526","DateofBirth":"11-Nov-2000","IsMobileNumberVerified":false,"IsProfileImageVerified":true,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"Online","UserProfileUrl":"Bilkis-khanom-117897","UserAccesType":0,"Address":null,"Email":"mk.adv001@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":90948,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":null,"MiddleName":"","AreYouASmoker":"Yes","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117897},{"FirstName":"haf.adv102","LastName":"Rahman","Name":"haf.adv102 Rahman","UserName":"haf.adv102","ProfileImageUrl":"https://mortgage-magic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2021-02-13T09:06:11.853","DateCreatedLocal":"2021-02-13T09:06:11.853Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"hafadv102-rahman","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"116043","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"Male","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"88017142258556","DateofBirth":"11-Nov-1985","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"Online","UserProfileUrl":"haf.adv102-Rahman-117965","UserAccesType":0,"Address":null,"Email":"haf.adv102@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":"Mrs","MiddleName":"","AreYouASmoker":"","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117965},{"FirstName":"Sabita","LastName":"Chowdhury","Name":"Sabita Chowdhury","UserName":"sc.adv001","ProfileImageUrl":"https://mortgage-magic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2020-11-06T10:02:59.923","DateCreatedLocal":"2020-11-06T10:02:59.923Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"sabita-chowdhury","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"115765","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"55785756666","DateofBirth":"11-Nov-2000","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"25-Mar-2021 07:14 ","UserProfileUrl":"Sabita-Chowdhury-117880","UserAccesType":0,"Address":null,"Email":"sc.adv001@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":"Mr","MiddleName":"","AreYouASmoker":"Yes","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117880}]}}';
      final model = TimeLineAdvisorAPIModel.fromJson(json.decode(j));
      if (model != null && mounted) {
        try {
          if (model.success) {
            try {
              final List<dynamic> timelineUserModel = model.responseData.users;
              if (timelineUserModel != null && mounted) {
                //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                if (timelineUserModel.length != pageCount) {
                  isPageDone = true;
                }
                try {
                  for (TimelineUserModel user in timelineUserModel) {
                    listTimelineUserModel.add(user);
                  }
                } catch (e) {
                  log(e.toString());
                }
                log(listTimelineUserModel.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              } else {
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              //final err = model.errorMessages.login[0].toString();
              if (mounted) {
                showToast(msg: "Timelines not found");
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        } catch (e) {
          log(e.toString());
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      } else {
        log("not in");
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      }
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  onPageLoad2() async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> timelineUserModel =
                      model.responseData.users;
                  if (timelineUserModel != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (timelineUserModel.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TimelineUserModel user in timelineUserModel) {
                        listTimelineUserModel.add(user);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listTimelineUserModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(msg: "Timelines not found");
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            log("not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimelineUserModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimelineUserModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: "Message",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listTimelineUserModel.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    onPageLoad();
                  }
                }
                return true;
              },
              child: Theme(
                data: MyTheme.refreshIndicatorTheme,
                child: RefreshIndicator(
                  onRefresh: _getRefreshData,
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: listTimelineUserModel.length,
                      itemBuilder: (context, index) {
                        return drawRecentCaseItem(index);
                      },
                    ),
                  ),
                ),
              ),
            )
          : (!isLoading)
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/home/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt:
                                  "Looks like you haven't got any timeline yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize + .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Refresh",
                            bgColor: MyTheme.mycasesNFBtnColor,
                            width: getWP(context, 50),
                            height: getHP(context, 8),
                            callback: () {
                              onPageLoad();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawRecentCaseItem(index) {
    try {
      TimelineUserModel timelineUserModel = listTimelineUserModel[index];
      final unreadMessageCount = (timelineUserModel.unreadMessageCount == 0);
      final doubletik =
          (unreadMessageCount) ? "check2_icon.png" : "check1_icon.png";
      String lastLoginDate = timelineUserModel.lastLoginDate;
      //try {} catch (e) {}
      return GestureDetector(
        onTap: () async {
          try {
            /*navTo(
              context: context,
              page: () => TaskBiddingScreen(
                  title: timelineUserModel.name, taskId: timelineUserModel. /*--caseModel.id*/),
            ).then((value) {
              //callback(route);
            });*/
          } catch (e) {
            log(e.toString());
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Container(
            //color: Colors.black,
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        decoration: MyTheme.picEmboseCircleDeco,
                        child: CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.transparent,
                          backgroundImage: new CachedNetworkImageProvider(
                            MyNetworkImage.checkUrl(
                                (timelineUserModel.profileImageUrl != null)
                                    ? timelineUserModel.profileImageUrl
                                    : MyDefine.MISSING_IMG),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5),
                          Txt(
                              txt: timelineUserModel.name,
                              txtColor: Colors.grey,
                              txtSize: MyTheme.txtSize + .1,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt: "advisor", //timelineUserModel
                              //.community, //caseModel.companyName,
                              txtColor: MyTheme.redColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Image.asset(
                            "assets/images/icons/" + doubletik,
                            width: 25,
                            height: 25,
                            color: (unreadMessageCount)
                                ? Colors.green
                                : Colors.grey,
                          ),
                        ],
                      ),
                    ),
                    //Spacer(),
                    //SizedBox(width: 10),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: 5),
                          Container(
                            //color: Colors.black,
                            child: Txt(
                                txt: lastLoginDate,
                                txtColor: Colors.grey,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.end,
                                isBold: false),
                          ),
                          SizedBox(height: 20),
                          Container(
                            width: 30,
                            height: 30,
                            child: Center(
                              child: Txt(
                                  txt:
                                      timelineUserModel.companyCount.toString(),
                                  txtColor: Colors.white,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: MyTheme.redColor),
                          ),
                        ],
                      ),
                    ),
                    //SizedBox(width: 5),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: 20, left: getWP(context, 15), right: 20),
                  child: Container(color: Colors.grey, height: .5),
                ),
                //SizedBox(height: 20),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
