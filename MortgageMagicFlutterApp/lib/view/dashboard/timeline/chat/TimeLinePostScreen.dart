import 'dart:async';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/dashboard/timeline/TimelineChatAPIMgr.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/controller/helper/tab_timeline/TimeLinePostHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_timeline/TaskBiddingModel.dart';
import 'package:aitl/model/json/tab_timeline/TimeLinePostModel.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/MyNetworkImage.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:permission_handler/permission_handler.dart';
import 'utils/ChatBubble.dart';
// Or import PubNub into a named namespace
//import 'package:pubnub/pubnub.dart' as pn;
import 'package:jiffy/jiffy.dart';

class TimeLinePostScreen extends StatefulWidget {
  final String title;
  final int taskId;
  final TaskBiddingModel taskBiddingModel;

  const TimeLinePostScreen({
    Key key,
    @required this.title,
    @required this.taskId,
    @required this.taskBiddingModel,
  }) : super(key: key);
  @override
  State createState() => _TimeLinePostScreenState();
}

class _TimeLinePostScreenState extends State<TimeLinePostScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController textController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  List<TimeLinePostModel> listTimeLineModel = [];

  Timer timerGetTimeLine;
  Timer timerScrollToBottom;
  static const int callTimelineSec = 35;
  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;

  String msgTmp = '';

  postTimelineAPI() async {
    try {
      isLoading = true;
      final param = TimeLinePostHelper().getParam(
        message: msgTmp,
        postTypeName: "status",
        additionalAttributeValue: null,
        inlineTags: [],
        checkin: "",
        fromLat: 0,
        fromLng: 0,
        smallImageName: "",
        receiverId: widget.taskBiddingModel.userId,
        ownerId: 0,
        taskId: widget.taskId,
        isPrivate: true,
      );
      log(param.toString());
      TimelineChatAPIMgr().wsPostTimelineAPI(
          context: context,
          param: param,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  onPageLoad(1);
                } else {}
              } catch (e) {
                log(e.toString());
              }
            }
          });
    } catch (e) {
      log(e.toString());
      isLoading = false;
    }
  }

  onPageLoad(int startPage) async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineChatAPIMgr().wsOnPageLoad(
        context: context,
        startPage: startPage,
        pageCount: pageCount,
        taskId: widget.taskId,
        taskBiddingUserId: widget.taskBiddingModel.userId,
        callback: (model) {
          if (model != null && mounted) {
            msgTmp = '';
            try {
              if (model.success) {
                try {
                  final List<dynamic> timeLines =
                      model.responseData.timelinePosts;
                  if (timeLines != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility

                    try {
                      setState(() {
                        isLoading = false;
                      });
                      if (listTimeLineModel.length > 0) {
                        try {
                          TimeLinePostModel tlModel1 = listTimeLineModel[0];
                          TimeLinePostModel tlModel2 =
                              model.responseData.timelinePosts[0];
                          if (tlModel1.id == tlModel2.id) {
                            return;
                          }
                        } catch (e) {
                          log(e.toString());
                        }
                      }

                      //await _removeFromMeMsg();
                      if (startPage == 1) {
                        listTimeLineModel.clear();
                        //await _removeFromMeMsg();
                      }
                      for (TimeLinePostModel timeLine in timeLines) {
                        //if (!listTimeLineModel.contains(timeLine))
                        listTimeLineModel.add(timeLine);
                      }
                      setState(() {
                        if (timeLines.length != pageCount) {
                          isPageDone = true;
                        }
                      });
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listTimeLineModel.toString());
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      //initPubNub();
    } catch (e) {}
  }

  /*_removeFromMeMsg() async {
    try {
      for (TimeLinePostModel tlModel in listTimeLineModel) {
        try {
          if (tlModel.isFromMe) {
            listTimeLineModel.remove(tlModel);
          }
        } catch (e) {}
      }
    } catch (e) {}
  }*/

  //  ******************* Pub Nub Start Here....

  /*initPubNub() async {
    userModel =  DashBoardScreenState.userModel;
    final pubnub = pn.PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(await Common.getUDID(context))));

    final param = TimeLinePostHelper().getParam(
      message: "testinggggg pubnub",
      postTypeName: "status",
      additionalAttributeValue: null,
      inlineTags: [],
      checkin: "",
      fromLat: 0,
      fromLng: 0,
      smallImageName: "",
      receiverId: widget.receiverId,
      ownerId: 0,
      senderId: userModel.id,
      taskId: widget.taskId,
      isPrivate: true,
    );

    final myChannel = pubnub.channel(userModel.id.toString());
    myChannel.publish({
      'data': param,
      'user': userModel.id,
    });

    var subscription = pubnub.subscribe(channels: {userModel.id.toString()});

    subscription.messages.listen((envelope) {
      print('${envelope.uuid} sent a message: ${envelope.payload}');
    });

    var history = myChannel.history(chunkSize: 50);
    log(history.toString());

    appInit();
    /*myChannel.subscribe().messages.listen((envelope) {
      print(envelope.payload);
    });*/
  }*/

  //  ******************* Pub Nub End Here....
  @override
  void dispose() {
    listTimeLineModel = null;
    textController.dispose();
    _scrollController.dispose();
    _scrollController = null;
    try {
      timerScrollToBottom = null;
      timerGetTimeLine?.cancel();
      timerGetTimeLine = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      await onPageLoad(1);
      pageStart++;
      timerGetTimeLine =
          Timer.periodic(Duration(seconds: callTimelineSec), (Timer t) {
        //  call to ws for getting any new msg of the sender
        if (!isLoading && msgTmp.length == 0) {
          onPageLoad(1);
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Widget buildSingleMessage(index) {
    try {
      TimeLinePostModel timelineModel = listTimeLineModel[index];
      bool fromMe =
          (timelineModel.senderId == userData.userModel.id) ? true : false;

      //bool isFromMeSent = false;
      String lastMsgDate;
      DateTime date1;
      try {
        //if (index  <= listTimeLineModel.length) {
        TimeLinePostModel timelineModel2 = listTimeLineModel[(index + 1)];
        //var jiffy1 = Jiffy(timelineModel2.dateCreated);
        //var jiffy2 = Jiffy(timelineModel2.dateCreated)..date;

        date1 = Jiffy(timelineModel.dateCreated).dateTime;
        DateTime date2 = Jiffy(timelineModel2.dateCreated).dateTime;
        //final days = (jiffy2.diff(jiffy1.date));
        if (date1.day > date2.day) {
          // log(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
          //isFromMeSent = true;
        }
        //}
      } catch (e) {
        log(e.toString());
      }

      Alignment alignment = fromMe ? Alignment.topRight : Alignment.topLeft;
      Alignment chatArrowAlignment =
          fromMe ? Alignment.topRight : Alignment.topLeft;

      Color chatBgColor = fromMe ? MyTheme.redColor : MyTheme.pinkColor;
      EdgeInsets edgeInsets = fromMe
          ? EdgeInsets.fromLTRB(5, 5, 15, 5)
          : EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = fromMe
          ? EdgeInsets.fromLTRB(80, 5, 10, 5)
          : EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //(!isFromMeSent && fromMe && timelineModel.fromMeDate != null)

            (fromMe && lastMsgDate != null)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Center(
                      child: Txt(
                          txt: lastMsgDate,
                          txtColor: Colors.grey,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  )
                : SizedBox(),
            Row(
              children: [
                (!fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    margin: margins,
                    child: Align(
                      alignment: alignment,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomPaint(
                            painter: ChatBubble(
                              color: chatBgColor,
                              alignment: chatArrowAlignment,
                            ),
                            child: Container(
                              margin: EdgeInsets.all(10),
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: edgeInsets,
                                    child: Txt(
                                        txt: timelineModel.message
                                            .toString()
                                            .trim(),
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .3,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                  ),
                ),
                (fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
              ],
            ),

            Align(
              alignment:
                  (fromMe) ? Alignment.bottomRight : Alignment.bottomLeft,
              child: Padding(
                padding: (!fromMe)
                    ? EdgeInsets.only(left: getWP(context, 13))
                    : EdgeInsets.only(right: getWP(context, 13)),
                child: Txt(
                    txt: getTimeAgoTxt(timelineModel.dateCreated),
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return Container(
      decoration: MyTheme.picEmboseCircleDeco,
      child: CircleAvatar(
        radius: 25.0,
        backgroundColor: Colors.transparent,
        backgroundImage:
            new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
      ),
    );
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            if (!isLoading && !isPageDone && msgTmp.length == 0) {
              onPageLoad(pageStart++);
            }
          }
          return true;
        },
        child: ListView.builder(
          controller: _scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            return buildSingleMessage(index);
          },
        ),
      ),
    );
  }

  _bottomChatArea() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          _chatTextArea(),
          /*GestureDetector(
            onLongPressStart: (details) async {
              bool hasPermission = await checkPermission();
              if (hasPermission) {
                log("start recording");
                // Check and request permission
                Directory tempDir = await getTemporaryDirectory();
                String tempPath = tempDir.path;
                // https://pub.dev/packages/record_mp3
                //start record
                RecordMp3.instance.start(tempPath, (type) {
                  // record fail callback
                  log(type);
                });
              } else {
                log("cannot start recording:: permission not allowed");
              }
            },
            onLongPressEnd: (details) {
              log("end recording");
              //complete record and export a record file
              RecordMp3.instance.stop();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                child: Image.asset(
                  "assets/images/icons/mic_icon.png",
                  width: 60,
                  height: 60,
                ),
              ),
            ),
          ),*/
          IconButton(
            iconSize: 30,
            icon: Icon(
              Icons.send,
              color: MyTheme.dgreyColor,
            ),
            onPressed: () async {
              //Check if the textfield has text or not
              FocusScope.of(context).requestFocus(FocusNode());
              onSendClicked();
            },
          ),
        ],
      ),
    );
  }

  onSendClicked() async {
    try {
      if (textController.text.isNotEmpty) {
        //Add the message to the list
        String formattedDate = DateFormat('dd-MMM-yyyy').format(DateTime.now());
        if (mounted) {
          //Scrolldown the list to show the latest message
          msgTmp = textController.text.trim();
          final TimeLinePostModel timeLinePostModel = TimeLinePostModel();
          timeLinePostModel.senderId = userData.userModel.id;
          timeLinePostModel.ownerId = userData.userModel.id;
          timeLinePostModel.ownerImageUrl = userData.userModel.profileImageURL;
          timeLinePostModel.message = msgTmp;
          timeLinePostModel.dateCreatedUtc = formattedDate;
          timeLinePostModel.isFromMe = true;
          listTimeLineModel.insert(0, timeLinePostModel);
          textController.text = '';
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
          //_chatListScrollToBottom();
          //setState(() {});
          //Future.delayed(Duration(seconds: 5), () async {
          pageStart = 1;
          await postTimelineAPI();
          //});
        }
      }
    } catch (e) {}
  }

  _chatTextArea() {
    /*return Expanded(
      child: Container(
        decoration: new BoxDecoration(
            color: MyTheme.pinkColor,
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(40.0),
              topRight: const Radius.circular(40.0),
              bottomLeft: const Radius.circular(40.0),
              bottomRight: const Radius.circular(40.0),
            )),
        child: Row(
          children: [
            IconButton(
                iconSize: 30,
                icon: Icon(
                  Icons.tag_faces,
                  color: MyTheme.redColor,
                ),
                onPressed: () {}),
            Expanded(
              child: TextField(
                controller: textController,
                textInputAction: TextInputAction.send,
                textAlign: TextAlign.center,
                onSubmitted: (value) {
                  onSendClicked();
                },
                maxLength: 255,
                autocorrect: false,
                style: TextStyle(
                  color: Colors.white,
                  fontSize:
                      getTxtSize(context: context, txtSize: MyTheme.txtSize),
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintStyle: TextStyle(color: Colors.white),
                  counter: Offstage(),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
                  hintText: 'Type your message...',
                ),
              ),
            ),
            IconButton(
                iconSize: 30,
                icon: Icon(
                  Icons.attach_file,
                  color: MyTheme.redColor,
                ),
                onPressed: () {}),
          ],
        ),
      ),
    );*/
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Container(
          decoration: new BoxDecoration(
              color: MyTheme.greyColor,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(40.0),
                topRight: const Radius.circular(40.0),
                bottomLeft: const Radius.circular(40.0),
                bottomRight: const Radius.circular(40.0),
              )),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: IconButton(
                    iconSize: 30,
                    icon: Icon(
                      Icons.tag_faces,
                      color: Colors.black54,
                    ),
                    onPressed: () {}),
              ),
              Expanded(
                child: TextField(
                  controller: textController,
                  textInputAction: TextInputAction.send,
                  textAlign: TextAlign.center,
                  onSubmitted: (value) {
                    onSendClicked();
                  },
                  maxLength: 255,
                  autocorrect: false,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.black54),
                    counter: Offstage(),
                    contentPadding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
                    hintText: 'Type a message...',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Message",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.redColor),
          centerTitle: true,
          bottom: PreferredSize(
            preferredSize: new Size(getW(context), getHP(context, 10)),
            child: Column(
              children: [
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : SizedBox(),
                Divider(color: Colors.black, height: 10),
                Container(
                  //color: Colors.black,
                  //height: geth,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 3,
                          child: Txt(
                              txt: widget.taskBiddingModel.ownerName,
                              txtColor: MyTheme.timelinePostCallerNameColor,
                              txtSize: MyTheme.txtSize + 1,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),
                        // SizedBox(width: 5),
                        ProfileHelper().getUserOnlineStatus(
                            context: context,
                            statusIndex:
                                (widget.taskBiddingModel.isInPersonOrOnline)
                                    ? 1
                                    : 0,
                            size: 3),
                        Expanded(child: SizedBox()),
                        Expanded(
                          child: IconButton(
                            iconSize: 40,
                            icon: Image.asset(
                                'assets/images/icons/phone_icon.png'),
                            onPressed: () async {
                              openUrl(context,
                                  "tel://" + userData.userModel.mobileNumber);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (mounted) ? drawChatUI() : SizedBox(),
        ),
      ),
    );
  }

  drawChatUI() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          buildMessageList(),
          //Divider(height: 1.0),
          _bottomChatArea(),
        ],
      ),
    );
  }

  Future<bool> checkPermission() async {
    Map<PermissionGroup, PermissionStatus> map = await new PermissionHandler()
        .requestPermissions(
            [PermissionGroup.storage, PermissionGroup.microphone]);
    print(map[PermissionGroup.microphone]);
    return map[PermissionGroup.microphone] == PermissionStatus.granted;
  }
}
