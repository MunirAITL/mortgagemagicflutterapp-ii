import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/dashboard/timeline/TaskbidAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_timeline/TaskBiddingModel.dart';
import 'package:aitl/view/dashboard/timeline/chat/TimeLinePostScreen.dart';
import 'package:aitl/view/mywidgets/AppbarBotProgbar.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/MyNetworkImage.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class TaskBiddingScreen extends StatefulWidget {
  final String title;
  final int taskId;

  const TaskBiddingScreen({
    Key key,
    @required this.title,
    @required this.taskId,
  }) : super(key: key);
  @override
  State createState() => _TaskBiddingScreenState();
}

class _TaskBiddingScreenState extends State<TaskBiddingScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<TaskBiddingModel> listTaskBiddingModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  onPageLoad() async {
    try {
      TaskbidAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        taskId: widget.taskId,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> taskBiddings =
                      model.responseData.taskBiddings;

                  if (taskBiddings != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (taskBiddings.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TaskBiddingModel taskBidding in taskBiddings) {
                        listTaskBiddingModel.add(taskBidding);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listTaskBiddingModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(msg: "Taskbiddings not found");
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskBiddingModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTaskBiddingModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Txt(
                txt: widget.title,
                txtColor: MyTheme.redColor,
                txtSize: MyTheme.appbarTitleFontSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          centerTitle: true,
          leading: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: IconButton(
              icon: Icon(Icons.arrow_back, color: MyTheme.redColor),
              onPressed: () async {
                Navigator.pop(context);
              },
            ),
          ),
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: (listTaskBiddingModel.length > 0)
            ? Container(
                child: Theme(
                  data: MyTheme.refreshIndicatorTheme,
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ListView.builder(
                        itemCount: listTaskBiddingModel.length,
                        itemBuilder: (context, index) {
                          return drawMessageList(index);
                        },
                      ),
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: ListView(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        shrinkWrap: true,
                        children: [
                          Container(
                            width: getWP(context, 100),
                            height: getHP(context, 48),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          //SizedBox(height: 40),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              width: getWP(context, 70),
                              child: Txt(
                                txt:
                                    "Looks like you haven't got any taskbidding yet",
                                txtColor: MyTheme.mycasesNFBtnColor,
                                txtSize: MyTheme.txtSize + .2,
                                txtAlign: TextAlign.center,
                                isBold: false,
                                //txtLineSpace: 1.5,
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: MMBtn(
                              txt: "Refresh",
                              bgColor: MyTheme.mycasesNFBtnColor,
                              width: getWP(context, 50),
                              height: getHP(context, 8),
                              callback: () {
                                onPageLoad();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      ),
    );
  }

  drawMessageList(int index) {
    try {
      final TaskBiddingModel taskBiddingModel = listTaskBiddingModel[index];
      return GestureDetector(
        onTap: () async {
          try {
            navTo(
              context: context,
              page: () => TimeLinePostScreen(
                title: widget.title,
                taskId: widget.taskId,
                taskBiddingModel: taskBiddingModel,
              ),
            ).then((value) {
              //callback(route);
            });
          } catch (e) {}
        },
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        decoration: MyTheme.picEmboseCircleDeco,
                        child: CircleAvatar(
                          radius: 25,
                          backgroundColor: Colors.transparent,
                          backgroundImage: new CachedNetworkImageProvider(
                              MyNetworkImage.checkUrl((taskBiddingModel != null)
                                  ? taskBiddingModel.ownerImageUrl
                                  : MyDefine.MISSING_IMG)),
                        ),
                      ),
                    ),
                    //SizedBox(width: 10),
                    Expanded(
                      flex: 5,
                      child: Container(
                        //color: Colors.black,
                        //width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Txt(
                                txt: taskBiddingModel.ownerName,
                                txtColor: MyTheme.timelineTitleColor,
                                txtSize: MyTheme.txtSize + .3,
                                txtAlign: TextAlign.start,
                                isBold: true),
                            SizedBox(height: 10),
                            Txt(
                                txt: taskBiddingModel.communityName,
                                txtColor: MyTheme.redColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      flex: 2,
                      child: Badge(
                        showBadge:
                            (taskBiddingModel.totalComments > 0) ? true : false,
                        badgeContent: Text(
                          taskBiddingModel.totalComments.toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        //position: BadgePosition.topEnd(
                        //top: -topBadgePos, end: -endBadgePos),
                        child: Icon(
                          Icons.message,
                          color: MyTheme.redColor,
                          size: 30,
                        ),
                      ),
                    ),
                    //SizedBox(width: 10),
                  ],
                ),
                //SizedBox(height: 20),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
