import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/dashboard/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/helper/more/badge/BadgeHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/mywidgets/ImageHoverPH.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class BadgePhotoIDScreen extends StatefulWidget {
  List<UserBadgeModel> listUserBadgeModel;
  BadgePhotoIDScreen({
    Key key,
    @required this.listUserBadgeModel,
  }) : super(key: key);
  @override
  State createState() => _BadgePhotoIDScreenState();
}

class _BadgePhotoIDScreenState extends State<BadgePhotoIDScreen> with Mixin {
  UserBadgeModel nationalIDCardModel, nationalIDCardModel2;
  BadgeType badgeType;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    nationalIDCardModel = null;
    nationalIDCardModel2 = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  browseFiles() async {
    try {
      try {
        FilePickerResult result = await FilePicker.platform.pickFiles(
          allowMultiple: false,
          type: FileType.custom,
          allowedExtensions: [
            'jpg',
            'jpeg',
            'png',
          ],
        );

        if (result != null) {
          MediaUploadAPIMgr().wsMediaUploadFileAPI(
            context: context,
            file: File(result.files.single.path),
            callback: (model) async {
              if (model != null && mounted) {
                try {
                  if (model.success) {
                    //listMediaUploadFilesModel.add(model.responseData.images[0]);
                    final refrenceUrl = model.responseData.images[0].url;
                    await BadgeAPIMgr().wsPostPhotoIDBadge(
                        context: context,
                        refrenceUrl: refrenceUrl,
                        callback: (BadgePhotoIDAPIModel model2) {
                          try {
                            widget.listUserBadgeModel =
                                BadgeHelper().updateListBadgeAPIModel(
                              widget.listUserBadgeModel,
                              model2.responseData.userBadge,
                              badgeType,
                            );
                            log(widget.listUserBadgeModel.toString());
                            setState(() {});
                          } catch (e) {}
                        });
                  } else {
                    final err =
                        model.errorMessages.upload_pictures[0].toString();
                    showToast(msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            },
          );
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {
      log(e.toString());
    }
  }

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 160,
                  elevation: 0,
                  backgroundColor: MyTheme.themeData.accentColor,
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,

                  title: Txt(
                      txt: "Upload Documents",
                      txtColor: Colors.black,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    try {
      nationalIDCardModel = BadgeHelper()
          .getBadgeByType(widget.listUserBadgeModel, BadgeType.NationalIDCard);
    } catch (e) {}
    try {
      nationalIDCardModel2 = BadgeHelper()
          .getBadgeByType(widget.listUserBadgeModel, BadgeType.NationalIDCard2);
      log(nationalIDCardModel2);
    } catch (e) {}

    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 30, right: 30, bottom: 30),
      child: Container(
        color: Colors.white,
        child: ListView(
          shrinkWrap: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt:
                      "You may upload a maximum of 2 items. File formats can be JPG/PNG and must be no larger than 5MB. For your own security and privacy, It will not shown details information at public profile.",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Column(
              children: [
                ImageHoverPH(
                  id: nationalIDCardModel.id,
                  url: nationalIDCardModel.refrenceUrl,
                  badgeType: BadgeType.NationalIDCard,
                  callback: (type) {
                    badgeType = type;
                    browseFiles();
                  },
                  callbackDelbyID: (int id) {
                    BadgeAPIMgr().wsDelPhotoIDBadge(
                        context: context,
                        id: id,
                        callback: (BadgePhotoIDAPIModel model) {
                          //
                        });
                  },
                ),
                ImageHoverPH(
                  id: nationalIDCardModel.id,
                  url: nationalIDCardModel2.refrenceUrl,
                  badgeType: BadgeType.NationalIDCard2,
                  callback: (type) {
                    badgeType = type;
                    browseFiles();
                  },
                  callbackDelbyID: (int id) {
                    BadgeAPIMgr().wsDelPhotoIDBadge(
                        context: context,
                        id: id,
                        callback: (BadgePhotoIDAPIModel model) {
                          //
                        });
                  },
                ),
                //ImageHoverPH(url: userModel.profileImageURL),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt:
                      "Uploaded document is pending verification from mortgage-magic.co.uk",
                  txtColor: Colors.red,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ],
        ),
      ),
    );
  }
}
