import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/dashboard/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/dashboard/more/badges/BadgePhotoIDScreen.dart';
import 'package:aitl/view/mywidgets/webview/EIDScreen.dart';
import 'package:aitl/view/mywidgets/BtnOutline.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BadgeScreen extends StatefulWidget {
  @override
  State createState() => _BadgeScreenState();
}

class _BadgeScreenState extends State<BadgeScreen> with Mixin {
  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  UserBadgeModel userBadgeModel;

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;
              } else {
                //showToast(msg: model.errorMessages.toString());
                showToast(msg: "Sorry, something went wrong");
              }
            } catch (e) {
              log(e.toString());
            }
          } else {
            log("not in");
          }
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listItems = null;
    listUserBadgeModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      listItems = [
        {
          "icon": "assets/images/screens/home/more/badge/badge_phone_icon.png",
          "title": "Mobile",
          "desc":
              "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
          "btnTitle": null,
        },
        {
          "icon": "assets/images/screens/home/more/badge/badge_email_icon.png",
          "title": "Email",
          "desc":
              "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
          "btn": BtnOutline(
              txt: "Send Email",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                //
                BadgeAPIMgr().wsPostEmailBadge(
                    context: context,
                    callback: (BadgeEmailAPIModel model) {
                      if (mounted) {
                        if (model.success) {
                          showAlert(
                              msg:
                                  "We have sent an email for verification, please check your email");
                        } else {
                          //showAlert(msg: model.errorMessages.);
                          showAlert(msg: "Sorry, something went wrong");
                        }
                      }
                    });
              }),
        },
        {
          "icon": "assets/images/screens/home/more/badge/badge_pp_icon.png",
          "title": "Passport / Photo ID",
          "desc":
              "You receive this badge when your ID is verified with your Passport, Driving License or any other acceptable form of ID.",
          "btn": BtnOutline(
              txt: "Add",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                Get.to(() => BadgePhotoIDScreen(
                      listUserBadgeModel: listUserBadgeModel,
                    )).then((value) => updateUserBadges());
              }),
        },
        {
          "icon": "assets/images/screens/home/more/badge/badge_fb_icon.png",
          "title": "Facebook",
          "desc":
              "You receive this badge when you connect your Facebook account.",
          "btn": BtnOutline(
              txt: "Add",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                //
              }),
        },
        {
          "icon": "assets/images/screens/home/more/badge/badge_prv_icon.png",
          "title": "Customer Privacy",
          "desc":
              "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at 16-Mar-2020 18:43",
          "btn": BtnOutline(
              txt: "Download",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                //
              }),
        },
        {
          "icon": "assets/images/screens/home/more/badge/badge_eid_icon.png",
          "title": "Electronic ID Verification",
          "desc":
              "You receive this badge when your ID is verified through an EID system.",
          "btn": BtnOutline(
              txt: "Add",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                //
                Get.to(
                  () => EIDScreen(),
                  fullscreenDialog: true,
                );
              }),
        },
        //"Application tutorial"
      ];
    } catch (e) {}

    try {
      updateUserBadges();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                    //expandedHeight: 160,
                    elevation: 0,
                    //toolbarHeight: getHP(context, 10),
                    backgroundColor: MyTheme.themeData.accentColor,
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: false,
                    title: Txt(
                        txt: "Badges",
                        txtColor: Colors.black,
                        txtSize: MyTheme.appbarTitleFontSize,
                        txtAlign: TextAlign.start,
                        isBold: false)),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            SizedBox(height: 10),
            Txt(
                txt:
                    "Badges are issued when specific requirements are met. A green tick shows that the verification is currently active.",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                //txtLineSpace: 1.2,
                isBold: true),
            SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                BtnOutline btn = item['btn'] as BtnOutline;

                return Card(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: ListTile(
                      // leading: Image.asset(item['icon']),
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              width: getWP(context, 10),
                              height: getWP(context, 10),
                              child: Image.asset(item['icon'])),
                          SizedBox(width: 10),
                          Expanded(
                            child: Txt(
                                txt: item["title"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isOverflow: true,
                                isBold: true),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: item["desc"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                //txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          btn ?? SizedBox(),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
