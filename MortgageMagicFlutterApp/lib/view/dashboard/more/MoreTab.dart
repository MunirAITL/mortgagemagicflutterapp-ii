import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/more/MoreHelper.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/dashboard/DashboardScreen.dart';
import 'package:aitl/view/dashboard/noti/NotiTab.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoreTab extends StatefulWidget {
  @override
  State createState() => _MoreTabState();
}

class _MoreTabState extends State<MoreTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "More",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            child: ListView.builder(
              itemCount: MoreHelper.listMore.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> mapMore = MoreHelper.listMore[index];
                return GestureDetector(
                  onTap: () async {
                    if (mounted) {
                      Type route = mapMore['route'];
                      if (route != null) {
                        if (identical(route, NotiTab)) {
                          _stateProvider
                              .notify(ObserverState.STATE_CHANGED_tabbar4);
                        } else {
                          await MoreHelper().setRoute(
                              context: context,
                              route: route,
                              callback: (route2) {});
                        }
                      } else {
                        //  signout
                        DashBoardScreenState.currentTab = 0;
                        _stateProvider
                            .notify(ObserverState.STATE_CHANGED_logout);
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: ListTile(
                          title: Txt(
                              txt: mapMore['title'].toString(),
                              txtColor: MyTheme.moreTxtColor,
                              txtSize: MyTheme.txtSize + .3,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          trailing: (index != MoreHelper.listMore.length - 1)
                              ? Icon(
                                  Icons.arrow_forward_ios,
                                  color: MyTheme.moreTxtColor,
                                  //size: 20,
                                )
                              : SizedBox(),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
