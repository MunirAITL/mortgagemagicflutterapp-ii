import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/dashboard/more/help/SupportCentreScreen.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  List<String> listItems = [
    "Support centre",
    "Terms and conditions",
    "Privacy",
    "Application tutorial"
  ];

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Help",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ListView.builder(
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                return GestureDetector(
                  onTap: () async {
                    switch (index) {
                      case 0: //  Support Center
                        navTo(
                          context: context,
                          page: () => SupportCentreScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 1: //  TC
                        Get.to(
                          () => WebScreen(
                            title: "Terms & Conditions",
                            url: Server.TC_URL,
                          ),
                        ).then((value) {
                          //callback(route);
                        });

                        break;
                      case 2: //  Privacy
                        Get.to(
                          () => WebScreen(
                            title: "Privacy",
                            url: Server.PRIVACY_URL,
                          ),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 3: //  Application Tuturial
                        Navigator.pop(context);
                        _stateProvider
                            .notify(ObserverState.STATE_CHANGED_tabbar1);
                        break;
                      default:
                    }
                  },
                  child: Card(
                    child: ListTile(
                      title: Txt(
                          txt: item,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      trailing: Icon(
                        Icons.arrow_right,
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
