import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/dashboard/more/help/ResolutionScreen.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Support",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.redColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {
            navTo(
              context: context,
              page: () => ResolutionScreen(),
            ).then((value) {
              //callback(route);
            })
          },
        ),
        body: drawSupportButtons(context),
      ),
    );
  }

  drawSupportButtons(context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMBtn(
              txt: "Email: " + AppDefine.SUPPORT_EMAIL,
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMBtn(
              txt: "Call: " + AppDefine.SUPPORT_CALL,
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMBtn(
              txt: "Send Message",
              width: getW(context),
              height: getHP(context, 7),
              radius: 10,
              callback: () async {
                navTo(
                  context: context,
                  page: () => ResolutionScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
