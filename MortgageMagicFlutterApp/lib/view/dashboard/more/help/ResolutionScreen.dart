import 'dart:developer';
import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/dashboard/more/help/ResolutionAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/helper/more/ResolutionHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:file_picker/file_picker.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends State<ResolutionScreen> with Mixin {
  final TextEditingController _desc = TextEditingController();

  static const int MAX_FILE_UPLOAD = 5;

  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];
  ResolutionHelper resolutionHelper;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _desc.dispose();
    resolutionHelper = null;
    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  browseFiles() async {
    try {
      FilePickerResult result = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.any,
        //type: FileType.custom,
        /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
      );

      if (result != null) {
        //wsMediaUploadFileAPI(File(result.files.single.path));
        if (listMediaUploadFilesModel.length > MAX_FILE_UPLOAD) {
          showToast(
              msg: "Maximum file upload limit is " +
                  MAX_FILE_UPLOAD.toString() +
                  " files");
          return;
        }
        MediaUploadAPIMgr().wsMediaUploadFileAPI(
          context: context,
          file: File(result.files.single.path),
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  listMediaUploadFilesModel.add(model.responseData.images[0]);
                  setState(() {});
                } else {
                  final err = model.errorMessages.upload_pictures[0].toString();
                  showToast(msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      log(e.toString());
    }
  }

  appInit() async {
    try {
      resolutionHelper = ResolutionHelper();
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Contact us",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawEmailUI(),
        ),
      ),
    );
  }

  drawEmailUI() {
    return (resolutionHelper == null)
        ? SizedBox()
        : ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: DropDownPicker(
                  cap: "Support Ticket Type",
                  itemSelected: resolutionHelper.opt,
                  dropListModel: resolutionHelper.dd,
                  onOptionSelected: (optionItem) {
                    resolutionHelper.opt = optionItem;
                    setState(() {});
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Txt(
                    txt: "How can we help you?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextField(
                  controller: _desc,
                  minLines: 5,
                  maxLines: 10,
                  //expands: true,
                  autocorrect: false,
                  maxLength: 500,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    hintText: 'Description',
                    hintStyle: TextStyle(color: Colors.grey),
                    //labelText: 'Your message',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Txt(
                    txt: "Attachments - " +
                        listMediaUploadFilesModel.length.toString() +
                        ' files added',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              for (MediaUploadFilesModel model in listMediaUploadFilesModel)
                Card(
                  child: ListTile(
                    leading: IconButton(
                        icon: Icon(
                          Icons.remove_circle,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          listMediaUploadFilesModel.remove(model);
                          setState(() {});
                        }),
                    title: Align(
                      alignment: Alignment(-1.2, 0),
                      child: Txt(
                          txt: model.name.split('/').last ?? '',
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: DottedBorder(
                  borderType: BorderType.RRect,
                  radius: Radius.circular(12),
                  padding: EdgeInsets.all(6),
                  color: Colors.grey,
                  strokeWidth: 3,
                  child: GestureDetector(
                    onTap: () async {
                      await browseFiles();
                    },
                    child: Container(
                      height: getHP(context, 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.attach_file,
                            color: MyTheme.themeData.accentColor,
                            size: 30,
                          ),
                          Txt(
                              txt: "Add upto 5 files",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: MMBtn(
                  txt: "Submit",
                  width: getW(context),
                  height: getHP(context, 7),
                  radius: 10,
                  callback: () {
                    if (resolutionHelper.opt.id == null) {
                      showToast(msg: "Please choose ticket type from the list");
                      return;
                    } else if (_desc.text.trim().length == 0) {
                      showToast(msg: "Please enter description");
                      return;
                    }
                    List<String> listFileUrl = [];
                    for (MediaUploadFilesModel model
                        in listMediaUploadFilesModel) {
                      listFileUrl.add(model.url);
                    }

                    ResolutionAPIMgr().wsResolutionAPI(
                      context: context,
                      resolutionHelper: resolutionHelper,
                      desc: _desc.text.trim(),
                      listFileUrl: listFileUrl,
                      callback: (model) {
                        if (model != null && mounted) {
                          try {
                            if (model.success) {
                              _desc.text = "";
                              listMediaUploadFilesModel.clear();
                              final msg =
                                  model.messages.resolution_post[0].toString();
                              showToast(msg: msg, which: 1);
                              setState(() {});
                            } else {}
                          } catch (e) {
                            log(e.toString());
                          }
                        }
                      },
                    );
                  },
                ),
              ),
              SizedBox(height: getHP(context, 5)),
            ],
          );
  }
}
