import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/dashboard/more/settings/ChangePwdAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangePwdScreen extends StatefulWidget {
  @override
  State createState() => _ChangePwdScreenState();
}

class _ChangePwdScreenState extends State<ChangePwdScreen> with Mixin {
  final TextEditingController _curPwd = TextEditingController();
  final TextEditingController _newPwd = TextEditingController();
  final TextEditingController _newPwd2 = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _newPwd.dispose();
    _newPwd2.dispose();
    _curPwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {}

  validate() {
    if (!UserProfileVal().isEmpty(_curPwd, 'Please enter current password')) {
      return false;
    }
    if (!UserProfileVal().isEmpty(_newPwd, 'Please enter password')) {
      return false;
    }
    if (!UserProfileVal().isEmpty(_newPwd2, 'Please enter cofirm password')) {
      return false;
    }
    if (_newPwd.text.trim() != _newPwd2.text.trim()) {
      showToast(msg: "Confirm password does not match");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          titleSpacing: 0,
          title: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              width: 100,
              height: 60,
              child: Image.asset(
                "assets/images/icons/back_btn_icon.png",
              ),
            ),
          ),
          actions: [
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 0, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                  child: Txt(
                    txt: "Change your password",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1.3,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 10, bottom: 5),
                  child: Txt(
                    txt: "Please type new password.",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: drawInputBox(
                      title: "Current Password",
                      input: _curPwd,
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: drawInputBox(
                      title: "New Password",
                      input: _newPwd,
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: drawInputBox(
                      title: "Repeat Password",
                      input: _newPwd2,
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 20, left: 20, right: 20, bottom: 20),
                  child: MMBtn(
                      txt: "Reset Password",
                      height: getHP(context, 7),
                      width: getW(context),
                      callback: () {
                        if (validate()) {
                          ChangePwdAPIMgr().wsChangePwdAPI(
                            context: context,
                            curPwd: _curPwd.text.trim(),
                            newPwd: _newPwd.text.trim(),
                            newPwd2: _newPwd2.text.trim(),
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    try {
                                      final msg = model
                                          .messages.changePassword[0]
                                          .toString();
                                      showToast(msg: msg, which: 1);
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  } else {
                                    try {
                                      if (mounted) {
                                        final err = model
                                            .errorMessages.changePassword[0]
                                            .toString();
                                        showToast(msg: err);
                                      }
                                    } catch (e) {
                                      log(e.toString());
                                    }
                                  }
                                } catch (e) {
                                  log(e.toString());
                                }
                              }
                            },
                          );
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
