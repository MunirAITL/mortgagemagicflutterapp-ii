import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class CaseAlertScreen extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertScreen({Key key, @required this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Case alerts",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: MyTheme.redColor),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawCaseUI(context),
      ),
    );
  }

  drawCaseUI(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + .8,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "Close",
                width: getW(context),
                height: getHP(context, 7),
                radius: 10,
                callback: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
