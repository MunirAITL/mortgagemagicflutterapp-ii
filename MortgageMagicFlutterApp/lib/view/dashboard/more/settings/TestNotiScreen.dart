import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/dashboard/more/settings/TestNofiAPIMgr.dart';
import 'package:aitl/view/mywidgets/BtnOutline.dart';
import 'package:aitl/view/mywidgets/Misc.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class TestNotiScreen extends StatefulWidget {
  @override
  State createState() => _TestNotiScreenState();
}

class _TestNotiScreenState extends State<TestNotiScreen> with Mixin {
  @mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Notification Settings",
              txtColor: MyTheme.redColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: MyTheme.redColor),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawTestUI(),
      ),
    );
  }

  drawTestUI() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 3)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Txt(
                      txt: "Is it working?",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: BtnOutline(
                    txt: "Test it",
                    txtColor: MyTheme.redColor,
                    borderColor: MyTheme.redColor,
                    callback: () {
                      TestNotiAPIMgr().wsTestNotiAPI(
                        context: context,
                        callback: (model) {
                          if (model != null && mounted) {
                            try {
                              if (model.success) {
                                try {
                                  final msg = model
                                      .responseData.notification.description;
                                  //showToast(msg: msg, which: 1);
                                  showAlert(msg: msg);
                                  /*showAlert(
                  context: context,
                  msg: msg,
                  which: 1,
                );*/
                                } catch (e) {
                                  log(e.toString());
                                }
                              } else {
                                try {
                                  final err =
                                      model.messages.pushMessage[0].toString();
                                  showToast(msg: err, which: 0);
                                } catch (e) {
                                  log(e.toString());
                                }
                              }
                            } catch (e) {
                              log(e.toString());
                            }
                          } else {
                            log("not in");
                          }
                        },
                      );
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt:
                    "Make sure you're actually getting those all important push notifications.",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          drawLine(context: context, w: getW(context)),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt:
                    "Your notifications can be updated at any time via the options below",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          drawLine(context: context, w: getW(context)),
        ],
      ),
    );
  }
}
