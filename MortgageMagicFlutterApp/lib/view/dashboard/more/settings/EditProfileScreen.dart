import 'dart:convert';
import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/dashboard/more/settings/EditProfileAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/form_validate/UserProfileVal.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/controller/helper/more/settings/EditProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/mywidgets/CamPicker.dart';
import 'package:aitl/view/mywidgets/DatePickerView.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/TxtBox.dart';
import 'package:aitl/view/mywidgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../mywidgets/com/DeactivateProfileDialog.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State createState() => _EditProfileState();
}

enum SmookerEnum { yes, no }

class _EditProfileState extends State<EditProfileScreen> with Mixin {
  StateProvider _stateProvider = StateProvider();

  File _pathFG;

  EditProfileHelper editProfileHelper;

  //  personal info
  final _pwd = TextEditingController();
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _dob = TextEditingController();
  final _mobile = TextEditingController();
  final _tel = TextEditingController();
  //final _ref = TextEditingController();

  //  visa info
  final _nin = TextEditingController();
  final _passport = TextEditingController();

  //  deactivate reason
  final TextEditingController _deactivateReason = TextEditingController();

  //  Email
  String email;
  int profileImageId;

  //  DOB
  String dob = "";

  //  Visa
  String visaExp = "";

  //  Passport
  String passportExp = "";

  SmookerEnum _radio = SmookerEnum.no;

  updateProfileAPI() async {
    try {
      if (validate()) {
        final param = EditProfileHelper().getParam(
          firstName: _fname.text.trim(),
          lastName: _lname.text.trim(),
          name: userData.userModel.name,
          email: email,
          userName: userData.userModel.userName,
          profileImageId: profileImageId,
          coverImageId: '',
          referenceId: userData.userModel.referenceID.toString(),
          referenceType: "", //_ref.text.trim(),
          remarks: userData.userModel.remarks,
          cohort: userData.userModel.cohort,
          communityId: userData.userModel.communityID.toString(),
          isFirstLogin: false,
          mobileNumber: _mobile.text.trim(),
          dateofBirth: dob,
          middleName: _mname.text.trim(),
          namePrefix: _fname.text.trim(),
          areYouASmoker: (_radio == SmookerEnum.no) ? 'No' : 'Yes',
          countryCode: '',
          addressLine1: userData.userModel.addressLine1,
          addressLine2: userData.userModel.addressLine2,
          addressLine3: userData.userModel.addressLine3,
          town: userData.userModel.town,
          county: userData.userModel.county,
          postcode: userData.userModel.postcode,
          telNumber: _tel.text.trim(),
          nationalInsuranceNumber: _nin.text.trim(),
          nationality: editProfileHelper.optNationalities.title,
          countryofBirth: editProfileHelper.optCountriesBirth.title,
          countryofResidency: editProfileHelper.optCountriesResidential.title,
          passportNumber: _passport.text.trim(),
          maritalStatus: editProfileHelper.optMaritalStatus.title,
          occupantType: userData.userModel.occupantType,
          livingDate: userData.userModel.livingDate,
          password: _pwd.text.trim(),
          userCompanyId: userData.userModel.userCompanyID,
          visaExpiryDate: visaExp,
          passportExpiryDate: passportExp,
          visaName: userData.userModel.visaName,
          otherVisaName: userData.userModel.otherVisaName,
          id: userData.userModel.id,
        );
        log(json.encode(param));
        EditProfileAPIMgr().wsUpdateProfileAPI(
          context: context,
          param: param,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    final msg = model.messages.post_user[0].toString();
                    showToast(msg: msg, which: 1);
                  } catch (e) {
                    log(e.toString());
                  }
                } else {
                  try {
                    if (mounted) {
                      final err = model.messages.post_user[0].toString();
                      showToast(msg: err);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _pathFG = null;
    editProfileHelper = null;
    _stateProvider = null;

    //  personal info
    _pwd.dispose();
    _fname.dispose();
    _mname.dispose();
    _lname.dispose();
    _dob.dispose();
    _mobile.dispose();
    _tel.dispose();
    //_ref.dispose();

    //  visa info
    _passport.dispose();
    _nin.dispose();

    //  deactivate
    _deactivateReason.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      startLoading();
      Future.delayed(Duration(seconds: 1), () async {
        try {
          final fgPath =
              await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicFG_Key);
          if (fgPath != null) {
            _pathFG = File(fgPath);
          }
        } catch (e) {}

        //  personal info
        email = userData.userModel.email;
        profileImageId = userData.userModel.profileImageID;
        _fname.text = userData.userModel.firstName;
        _mname.text = userData.userModel.middleName;
        _lname.text = userData.userModel.lastName;
        _dob.text = userData.userModel.dateofBirth;
        _mobile.text = userData.userModel.mobileNumber;
        _tel.text = userData.userModel.telNumber;
        //_ref.text = userModel.referenceType;

        //  visa info
        _nin.text = userData.userModel.nationalInsuranceNumber;
        _passport.text = userData.userModel.passportNumber;
        passportExp = DateFormat('dd-MM-yyyy')
            .format(DateTime.parse(userData.userModel.passportExpiryDate));
        visaExp = DateFormat('dd-MM-yyyy')
            .format(DateTime.parse(userData.userModel.visaExpiryDate));

        //  populate countries drop down all 3
        editProfileHelper = EditProfileHelper();
        await editProfileHelper.getCountriesBirth(
            context: context, cap: userData.userModel.countryofBirth);
        editProfileHelper.getCountriesResidential(
            context: context, cap: userData.userModel.countryofResidency);
        editProfileHelper.getCountriesNationaity(
            context: context, cap: userData.userModel.nationality);
        //  set data into drop down
        //editProfileHelper.optTitle.title = userModel.
        //editProfileHelper.optGender.title = userModel.
        if (userData.userModel.maritalStatus != '')
          editProfileHelper.optMaritalStatus.title =
              userData.userModel.maritalStatus;
        dob = userData.userModel.dateofBirth;
        if (userData.userModel.countryofBirth != '')
          editProfileHelper.optCountriesBirth.title =
              userData.userModel.countryofBirth;
        if (userData.userModel.countryofResidency != '')
          editProfileHelper.optCountriesResidential.title =
              userData.userModel.countryofResidency;
        if (userData.userModel.nationality != '')
          editProfileHelper.optNationalities.title =
              userData.userModel.nationality;
        stopLoading();
        setState(() {});
      });
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_mobile)) {
      return false;
    } else if (_passport.text.trim().length > 0 && passportExp == '') {
      showToast(msg: "Invalid passport expiry date");
      return false;
    } else if (visaExp == '') {
      showToast(msg: "Invalid visa expiry date");
      return false;
    } else if (editProfileHelper.optTitle.id == null) {
      showToast(msg: "Please choose title from the list");
      return false;
    } else if (editProfileHelper.optGender.id == null) {
      showToast(msg: "Please choose gender from the list");
      return false;
    } else if (editProfileHelper.optMaritalStatus.id == null) {
      showToast(msg: "Please choose marital status from the list");
      return false;
    } else if (editProfileHelper.optCountriesBirth.id == null) {
      showToast(msg: "Please choose country of birth from the list");
      return false;
    } else if (editProfileHelper.optCountriesResidential.id == null) {
      showToast(msg: "Please choose country of residence from the list");
      return false;
    } else if (editProfileHelper.optNationalities.id == null) {
      showToast(msg: "Please choose nationality from the list");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Edit Profile",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            child: (editProfileHelper == null)
                ? SizedBox()
                : ListView(
                    shrinkWrap: true,
                    children: [
                      SizedBox(height: 20),
                      drawCamPicker(),
                      SizedBox(height: 20),
                      drawPersonalInfoView(),
                      drawVisaInfoView(),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 10, top: 20),
                        child: MMBtn(
                          txt: "Save Profile",
                          width: getW(context),
                          height: getHP(context, 7),
                          radius: 10,
                          callback: () {
                            updateProfileAPI();
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Container(
                          width: getW(context),
                          height: getHP(context, 7),
                          child: new ElevatedButton(
                              child: Txt(
                                  txt: "Deactivate My Account",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                              onPressed: () {
                                showDeactivateProfileDialog(
                                    context: context,
                                    email: _deactivateReason,
                                    callback: (String reason) {
                                      //
                                      if (reason != null) {
                                        EditProfileAPIMgr()
                                            .wsDeactivateProfileAPI(
                                          context: context,
                                          reason: reason,
                                          callback: (model) {
                                            if (model != null && mounted) {
                                              try {
                                                if (model.success) {
                                                  try {
                                                    final msg = model
                                                        .messages.delete[0]
                                                        .toString();
                                                    showToast(
                                                        msg: msg, which: 1);
                                                    Future.delayed(
                                                        Duration(
                                                            seconds: MyConfig
                                                                .AlertDismisSec),
                                                        () {
                                                      //  signout
                                                      _stateProvider.notify(
                                                          ObserverState
                                                              .STATE_CHANGED_logout);
                                                    });
                                                  } catch (e) {
                                                    log(e.toString());
                                                  }
                                                } else {
                                                  try {
                                                    if (mounted) {
                                                      final err = model
                                                          .messages.delete[0]
                                                          .toString();
                                                      showToast(msg: err);
                                                    }
                                                  } catch (e) {
                                                    log(e.toString());
                                                  }
                                                }
                                              } catch (e) {
                                                log(e.toString());
                                              }
                                            }
                                          },
                                        );
                                      }
                                    });
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                    Colors.transparent,
                                  ),
                                  elevation:
                                      MaterialStateProperty.all<double>(0),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                          side: BorderSide(
                                              color: Colors.black,
                                              width: 1))))),
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  drawCamPicker() {
    return Container(
      alignment: Alignment.center,
      child: Stack(
        children: [
          Container(
            width: getWP(context, 25),
            height: getWP(context, 25),
            decoration: MyTheme.picEmboseCircleDeco,
            child: Container(
              //height: getW(context) * 0.3,
              //width: getW(context) * 0.3,

              decoration: BoxDecoration(
                image: DecorationImage(
                  image: (_pathFG != null)
                      ? FileImage(_pathFG)
                      : AssetImage('assets/images/icons/user_icon.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: Container(
              width: 30,
              height: 30,
              child: MaterialButton(
                onPressed: () {
                  CamPicker().showCamModal(
                    context: context,
                    prefkey: ProfileHelper.ProfilePicFG_Key,
                    isRear: false,
                    callback: (File path) {
                      if (path != null) {
                        _pathFG = path;
                        MediaUploadAPIMgr().wsMediaUploadFileAPI(
                          context: context,
                          file: path,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  profileImageId =
                                      model.responseData.images[0].id;
                                  setState(() {});
                                } else {
                                  final err = model
                                      .errorMessages.upload_pictures[0]
                                      .toString();
                                  showToast(msg: err);
                                }
                              } catch (e) {
                                log(e.toString());
                              }
                            }
                          },
                        );
                      }
                    },
                  );
                },
                color: Colors.grey,
                child: Icon(
                  Icons.camera_alt_outlined,
                  size: 20,
                ),
                padding: EdgeInsets.all(0),
                shape: CircleBorder(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawPersonalInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        //width: getW(context),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 10, right: 10, bottom: 10),
              child: Container(
                width: getW(context),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Txt(
                        txt: "Personal information",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.black87,
                      height: 0.5,
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DropDownPicker(
                cap: "Choose Title",
                itemSelected: editProfileHelper.optTitle,
                dropListModel: editProfileHelper.ddTitle,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optTitle = optionItem;
                  setState(() {});
                },
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "First Name",
                input: _fname,
                kbType: TextInputType.name,
                len: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "Middle Name",
                input: _mname,
                kbType: TextInputType.name,
                len: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "Last Name",
                input: _lname,
                kbType: TextInputType.name,
                len: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: DropDownPicker(
                cap: "Choose Gender",
                itemSelected: editProfileHelper.optGender,
                dropListModel: editProfileHelper.ddGender,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optGender = optionItem;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DropDownPicker(
                cap: "Choose Marital Status",
                itemSelected: editProfileHelper.optMaritalStatus,
                dropListModel: editProfileHelper.ddMaritalStatus,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optMaritalStatus = optionItem;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DatePickerView(
                cap: 'Select date of birth',
                dt: (dob == '') ? 'Select date of birth' : dob,
                initialDate: dateDOBlast,
                firstDate: dateDOBfirst,
                lastDate: dateDOBlast,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dob = DateFormat('dd-MM-yyyy').format(value).toString();
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DropDownPicker(
                cap: "Country of birth",
                itemSelected: editProfileHelper.optCountriesBirth,
                dropListModel: editProfileHelper.ddMaritalCountriesBirth,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optCountriesBirth = optionItem;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 20, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Email",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 15),
                  TxtBox(txt: email),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "Mobile Number",
                input: _mobile,
                kbType: TextInputType.phone,
                len: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "Telephone Number",
                input: _tel,
                kbType: TextInputType.phone,
                len: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: DropDownPicker(
                cap: "Country of Residence",
                itemSelected: editProfileHelper.optCountriesResidential,
                dropListModel: editProfileHelper.ddMaritalCountriesResidential,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optCountriesResidential = optionItem;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DropDownPicker(
                cap: "Nationality",
                itemSelected: editProfileHelper.optNationalities,
                dropListModel: editProfileHelper.ddMaritalNationalities,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optNationalities = optionItem;
                  setState(() {});
                },
              ),
            ),
            drawSmooker(),
          ],
        ),
      ),
    );
  }

  drawVisaInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        //width: getW(context),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 30, bottom: 30),
              child: Container(
                width: getW(context),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Txt(
                        txt: "Visa information",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.black87,
                      height: 0.5,
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "National Insurance Number",
                input: _nin,
                kbType: TextInputType.name,
                len: 50,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawInputBox(
                title: "Passport Number",
                input: _passport,
                kbType: TextInputType.name,
                len: 50,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: DatePickerView(
                cap: 'Passport Expiry Date',
                dt: (passportExp == '') ? 'Passport Expiry Date' : passportExp,
                initialDate: dateExpfirst,
                firstDate: dateExpfirst,
                lastDate: dateExplast,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        passportExp =
                            DateFormat('dd-MM-yyyy').format(value).toString();
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DropDownPicker(
                cap: "Visa Status",
                itemSelected: editProfileHelper.optVisaStatus,
                dropListModel: editProfileHelper.ddVisaStatus,
                onOptionSelected: (optionItem) {
                  editProfileHelper.optVisaStatus = optionItem;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
              child: DatePickerView(
                cap: 'Visa Expiry Date',
                dt: (visaExp == '') ? 'Visa Expiry Date' : visaExp,
                initialDate: dateExpfirst,
                firstDate: dateExpfirst,
                lastDate: dateExplast,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        visaExp =
                            DateFormat('dd-MM-yyyy').format(value).toString();
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawSmooker() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
              txt: "Are you a smooker?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
            SizedBox(height: 15),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radio = SmookerEnum.no;
                        });
                      }
                    },
                    child: Radio(
                      value: SmookerEnum.no,
                      groupValue: _radio,
                      onChanged: (SmookerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radio = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "No",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radio = SmookerEnum.yes;
                        });
                      }
                    },
                    child: Radio(
                      value: SmookerEnum.yes,
                      groupValue: _radio,
                      onChanged: (SmookerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radio = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Yes",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
