import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/dashboard/more/settings/NotiSettingsCfg.dart';
import 'package:aitl/controller/api/dashboard/more/settings/NotiSettingsAPIMgr.dart';
import 'package:aitl/view/mywidgets/MMBtn.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotiSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotiSettingsScreenState();
}

class _NotiSettingsScreenState extends State<NotiSettingsScreen> with Mixin {
  NotiSettingsCfg notiSettingsCfg;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    notiSettingsCfg = null;
    super.dispose();
  }

  appInit() async {
    try {
      notiSettingsCfg = NotiSettingsCfg();
      NotiSettingsAPIMgr().wsGetNotificationSettingsAPI(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final caseUpdateMap = notiSettingsCfg.listOpt[0];
                  final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                  final caseCompletedMap = notiSettingsCfg.listOpt[2];
                  final caseReminderMap = notiSettingsCfg.listOpt[3];
                  final userUpdateMap = notiSettingsCfg.listOpt[4];
                  final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                  final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

                  //  caseUpdate
                  caseUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseUpdateEmail;
                  caseUpdateMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseUpdateSMS;
                  caseUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseUpdateNotification;
                  //  caseRecomendation
                  caseRecomendationMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationEmail;
                  caseRecomendationMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationNotification;
                  //  caseCompleted
                  caseCompletedMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseCompletedEmail;
                  caseCompletedMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseCompletedSMS;
                  caseCompletedMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseCompletedNotification;
                  //  caseReminder
                  caseReminderMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseReminderEmail;
                  caseReminderMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseReminderSMS;
                  caseReminderMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseReminderNotification;
                  //  userUpdateMap
                  userUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isUserUpdateEmail;
                  userUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isUserUpdateNotification;
                  //  helpfulInfoMap
                  helpfulInfoMap['isEmail'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationEmail;
                  helpfulInfoMap['isPush'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationNotification;
                  //  updateNewsLetterMap
                  updateNewsLetterMap['isEmail'] = model.responseData
                      .userNotificationSetting.isUpdateAndNewsLetterEmail;
                  updateNewsLetterMap['isPush'] = model
                      .responseData
                      .userNotificationSetting
                      .isUpdateAndNewsLetterNotification;

                  //notiSettingsCfg.listOpt[6] = updateNewsLetterMap;
                  //log(notiSettingsCfg.listOpt[6].toString());

                  setState(() {});
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(msg: "Notifications Settings not found");
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        },
      );
    } catch (e) {}
    try {
      /*await NotiSettingsHelper().getSettings().then((listOpt) {
        if (listOpt != null) {
          notiSettingsCfg.listOpt = listOpt;
          setState(() {});
        } else {}
      });*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Notification settings",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: drawNotiSettingsItems(),
      ),
    );
  }

  drawNotiSettingsItems() {
    return Container(
      child: (notiSettingsCfg == null)
          ? SizedBox()
          : SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 30, right: 30, top: 20, bottom: 20),
                    child: Txt(
                        txt:
                            "Your notifications can be updated at any time via options below or the MortgageLad app",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .4,
                        //txtLineSpace: 1.3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  for (var listOpt in notiSettingsCfg.listOpt)
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        width: double.infinity,
                        //color: Colors.black,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Txt(
                                txt: listOpt['title']
                                    .toString()
                                    .capitalizeFirstofEach,
                                txtColor: MyTheme.timelineTitleColor,
                                txtSize: MyTheme.txtSize + .3,
                                txtAlign: TextAlign.start,
                                isBold: true),
                            SizedBox(height: 10),
                            Txt(
                                txt: listOpt['subTitle'],
                                txtColor: MyTheme.timelineTitleColor,
                                txtSize: MyTheme.txtSize,
                                //txtLineSpace: 1.2,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            SizedBox(height: 10),
                            (listOpt['email'] as bool)
                                ? Theme(
                                    data: MyTheme.radioThemeData,
                                    child: Transform.translate(
                                      offset: Offset(-10, 0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Transform.scale(
                                            scale: 1.2,
                                            child: Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isEmail'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isEmail'] = newValue;
                                                });
                                              },
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Txt(
                                              txt: 'Email',
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        ],
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            (listOpt['sms'] as bool)
                                ? Theme(
                                    data: MyTheme.radioThemeData,
                                    child: Transform.translate(
                                      offset: Offset(-10, 0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Transform.scale(
                                            scale: 1.2,
                                            child: Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isSms'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isSms'] = newValue;
                                                });
                                              },
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Txt(
                                              txt: 'Sms',
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        ],
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            (listOpt['push'] as bool)
                                ? Theme(
                                    data: MyTheme.radioThemeData,
                                    child: Transform.translate(
                                      offset: Offset(-10, 0),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Transform.scale(
                                            scale: 1.2,
                                            child: Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isPush'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isPush'] = newValue;
                                                });
                                              },
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          Txt(
                                              txt: 'Push',
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        ],
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                          ],
                        ),
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20, left: 30, right: 30, bottom: 20),
                    child: MMBtn(
                      txt: "Save",
                      width: getW(context),
                      height: getHP(context, 7),
                      radius: 10,
                      callback: () async {
                        NotiSettingsAPIMgr().wsPostNotificationSettingsAPI(
                          context: context,
                          notiSettingsCfg: notiSettingsCfg,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    final msg = model
                                        .messages.postNotificationSetting[0]
                                        .toString();
                                    showToast(msg: msg, which: 1);
                                    //await NotiSettingsHelper().setSettings(notiSettingsCfg.listOpt);
                                  } catch (e) {
                                    log(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err = model
                                          .messages.postNotificationSetting[0]
                                          .toString();
                                      showToast(msg: err, which: 1);
                                    }
                                  } catch (e) {
                                    log(e.toString());
                                  }
                                }
                              } catch (e) {
                                log(e.toString());
                              }
                            }
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
