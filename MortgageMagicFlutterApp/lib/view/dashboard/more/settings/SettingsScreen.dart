import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/more/MoreHelper.dart';
import 'package:aitl/view/dashboard/more/settings/ChangePwdScreen.dart';
import 'package:aitl/view/dashboard/more/settings/TestNotiScreen.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'EditProfileScreen.dart';
import 'NotiSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with Mixin {
  List<String> listItems = [
    "Edit Profile",
    "Notification Settings",
    "Change Password",
    //"Test Notification"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_ios, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Txt(
                      txt: "Settings",
                      txtColor: MyTheme.redColor,
                      txtSize: MyTheme.appbarTitleFontSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            child: ListView.builder(
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                return GestureDetector(
                  onTap: () async {
                    switch (index) {
                      case 0:
                        navTo(
                          context: context,
                          page: () => EditProfileScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 1:
                        navTo(
                          context: context,
                          page: () => NotiSettingsScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;

                      case 2:
                        navTo(
                          context: context,
                          page: () => ChangePwdScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 3:
                        navTo(
                          context: context,
                          page: () => TestNotiScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      default:
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: ListTile(
                          title: Txt(
                              txt: item,
                              txtColor: MyTheme.moreTxtColor,
                              txtSize: MyTheme.txtSize + .3,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          trailing: (index != MoreHelper.listMore.length - 1)
                              ? Icon(
                                  Icons.arrow_forward_ios,
                                  color: MyTheme.moreTxtColor,
                                  //size: 20,
                                )
                              : SizedBox(),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
