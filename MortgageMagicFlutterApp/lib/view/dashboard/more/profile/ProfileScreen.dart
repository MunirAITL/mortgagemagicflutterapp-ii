import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/more/ProfileHelper.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/mywidgets/CamPicker.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with Mixin {
  File _pathBG;
  File _pathFG;

  var address;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _pathBG = null;
    _pathFG = null;
    address = null;
    super.dispose();
  }

  appInit() async {
    Future.delayed(Duration.zero, () async {
      try {
        final bgPath =
            await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicBG_Key);
        if (bgPath != null) {
          _pathBG = File(bgPath);
        }
      } catch (e) {}
      try {
        final fgPath =
            await PrefMgr.shared.getPrefStr(ProfileHelper.ProfilePicFG_Key);
        if (fgPath != null) {
          _pathFG = File(fgPath);
        }
      } catch (e) {}
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    try {
      address = userData.userModel.address;
    } catch (e) {}
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        //resizeToAvoidBottomPadding: true,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                elevation: MyTheme.appbarElevation,
                backgroundColor: MyTheme.themeData.accentColor,
                iconTheme: IconThemeData(
                    color: MyTheme.redColor //change your color here
                    ),

                automaticallyImplyLeading: false,
                title: Container(
                  width: getW(context),
                  //color: Colors.yellow,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        //color: Colors.black,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Row(
                            children: [
                              Icon(Icons.arrow_back_ios, size: 20),
                              Txt(
                                  txt: "Back",
                                  txtColor: MyTheme.redColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Txt(
                            txt: "Profile",
                            txtColor: MyTheme.redColor,
                            txtSize: MyTheme.appbarTitleFontSize,
                            txtAlign: TextAlign.center,
                            isBold: true),
                      ),
                      IconButton(
                        iconSize: 30,
                        icon: Image.asset(
                            "assets/images/icons/help_circle_icon.png"),
                        onPressed: () {
                          // do something
                          Get.to(
                            () => WebScreen(
                              title: "Help",
                              url: Server.HELP_INFO_URL,
                            ),
                          ).then((value) {
                            //callback(route);
                          });
                        },
                      )
                    ],
                  ),
                ),

                expandedHeight: getHP(context, 55),
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  //collapseMode: CollapseMode.pin,
                  centerTitle: true,
                  background: Container(
                    decoration: (_pathBG != null)
                        ? BoxDecoration(
                            image: DecorationImage(
                              image: FileImage(_pathBG),
                              fit: BoxFit.cover,
                            ),
                          )
                        : BoxDecoration(color: MyTheme.themeData.accentColor),
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: getHP(context, 12)),
                          /*Align(
                                  alignment: Alignment.topRight,
                                  child: GestureDetector(
                                    onTap: () {
                                      CamPicker().showCamModal(
                                        context: context,
                                        prefkey: ProfileHelper.ProfilePicBG_Key,
                                        isRear: true,
                                        callback: (File path) {
                                          if (path != null) {
                                            setState(() {
                                              _pathBG = path;
                                            });
                                          }
                                        },
                                      );
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 20),
                                      child: Container(
                                        child: Image.asset(
                                          "assets/images/icons/cam_icon.png",
                                          //color: Colors.transparent,
                                          width: 30,
                                          height: 30,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),*/
                          //SizedBox(height: getHP(context, 3)),
                          Stack(
                            children: [
                              Container(
                                width: getWP(context, 25),
                                height: getWP(context, 25),
                                child: Container(
                                  height: getW(context) * 0.3,
                                  width: getW(context) * 0.3,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: (_pathFG != null)
                                          ? FileImage(_pathFG)
                                          : AssetImage(
                                              'assets/images/icons/user_icon.png'),
                                      fit: BoxFit.fill,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 0,
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  child: MaterialButton(
                                    onPressed: () {
                                      CamPicker().showCamModal(
                                        context: context,
                                        prefkey: ProfileHelper.ProfilePicFG_Key,
                                        isRear: false,
                                        callback: (File path) {
                                          if (path != null) {
                                            setState(() {
                                              _pathFG = path;
                                            });
                                          }
                                        },
                                      );
                                    },
                                    color: Colors.grey,
                                    child: Icon(
                                      Icons.camera_alt_outlined,
                                      size: 15,
                                    ),
                                    padding: EdgeInsets.all(0),
                                    shape: CircleBorder(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            //color: Colors.black,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 10, right: 2),
                                  child: Txt(
                                      txt: userData.userModel.name,
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.center,
                                      isBold: true),
                                ),
                                ProfileHelper().getUserOnlineStatus(
                                    context: context, statusIndex: 1),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Container(
                              //color: Colors.yellow,
                              width: getWP(context, 70),
                              child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/icons/pin_icon.png",
                                      width: 20,
                                      height: 20,
                                      color: Colors.black,
                                    ),
                                    (address.length == 0)
                                        ? Flexible(
                                            child: Txt(
                                                txt: address,
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          )
                                        : SizedBox(),
                                  ]),
                            ),
                          ),
                          SizedBox(height: 5),
                          ProfileHelper()
                              .getStarRatingView(rate: 4, reviews: 5),
                          SizedBox(height: 5),
                          ProfileHelper().getCompletionText(pa: 50),
                        ],
                      ),
                    ),
                  ),
                ),
                //background:
              ),
            ];
          },
          body: ListView(
            shrinkWrap: true,
            primary: true,
            children: [],
          ),
        ),
      ),
    );
  }
}
