import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/helper/more/badge/BadgeHelper.dart';
import 'package:aitl/view/mywidgets/MyNetworkImage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class ImageHoverPH extends StatefulWidget {
  final String url;
  final int id;
  final Function(BadgeType) callback;
  final Function(int) callbackDelbyID;
  final BadgeType badgeType;
  const ImageHoverPH({
    Key key,
    @required this.url,
    @required this.id,
    @required this.callback,
    @required this.callbackDelbyID,
    @required this.badgeType,
  }) : super(key: key);
  @override
  State createState() => _ImageHoverPHState();
}

class _ImageHoverPHState extends State<ImageHoverPH>
    with Mixin, TickerProviderStateMixin {
  bool isHovered = false;

  hoverActivation(hoverState) {
    setState(() {
      isHovered = hoverState;
    });
    log("activated" + hoverState.toString());
  }

  @override
  Widget build(context) {
    double w = getWP(context, 77);
    double h = getHP(context, 30);
    return Center(
      child: Container(
        //color: Colors.red,
        alignment: Alignment.center,
        child: MouseRegion(
            onEnter: (event) {
              hoverActivation(true);
            },
            onExit: (event) {
              hoverActivation(false);
            },
            child: AnimatedContainer(
              duration: Duration(milliseconds: 500),
              color: Colors.black.withOpacity(isHovered ? 0.5 : 0),
              child: Container(
                margin: EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 13.0, right: 8.0),
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 0.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ]),
                      child: Container(
                        width: w,
                        height: h,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(0),
                          child:
                              MyNetworkImage().loadCacheImage(url: widget.url),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0.0,
                      child: GestureDetector(
                        onTap: () {
                          widget.callbackDelbyID(widget.id);
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: CircleAvatar(
                            radius: 14.0,
                            backgroundColor: Colors.grey,
                            child: Icon(Icons.close, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: (h / 2) - 10,
                      left: (w / 2) - 10,
                      child: GestureDetector(
                        onTap: () {
                          widget.callback(widget.badgeType);
                        },
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: MyTheme.redColor,
                          child: Icon(Icons.camera_alt_outlined,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
