import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class BtnTextIcon extends StatelessWidget {
  final width, height;
  final txt;
  final IconData icon;
  final Image image;
  final bool isRightIco;
  final txtColor;
  final bgColor;
  final isCurve;
  final Function callback;

  const BtnTextIcon({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    @required this.width,
    @required this.height,
    @required this.icon,
    @required this.image,
    @required this.isRightIco,
    @required this.isCurve,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: MaterialButton(
        child: (isRightIco) ? drawRIco() : drawLIco(),
        onPressed: () {
          callback();
        },
        textColor: txtColor,
        color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1.0,
                color: Colors.transparent),
            borderRadius: BorderRadius.circular((isCurve) ? 10 : 0)),
      ),
    );
  }

  drawLIco() => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          (icon != null)
              ? Icon(
                  icon,
                  size: 40,
                )
              : image,
          Flexible(
            child: Container(
              //color: Colors.black,
              width: width * .9,
              child: Txt(
                  txt: txt,
                  txtColor: null,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
        ],
      );

  drawRIco() => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Container(
              //color: Colors.black,
              width: width * .9,
              child: Txt(
                  txt: txt,
                  txtColor: null,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          //Spacer(),
          (icon != null)
              ? Icon(
                  icon,
                  size: 40,
                )
              : image,
        ],
      );
}
