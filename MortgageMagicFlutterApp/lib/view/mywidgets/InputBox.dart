import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:flutter/services.dart';

class InputBox extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd;
  bool autofocus;
  InputBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    @required this.isPwd,
    this.autofocus = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: ctrl,
      autofocus: autofocus,
      keyboardType: kbType,
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : null,
      obscureText: isPwd,
      maxLength: len,
      autocorrect: false,
      style: TextStyle(
        color: Colors.black,
        fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        hintText: lableTxt,
        hintStyle: new TextStyle(
          color: Colors.grey,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        labelStyle: new TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        contentPadding: EdgeInsets.only(left: 20, right: 20),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
      ),
    );
  }
}
