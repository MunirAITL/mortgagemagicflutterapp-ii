import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/IcoTxtIco.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class DatePickerView extends StatelessWidget {
  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final String cap;
  final String dt;

  final Function callback;

  DatePickerView({
    Key key,
    @required this.cap,
    @required this.dt,
    @required this.callback,
    @required this.initialDate,
    @required this.firstDate,
    @required this.lastDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (this.cap != null)
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Txt(
                    txt: this.cap,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                )
              : SizedBox(),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: firstDate,
                lastDate: lastDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(primary: MyTheme.redColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: this.dt,
              rightIcon: Icons.keyboard_arrow_down,
              txtAlign: TextAlign.left,
              rightIconSize: 40,
              leftIconSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
