import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/mywidgets/InputBox.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/SwitchView.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:aitl/view/mywidgets/gplaces/GPlacesView.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import '../IcoTxtIco.dart';

class SPVSwitchView extends StatefulWidget {
  bool isSwitch;
  final Function(String) callback;
  final Function(String) callbackAddress;
  final Function(bool) callbackSwitch;
  final TextEditingController compName;
  final String regAddr;
  final TextEditingController regNo;
  String regDate;
  SPVSwitchView({
    Key key,
    @required this.compName,
    @required this.regAddr,
    @required this.regNo,
    @required this.regDate,
    @required this.callback,
    @required this.callbackAddress,
    @required this.callbackSwitch,
    this.isSwitch = false,
  }) : super(key: key);

  @override
  State createState() => _SPVSwitchViewState();
}

class _SPVSwitchViewState extends State<SPVSwitchView> with Mixin {
  String dd = "";
  String mm = "";
  String yy = "";

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                    txt:
                        "Are you buying the property in the name of a SPV (Limited Company)?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                    //txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 5),
                SwitchView(
                  onTxt: "Yes",
                  offTxt: "No",
                  value: widget.isSwitch,
                  onChanged: (value) {
                    widget.isSwitch = value;
                    widget.callbackSwitch(value);
                    if (mounted) {
                      setState(() {});
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (widget.isSwitch) ? drawSPVInputFields() : SizedBox(),
        ],
      ),
    );
  }

  drawSPVInputFields() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          children: [
            drawInputBox(
              title: "Name of Company",
              input: widget.compName,
              kbType: TextInputType.name,
              len: 100,
            ),
            /*drawInputBox(
              title: "Registered Address",
              input: widget.regAddr,
              kbType: TextInputType.streetAddress,
              len: 255,
            ),*/
            GPlacesView(
                title: "Registered Address",
                callback: (String address, Location loc) {
                  widget.callbackAddress(address);
                }),
            SizedBox(height: 20),
            drawInputBox(
              title: "Company Registeration Number",
              input: widget.regNo,
              kbType: TextInputType.name,
              len: 50,
            ),
            drawRegDate(),
          ],
        ),
      ),
    );
  }

  drawRegDate() {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Date registered",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(primary: MyTheme.redColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  if (mounted) {
                    setState(() {
                      try {
                        widget.regDate =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = date.toString().split('-');
                        this.dd = dobArr[0];
                        this.mm = dobArr[1];
                        this.yy = dobArr[2];
                        widget.callback(widget.regDate);
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: (widget.regDate == "") ? "Select a date" : widget.regDate,
              rightIcon: Icons.arrow_drop_down,
              txtAlign: TextAlign.left,
              rightIconSize: 50,
            ),
          ),
        ],
      ),
    );
  }
}
