import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/InputBox.dart';
import 'package:aitl/view/mywidgets/InputTitleBox.dart';
import 'package:aitl/view/mywidgets/SwitchView.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class OtherApplicantSwitchView extends StatefulWidget {
  bool isSwitch;
  int totalOtherApplicantRadio;
  final Function(int) callback;
  final Function(bool) callbackSwitch;
  List<TextEditingController> listOApplicantInputFieldsCtr;
  OtherApplicantSwitchView({
    Key key,
    @required this.listOApplicantInputFieldsCtr,
    @required this.callback,
    @required this.callbackSwitch,
    this.totalOtherApplicantRadio = 1,
    this.isSwitch = false,
  }) : super(key: key);

  @override
  State createState() => _OtherApplicantSwitchViewState();
}

enum RadioEnum { one, two, three }

class _OtherApplicantSwitchViewState extends State<OtherApplicantSwitchView>
    with Mixin {
  RadioEnum _character;

  List<String> listInputFieldsTitle = [
    "Please enter Second Applicant's email address:",
    "Please enter Third Applicant's email address:",
    "Please enter Fourth Applicant's email address:"
  ];
  @override
  void initState() {
    super.initState();

    try {
      switch (widget.totalOtherApplicantRadio) {
        case 1:
          _character = RadioEnum.one;
          break;
        case 2:
          _character = RadioEnum.two;
          break;
        case 3:
          _character = RadioEnum.three;
          break;
        default:
          _character = RadioEnum.one;
          break;
      }
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                    txt: "Is there other applicant?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                    //txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 5),
                SwitchView(
                  onTxt: "Yes",
                  offTxt: "No",
                  value: widget.isSwitch,
                  onChanged: (value) {
                    widget.isSwitch = value;
                    widget.callbackSwitch(value);
                    if (mounted) {
                      setState(() {
                        //_stateProvider.notify(ObserverState.STATE_CHANGED);
                      });
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (widget.isSwitch) ? drawApplicantView() : SizedBox(),
        ],
      ),
    );
  }

  drawApplicantView() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            Txt(
              txt:
                  "How many people do you want named on the case application?*",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
              //txtLineSpace: 1.2,
            ),
            SizedBox(height: 10),
            Txt(
              txt:
                  "It's ok if the person you're applying with doesn't have an income-they can still be named on your case application. Choose an option from the below One, Two or Three",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
              //txtLineSpace: 1.2,
            ),
            drawApplicantRadioView(),
            drawInputFieldsView(),
          ],
        ),
      ),
    );
  }

  drawApplicantRadioView() {
    return Container(
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Theme(
            data: MyTheme.radioThemeData,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _character = RadioEnum.one;
                        widget.callback(1);
                      });
                    }
                  },
                  child: Radio(
                    value: RadioEnum.one,
                    groupValue: _character,
                    onChanged: (RadioEnum value) {
                      if (mounted) {
                        setState(() {
                          _character = value;
                          widget.callback(1);
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "One",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),

                //SizedBox(width: 10),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _character = RadioEnum.two;
                        widget.callback(2);
                      });
                    }
                  },
                  child: Radio(
                    value: RadioEnum.two,
                    groupValue: _character,
                    onChanged: (RadioEnum value) {
                      if (mounted) {
                        setState(() {
                          _character = value;
                          widget.callback(2);
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Two",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),

                //SizedBox(width: 10),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _character = RadioEnum.three;
                        widget.callback(3);
                      });
                    }
                  },
                  child: Radio(
                    value: RadioEnum.three,
                    groupValue: _character,
                    onChanged: (RadioEnum value) {
                      if (mounted) {
                        setState(() {
                          _character = value;
                          widget.callback(3);
                        });
                      }
                    },
                  ),
                ),
                Flexible(
                  child: Txt(
                    txt: "Three",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawInputFieldsView() {
    try {
      int totalInputFields = 0;
      switch (_character.index) {
        case 0:
          totalInputFields = 1;
          break;
        case 1:
          totalInputFields = 2;
          break;
        case 2:
          totalInputFields = 3;
          break;
        default:
      }
      return (totalInputFields == 0)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                child: Column(
                  children: [
                    //SizedBox(height: 10),
                    for (int i = 0; i < totalInputFields; i++)
                      drawInputBox(
                        title: listInputFieldsTitle[i],
                        input: widget.listOApplicantInputFieldsCtr[i],
                        kbType: TextInputType.emailAddress,
                        len: 50,
                      ),
                  ],
                ),
              ),
            );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
