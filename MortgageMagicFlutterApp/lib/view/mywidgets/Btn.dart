import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final String txt;
  final Color txtColor;
  final Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  Btn({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      //color: Colors.brown,
      child: MaterialButton(
        child: Txt(
          txt: txt,
          txtColor: txtColor,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: true,
        ),
        onPressed: () => callback(),
        //textColor: Colors.black,
        color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1.0,
                color: Colors.transparent),
            borderRadius: new BorderRadius.circular(radius)),
      ),
    );
  }
}
