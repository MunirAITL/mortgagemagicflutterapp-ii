import 'dart:ui';
import 'package:aitl/config/dashboard/HomeCfg.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class CirclePainter extends CustomPainter {
  final int totalCases;
  final double completePA;
  final double fmaPA;
  final double offeredPA;
  final double width;
  final double height;
  final double fontSize;

  CirclePainter({
    @required this.totalCases,
    @required this.offeredPA,
    @required this.completePA,
    @required this.fmaPA,
    @required this.width,
    @required this.height,
    @required this.fontSize,
  });

  final TextPainter textPainter = TextPainter(
    textDirection: TextDirection.ltr,
  );

  @override
  void paint(Canvas canvas, Size size) {
    //double totalLeadPA = 100 - (completePA + fmaPA + offeredPA);

    double borderWidth = 20;

    Paint leadPaint = new Paint()
      ..color = HomeCfg.leadColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint completePaint = new Paint()
      ..color = HomeCfg.completedColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint fmaPaint = new Paint()
      ..color = HomeCfg.fmaColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint offeredPaint = new Paint()
      ..color = HomeCfg.offeredColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, leadPaint);

    //  red = completed
    double arcAngle = 2 * math.pi * (completePA / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -math.pi / 2, arcAngle, false, completePaint);

    //  red light = fma completed
    double arcAngle2 = 2 * math.pi * (fmaPA / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle, arcAngle2, false, fmaPaint);

    double arcAngle3 = 2 * math.pi * (offeredPA / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle + arcAngle2, arcAngle3, false, offeredPaint);

    drawText(arcAngle, canvas, size, totalCases.toString());
  }

  void drawText(double pos, Canvas canvas, Size size, String text) {
    final style1 = TextStyle(
        color: Colors.black.withAlpha(240),
        fontSize: fontSize * 2,
        letterSpacing: 1.2,
        fontWeight: FontWeight.w900);
    final style2 = TextStyle(
        color: Colors.black.withAlpha(240),
        fontSize: fontSize,
        letterSpacing: 1.2,
        fontWeight: FontWeight.w900);
    textPainter.text = TextSpan(style: style1, text: text);
    textPainter.layout();
    textPainter.paint(canvas, Offset(width * .12, size.height / 4));

    textPainter.text = TextSpan(style: style2, text: "Total Cases");
    textPainter.layout();
    textPainter.paint(canvas, Offset(width * .06, size.height / 1.8));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
