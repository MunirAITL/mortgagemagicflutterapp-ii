import 'dart:async';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class MyAlert with Mixin {
  static const double padding = 20;
  static const double avatarRadius = 45;
  Timer _t;
  AlertDialog _alert;

// set up the button
  okButton(context, fun) => MaterialButton(
        child: Txt(
            txt: (fun != null) ? "Yes" : "OK",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: true),
        onPressed: () {
          try {
            if (_t != null) _t.cancel();
            _alert = null;
            if (fun != null) fun();
            Navigator.of(context, rootNavigator: true).pop('dialog');
          } catch (e) {}
        },
      );

  cancelButton(context) => MaterialButton(
        child: Txt(
            txt: "No",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false),
        onPressed: () {
          try {
            if (_t != null) _t.cancel();
            _alert = null;
            Navigator.of(context, rootNavigator: true).pop('dialog');
          } catch (e) {}
        },
      );

  MyAlert(BuildContext context, String msg, int msgType, Function fun) {
    try {
      _alert = AlertDialog(
        backgroundColor: Colors.white,
        shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10)),
        elevation: 0,
        content: contentBox(context, msg, msgType, fun),
        actions: (fun != null)
            ? [
                cancelButton(context),
                okButton(context, fun),
              ]
            : [
                okButton(context, fun),
              ],
      );
      // show the dialog
      showDialog(
        context: context,
        builder: (context) {
          try {
            if (fun == null) {
              _t = Timer(Duration(seconds: MyConfig.AlertDismisSec), () {
                try {
                  _t.cancel();
                  _alert = null;
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                } catch (e) {}
              });
            }
          } catch (e) {}
          return _alert;
        },
      );
    } catch (e) {}
  }

  contentBox(context, msg, msgType, fun) {
    try {
      var img = "info.png";
      switch (msgType) {
        case 0:
          img = "error.png";
          break;
        case 1:
          img = "success.png";
          break;
        default:
      }
      return Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                left: padding,
                top: avatarRadius + padding,
                right: padding,
                bottom: padding),
            margin: EdgeInsets.only(top: avatarRadius),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(height: 20),
                  Txt(
                      txt: msg,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                ],
              ),
            ),
          ),
          Positioned(
            left: padding,
            right: padding,
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: avatarRadius,
              child: Image.asset(
                "assets/images/alert/" + img,
                width: getW(context) * 0.1,
                height: getW(context) * 0.1,
              ),
            ),
          ),
        ],
      );
    } catch (e) {}
  }
}
