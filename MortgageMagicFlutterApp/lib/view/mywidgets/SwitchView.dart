import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

/*class SwitchView2 extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  const SwitchView2({
    Key key,
    @required this.value,
    @required this.onChanged,
  }) : super(key: key);

  @override
  State createState() => _SwitchViewState();
}

class _SwitchViewState extends State<SwitchView> with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHP(context, 6),
      child: Row(
        children: [
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (widget.value)
                      ? MyTheme.themeData.accentColor
                      : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "Yes",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize-.4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(true);
                },
              ),
            ),
          ),
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (!widget.value)
                      ? MyTheme.themeData.accentColor
                      : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topRight: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "No",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize-.4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(false);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
*/

class SwitchView extends StatefulWidget {
  final bool value;
  final onTxt, offTxt;
  final ValueChanged<bool> onChanged;

  const SwitchView({
    Key key,
    @required this.value,
    @required this.onChanged,
    @required this.onTxt,
    @required this.offTxt,
  }) : super(key: key);

  @override
  State createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<SwitchView>
    with SingleTickerProviderStateMixin, Mixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            end: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            begin: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.onChanged(
                _circleAnimation.value == Alignment.centerLeft ? false : true);
          },
          child: Container(
            width: getWP(context, 20),
            //height: getHP(context, 7),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: MyTheme
                  .redColor, /*_circleAnimation.value == Alignment.centerLeft
                    ? Colors.grey
                    : MyTheme.redColor*/
            ),
            child: Row(
              mainAxisAlignment:
                  (_circleAnimation.value == Alignment.centerLeft)
                      ? MainAxisAlignment.start
                      : MainAxisAlignment.end,
              children: <Widget>[
                _circleAnimation.value == Alignment.centerRight
                    ? Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: Txt(
                          txt: widget.offTxt,
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                      )
                    : SizedBox(),
                Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: MyTheme.switchOffColor),
                ),
                _circleAnimation.value == Alignment.centerLeft
                    ? Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Txt(
                          txt: widget.onTxt,
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          ),
        );
      },
    );
  }
}
