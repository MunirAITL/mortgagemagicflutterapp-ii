import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class SplashScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyTheme.themeData.accentColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Container(
                    width: getWP(context, 65),
                    child: Image.asset(
                      'assets/images/logo/mm.png',
                      fit: BoxFit.fitWidth,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
